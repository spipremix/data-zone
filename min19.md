# min19

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[bolo](https://git.spip.net/spip-contrib-extensions/bolo)|[1.9.0;3.2.*]|7|3.2;3.0|< 2023-02-23|
|[citations_bb](https://git.spip.net/spip-contrib-extensions/citations_bien_balisees)|[1.9.0;4.*]|229|4.4;4.3;4.2;4.1;3.2;3.1;2.1;1.9|2024-07-05|
|[couteau_suisse](https://git.spip.net/spip-contrib-extensions/couteau_suisse)|[1.9.2;4.*]|1540|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2024-11-11|
