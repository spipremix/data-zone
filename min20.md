# min20

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[adxmenu](https://git.spip.net/spip-contrib-extensions/adx_menu)|[2.0.0;4.*]|10|4.4;4.3;3.2;3.1;3.0;2.1|2024-07-13|
|[bonne_annee](https://git.spip.net/spip-contrib-extensions/bonne_annee)|[2.0.0;4.*]|17|4.4;4.3;3.2;3.1;2.1|2024-07-13|
|[colorscope](https://git.spip.net/spip-contrib-extensions/colorscope)|[2.0.0;3.2.99]|7|3.0;2.1;2.0|< 2023-02-23|
|[googleplus1](https://git.spip.net/spip-contrib-extensions/googleplus1)|[2.0.10;3.2.*]|37|3.2;3.1;3.0;2.1;2.0|< 2023-02-23|
|[joomla2spip](https://git.spip.net/spip-contrib-extensions/joomla2spip)|[2.0.0;3.0.*]|0|N/A|2023-06-21|
|[logo_spip](https://git.spip.net/spip-contrib-extensions/logo_spip)|[2.0.0;4.*]|2|3.2;3.1|2024-07-13|
|[scrut_prop](https://git.spip.net/spip-contrib-extensions/scrutin_liste_proportionnel)|[2.0.0;3.2.*]|1|3.0|< 2023-02-23|
|[piwik](https://git.spip.net/spip-contrib-extensions/spip_piwik)|[2.0.8;4.*]|39|4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0|2025-01-29|
|[tablematieres](https://git.spip.net/spip-contrib-extensions/table_matieres)|[2.0.0;3.2.*]|13|3.2;3.0;2.1|< 2023-02-23|
|[squelette_leadescent](https://git.spip.net/spip-contrib-squelettes/leadescent)|[2.0.0;4.*]|4|4.2;3.2;2.1|2024-07-13|
|[squelette_multiflex](https://git.spip.net/spip-contrib-squelettes/multiflex)|[2.0.0;4.*]|56|4.3;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-05|
|[squelette_multiflex](https://git.spip.net/spip-contrib-squelettes/oswd_3626_multiflex-3)|[2.0.0;4.*]|56|4.3;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-13|
|[timelineblog](https://git.spip.net/spip-contrib-squelettes/timeline_blog)|[2.0.0;3.2.*]|1|4.3|< 2023-02-23|
|[theme_einsteiniumist](https://git.spip.net/spip-contrib-themes/zpip_v1_Einsteiniumist)|[2.0.10;4.*]|8|3.2;3.0;2.1|2024-07-05|
|[theme_wuwei](https://git.spip.net/spip-contrib-themes/zpip_v1_Wu-wei)|[2.0.10.0;4.*]|0|N/A|2024-07-05|
