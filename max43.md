# max43

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[breves](https://git.spip.net/spip-contrib-extensions/breves)|[4.2.0;4.3.*]|2644|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-03|
|[intertitrestdm](https://git.spip.net/spip-contrib-extensions/intertitres_hierarchises_et_table_matieres)|[4.1.0;4.3.*]|23|3.2;3.1;3.0;2.1;2.0;1.9|2024-05-31|
|[wikidata](https://git.spip.net/spip-contrib-extensions/Wikidata)|[3.2.0;4.3.*]|0|N/A|< 2023-02-23|
|[bazar](https://git.spip.net/spip-contrib-squelettes/bazar)|[3.2.0;4.3.*]|1|4.3|2024-09-13|
|[odaiba](https://git.spip.net/spip-contrib-squelettes/odaiba)|[4.1.0;4.3.*]|2|4.3|2024-06-12|
