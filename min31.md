# min31

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[accessibilite](https://git.spip.net/spip-contrib-extensions/accessibilite)|[3.1.0;4.*]|261|4.3;4.2;4.1;3.2;3.1;3.0;2.1|2024-07-05|
|[accordion](https://git.spip.net/spip-contrib-extensions/accordion)|[3.1.0;4.*]|15|4.4;4.3;4.2;4.1;4.0;3.2|2024-07-13|
|[ajaxfiltre](https://git.spip.net/spip-contrib-extensions/ajaxfiltre)|[3.1.0;4.*]|13|4.4;4.3;4.2;4.0;3.2|2024-12-12|
|[amappca](https://git.spip.net/spip-contrib-extensions/amappca)|[3.1.0;3.2.*]|0|N/A|< 2023-02-23|
|[animatecss](https://git.spip.net/spip-contrib-extensions/animatecss)|[3.1.0;3.2.*]|0|N/A|< 2023-02-23|
|[article_pdf](https://git.spip.net/spip-contrib-extensions/article_pdf)|[3.1.0;4.*]|351|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-05|
|[ayantsdroit](https://git.spip.net/spip-contrib-extensions/ayants_droit)|[3.1.0;3.2.*]|0|N/A|< 2023-02-23|
|[carto_itineraire](https://git.spip.net/spip-contrib-extensions/carto_itineraire)|[3.1.0;4.*]|43|4.3|2024-07-05|
|[codes_postaux](https://git.spip.net/spip-contrib-extensions/codes_postaux)|[3.1.0;3.2.*]|2|4.0;3.2|< 2023-02-23|
|[colonne](https://git.spip.net/spip-contrib-extensions/colonne_raccourci)|[3.1.0;3.2.*]|0|N/A|< 2023-02-23|
|[coupons](https://git.spip.net/spip-contrib-extensions/coupons)|[3.1.0;4.*.*]|2|3.2|2023-10-24|
|[ck](https://git.spip.net/spip-contrib-extensions/couteau-kiss)|[3.1.0;4.*]|210|4.4;4.3;4.2;4.1;3.3;3.2;3.1;3.0;2.1;2.0|2025-01-29|
|[deckjs](https://git.spip.net/spip-contrib-extensions/deckjs)|[3.1.1;3.2.*]|0|N/A|< 2023-02-23|
|[declarerparent](https://git.spip.net/spip-contrib-extensions/declarerparent)|[3.1.0;4.*]|100|4.3;4.2;4.1;4.0;3.2;3.1|2024-07-05|
|[disposition](https://git.spip.net/spip-contrib-extensions/disposition)|[3.1.0;3.2.*]|1|3.2|< 2023-02-23|
|[domlang](https://git.spip.net/spip-contrib-extensions/domlang)|[3.1.0;3.2.*]|0|N/A|< 2023-02-23|
|[emb_pdf](https://git.spip.net/spip-contrib-extensions/emb_pdf)|[3.1.0;4.*]|122|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[formidable_imagecrop](https://git.spip.net/spip-contrib-extensions/formidable_imagecrop)|[3.1.0;3.2.*]|0|N/A|< 2023-02-23|
|[frimousses](https://git.spip.net/spip-contrib-extensions/frimousses)|[3.1.0;4.*]|27|4.4;4.3;4.2;3.2;3.1|2024-07-05|
|[gridle](https://git.spip.net/spip-contrib-extensions/gridle)|[3.1.0;4.*]|3|4.2;4.0;3.2|2024-07-05|
|[htmlminifier](https://git.spip.net/spip-contrib-extensions/htmlminifier)|[3.1.0;4.*]|15|4.3;4.2;4.0;3.2|2024-07-05|
|[inscription3](https://git.spip.net/spip-contrib-extensions/inscription)|[3.1.0;4.*]|39|4.3;4.2;3.2;3.1;3.0;2.1|2024-11-18|
|[logos_roles](https://git.spip.net/spip-contrib-extensions/logos_roles)|[3.1.1;3.2.*]|5|3.2|< 2023-02-23|
|[logo_auto](https://git.spip.net/spip-contrib-extensions/logo_auto)|[3.1.0;4.*]|7|4.3;4.2;3.2|2024-10-18|
|[magnet](https://git.spip.net/spip-contrib-extensions/magnet)|[3.1.0;4.*]|33|4.4;4.3;4.2;3.2;3.1|2024-07-05|
|[markdown](https://git.spip.net/spip-contrib-extensions/markdown)|[3.1.0;4.*]|18|4.4;4.3;4.2;4.1;3.2;3.1|2024-07-05|
|[maxigos](https://git.spip.net/spip-contrib-extensions/maxigos)|[3.1.0;3.2.*]|1|3.2|< 2023-02-23|
|[mesfavoris](https://git.spip.net/spip-contrib-extensions/mesfavoris)|[3.1.0;4.*]|38|4.4;4.3;4.2;4.1;4.0;3.2;3.1;2.1|2025-01-29|
|[mosaique_ordoc](https://git.spip.net/spip-contrib-extensions/mosaique_ordoc)|[3.1.3;3.2.*]|2|3.2|< 2023-02-23|
|[nospam](https://git.spip.net/spip-contrib-extensions/nospam)|[3.1.0;4.*]|1981|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1;2.0;1.9|2025-01-29|
|[objets_virtuels](https://git.spip.net/spip-contrib-extensions/objets_virtuels)|[3.1.4;4.*]|46|4.4;4.3;4.2;4.1;3.2;3.1|2024-07-21|
|[owncloud](https://git.spip.net/spip-contrib-extensions/owncloud)|[3.1.0;4.0.*]|0|N/A|2024-09-12|
|[previsup](https://git.spip.net/spip-contrib-extensions/previsup)|[3.1.0;4.*]|46|4.3|2024-07-05|
|[prive_affiche_connexe](https://git.spip.net/spip-contrib-extensions/prive_affiche_connexe)|[3.1.0;3.2.*]|0|N/A|< 2023-02-23|
|[roles](https://git.spip.net/spip-contrib-extensions/roles)|[3.1.0;4.*]|122|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[rubriques_virtuelles](https://git.spip.net/spip-contrib-extensions/rubriques_virtuelles)|[3.1.0;4.*]|15|4.3;4.2;3.2|2024-07-13|
|[speedsyndic](https://git.spip.net/spip-contrib-extensions/speedsyndic2)|[3.1.6;3.2.*]|5|3.2;2.1;1.9|< 2023-02-23|
|[configs](https://git.spip.net/spip-contrib-extensions/spip_configs)|[3.1.6;4.*]|2|4.3;3.2|2024-07-05|
|[spout](https://git.spip.net/spip-contrib-extensions/spout)|[3.1.8;4.*]|150|4.4;4.3;4.2;4.1;4.0;3.2|2024-07-05|
|[switchcase](https://git.spip.net/spip-contrib-extensions/switchcase)|[3.1.0;4.*]|19|4.3;4.2;4.1;3.2|2024-07-05|
|[syndic_etendue](https://git.spip.net/spip-contrib-extensions/syndic_etendue)|[3.1.5;3.2.*]|0|N/A|< 2023-02-23|
|[tabcsv](https://git.spip.net/spip-contrib-extensions/tabcsv)|[3.1.0;3.2.*]|5|3.2|< 2023-02-23|
|[tarteaucitron](https://git.spip.net/spip-contrib-extensions/tarteaucitron)|[3.1.0;4.*]|145|4.4;4.3;4.2;4.1;4.0;3.2;3.1|2024-12-04|
|[timecircles](https://git.spip.net/spip-contrib-extensions/timecircles)|[3.1.0;4.*]|0|N/A|2024-07-05|
|[videos](https://git.spip.net/spip-contrib-extensions/videos)|[3.1.0;4.1.*]|135|4.3;4.2;4.1;3.2;3.1;3.0;2.1|2024-07-29|
|[moulinette](https://git.spip.net/spip-contrib-squelettes/moulinette)|[3.1.0-dev;4.*]|8|4.3;4.1;3.1|2024-07-13|
|[suip](https://git.spip.net/spip-contrib-squelettes/suip)|[3.1.0;4.*]|0|N/A|2024-07-05|
|[dist_theme_debug](https://git.spip.net/spip-contrib-themes/dist_v1_debug)|[3.1.0-dev;4.*]|0|N/A|2024-07-05|
|[dist_theme_distyle](https://git.spip.net/spip-contrib-themes/dist_v1_distyle)|[3.1.0-dev;4.*]|2|4.3;3.2|2024-07-05|
|[dist_theme_palatinesque](https://git.spip.net/spip-contrib-themes/dist_v1_palatinesque)|[3.1.0-dev;4.*]|0|N/A|2024-07-05|
|[dist_theme_penscratch](https://git.spip.net/spip-contrib-themes/dist_v1_penscratch)|[3.1.0-dev;4.*]|2|4.3;4.2|2024-07-05|
|[dist_theme_roubaix](https://git.spip.net/spip-contrib-themes/dist_v1_roubaix)|[3.1.0-dev;4.*]|1|4.0|2024-07-05|
|[dist_theme_tonton](https://git.spip.net/spip-contrib-themes/dist_v1_tonton)|[3.1.0-dev;4.*]|0|N/A|2024-07-05|
|[dist_theme_violette](https://git.spip.net/spip-contrib-themes/dist_v1_violette)|[3.1.0-dev;4.*]|3|4.3;4.2|2024-07-05|
|[dist_theme_waz](https://git.spip.net/spip-contrib-themes/dist_v1_waz)|[3.1.0-dev;4.*]|4|4.3;3.2|2024-07-05|
|[galactic_spip_net](https://git.spip.net/spip-galaxie/galactic_spip_net)|[3.1.0;4.*]|2|4.4|2025-01-29|
