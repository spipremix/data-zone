<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/galactic_programmer.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'galactic_programmer_description' => '',
	'galactic_programmer_nom' => 'Galactic - programmer.spip.net',
	'galactic_programmer_slogan' => 'Surcharge le squelette Galactic'
);
