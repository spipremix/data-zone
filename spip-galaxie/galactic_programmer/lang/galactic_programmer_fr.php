<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/galactic_programmer.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Auteur :',

	// C
	'champ_auteur' => 'Auteur(s)',
	'champ_date' => 'Date',
	'champ_id' => 'Id',
	'champ_langue' => 'Lang',
	'champ_resume' => 'Resumé',
	'champ_texte' => 'Texte',
	'champ_titre' => 'Titre',
	'chapitre' => 'Chapitre : ',
	'choisir' => 'Choisir...',
	'commentaire' => '@nb@ commentaire',
	'commentaire_aucun' => 'Aucun commentaire',
	'commentaires' => '@nb@ commentaires',
	'commentez' => 'Commentez la documentation',
	'creer_nouvelle_suggestion' => 'Proposer une nouvelle suggestion',

	// D
	'description' => 'Description',
	'documentation_papier' => 'Documentation papier !',
	'documentation_papier_complement' => 'Pour lire à tête reposée...',

	// E
	'editer_suggestion' => 'Editer cette suggestion',
	'en_savoir_plus' => 'En savoir plus !',
	'erreur_de_chargement_ajax' => 'Erreur de chargement AJAX !',
	'erreur_inscription_desactivee' => 'Les inscriptions sont désactivées sur ce site.',
	'erreur_inscription_session' => 'Vous êtes déjà identifié.',
	'exemple' => 'Exemple',
	'exercice' => 'Exercice',

	// I
	'icones_par' => 'Icones adaptées du thème ',
	'integrale' => 'L’intégrale !',

	// L
	'label_charger_url' => 'Accès rapide :',
	'label_exemple' => 'Exemple',
	'label_exercice' => 'Exercice',
	'label_reponse' => 'Réponse',
	'lien_sedna' => 'Les sites que nous suivons',
	'lien_sedna_img' => 'Sedna',

	// M
	'maj' => 'Révision du ',
	'mentions_legales' => 'Mentions légales',

	// N
	'nom' => 'Nom',
	'nouvelle_suggestion' => 'Nouvelle suggestion',

	// P
	'precedent' => 'Précédent',
	'proposer_suggestion' => 'Proposez une amélioration !',
	'proposer_suggestion_img' => 'Gestionnaire de tickets',

	// R
	'reponse' => 'Réponse',

	// S
	'signaler_coquille' => 'Signalez une coquille…',
	'sinscrire' => 'S’inscrire',
	'sommaire' => 'Contenu',
	'sommaire_livre' => 'Sommaire',
	'sous_licence' => 'sous licence',
	'suggestion' => 'Suggestion',
	'suggestions' => 'Suggestions',
	'suivant' => 'Suivant',
	'suivi' => 'Suivi',
	'suivi_dernieres_modifications_articles' => 'Dernières modifications des articles',
	'suivi_derniers_articles' => 'Derniers articles',
	'suivi_derniers_articles_proposes' => 'Derniers articles proposés',
	'suivi_derniers_commentaires' => 'Derniers commentaires',
	'suivi_description' => 'Suivi du site...',
	'symboles' => 'Symboles',

	// T
	'table_des_matieres' => 'Table des matières',
	'tickets_sur_inscription' => '
		L’écriture des tickets ou commentaires n’est
		possible qu’aux personnes identifiées.
	',
	'titre_articles_lies' => 'Articles complémentaires',
	'titre_identification' => 'Identification',
	'titre_inscription' => 'Inscription',
	'tout_voir' => 'Tout voir'
);
