<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/galactic.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'galactic_description' => '',
	'galactic_nom' => 'Galactic',
	'galactic_slogan' => 'Un squelette pour les sites de la galaxie'
);
