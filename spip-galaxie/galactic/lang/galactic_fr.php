<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/galactic.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'evitement_contenu' => 'Aller au contenu',
	'evitement_navigation' => 'Aller à la navigation',

	// G
	'galactic_titre' => 'Galactic',

	// L
	'licence' => 'Licence :',

	// M
	'mis_a_jour' => 'Mis à jour :',
	'mots_cles' => 'Mots clés',

	// P
	'publie_le' => 'Publié le :',

	// R
	'recherche' => 'Recherche',

	// T
	'traductions' => 'Traductions :',

	// V
	'vous_etes_ici' => 'Vous êtes ici :'
);
