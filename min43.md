# min43

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[debardeur](https://git.spip.net/spip-contrib-extensions/debardeur)|[4.3.0;4.*]|4|4.4|2025-01-28|
|[zotspip](https://git.spip.net/spip-contrib-extensions/zotspip)|[4.3.0;4.*]|17|4.4;4.3;4.2;4.0;3.2;3.1;3.0;2.1|2024-11-23|
|[oshiage](https://git.spip.net/spip-contrib-squelettes/oshiage)|[4.3.0;4.*]|2|4.3|2024-12-06|
|[tiles](https://git.spip.net/spip-contrib-squelettes/tiles)|[4.3.0;4.*]|0|N/A|2025-02-14|
|[ginza](https://git.spip.net/spip-contrib-squelettes/ginza)|[4.3.0;4.*]|1|4.3|2025-01-17|
|[qrcode_form](https://git.spip.net/spip-contrib-extensions/qrcode_form)|[4.3.0;4.*.*]|3|4.4;4.3|2024-11-16|
|[image_typo](https://git.spip.net/spip-contrib-extensions/image_typo)|[4.3.0;5.*.*]|1|4.4|2024-10-03|
|[html5up_verti](https://git.spip.net/spip-contrib-squelettes/html5up_verti)|[4.3.3;4.*]|0|N/A|2024-10-25|
|[adminergobandeau](https://git.spip.net/spip-contrib-extensions/adminergobandeau)|[4.3.4;4.*]|60|4.3|2024-11-25|
|[openseadragon](https://git.spip.net/spip-contrib-extensions/openseadragon)|[4.3.0;4.*]|1|4.3|2024-12-13|
|[statut_article_archive](https://git.spip.net/spip-contrib-extensions/statut_article_archive)|[4.3.0;4.*]|0|N/A|2025-01-27|
|[themefisher_pinwheel](https://git.spip.net/spip-contrib-squelettes/pinwheel)|[4.3.4;4.*]|0|N/A|2025-01-20|
