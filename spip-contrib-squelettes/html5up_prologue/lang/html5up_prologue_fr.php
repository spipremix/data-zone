<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accueil_article' => 'Article de la page d\'accueil',
	'accueil_article_explication' => 'Choisissez l\'accueil qui apparait sur la page d\'accueil sous la grille',
	'accueil_mode' => 'Mode de la page d\'accueil',
	'accueil_mode_blog' => 'Mode blog (derniers articles publiés)',
	'accueil_mode_explication' => 'Choisissez ce qui doit apparaître dans la grille',
	'accueil_mode_site' => 'Mode site (rubriques à la racine)',
	'accueil_mode_portfolio' => 'Mode portfolio (galerie des images du site)',
	'accueil_nb_grille' => 'Nombre d\'items dans la grille',
	'accueil_titre_grille' => 'Titre de la grille d\'accueil',
	'accueil_titre_grille_explication' => 'Choisissez le titre qui apparaîtra au dessus de la grille d\'accueil (laisser vide pour l\'affichage par défaut)',
	'accueil_visuel_entete' => 'Visuel d\'entête',
	'accueil_visuel_entete_explication' => 'Le visuel qui apparait en entête de lapage d\'accueil',

	// C
	'configurer_html5up_prologue' => 'Configurer HTML5up Prologue',

	// H
	'html5up_prologue_titre' => 'HTML5up Prologue',

	// T
	'theme_graphique_par_html5up' => 'Thème graphique par HTML5up',
	'titre_grille_blog' => 'Blog',
	'titre_grille_portfolio' => 'Portfolio',
	'titre_grille_rubriques' => 'Rubriques',

	// V
	'voir_la_suite' => 'Voir la suite',

);