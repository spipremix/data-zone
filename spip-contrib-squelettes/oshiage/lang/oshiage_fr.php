<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

//
// spip-core
//
'accueil_site' => 'Accueil',

// 0-9
'404_sorry' => 'Désolé, cette page n\'existe plus.',

// A
'articles_tous' => 'Tous les articles',

// B
'oshiage_type_rubrique' => 'Type de rubrique',
'oshiage_type_rubrique_tri_date' => 'Articles listés par date (les articles les récents en premier)',
'oshiage_type_rubrique_tri_date_archive' => 'Articles listés par date et par année',
'oshiage_type_rubrique_tri_num' => 'Articles listés par numéro (10. xxx, 20. yyy, ...)',
'oshiage_rubrique_surtitre' => 'Surtitre',
'oshiage_rubrique_surtitre_explication' => '(Facultatif) Permet d\'afficher une phrase courte au dessus du titre notamment sur le page d\'accueil',
'oshiage_rubrique_titre_long' => 'Titre long',
'oshiage_rubrique_titre_long_explication' => '(Facultatif) Permet d\'afficher un titre long notamment sur le page d\'accueil',


// C
'cacher_a2a' => 'Ne pas afficher de suggestions en bas d\'article',
'cfg_color_white' => 'Blanc',
'cfg_color_black' => 'Noir',
'cfg_color_primary_color' => 'Couleur principale',
'cfg_color_primary_color_hover' => 'Couleur principale (au survol)',
'cfg_color_primary_color_light' => 'Couleur principale (clair)',
'cfg_color_gray' => 'Gris',
'cfg_color_gray_light' => 'Gris (clair)',
'cfg_color_gray_super_light' => 'Gris (très clair)',
'cfg_color_green' => 'Vert',
'cfg_color_green_dark' => 'Vert (sombre)',
'cfg_color_green_light' => 'Vert (clair)',
'cfg_color_blue_light' => 'Bleu (clair)',
'cfg_color_purple' => 'Violet',
'cfg_color_purple_dark' => 'Violet (sombre)',
'cfg_color_purple_light' => 'Violet (clair)',
'cfg_titre_parametrages' => 'Configuration du site Oshiage',
'cfg_homepage' => 'Page d\'accueil',
'cfg_home_soutenir' => 'Article Nous Soutenir',
'cfg_home_partenaires' => 'Article Partenaires',
'cfg_home_ressource' => 'Rubrique Ressources',
'cfg_home_formation' => 'Rubrique Formations',
'cfg_home_reassurance' => 'Rubrique de présentation (réassurance)',
'cfg_home_actu' => 'Rubrique Actualités',
'cfg_footer' => 'Pied de page',
'cfg_footer_contact' => 'Liens > Contact ',
'cfg_footer_col1' => 'Liens > Colonne 1',
'cfg_footer_col2' => 'Liens > Colonne 2',
'cfg_footer_col3' => 'Liens > Colonne 2bis',
'cfg_footer_copyright_articles' => 'Liens ligne copyright',
'cfg_footer_copyright_articles_explication' => ' 2ème ligne du pied : ajouter les liens techniques (crédits, mentions légales ...)',
'cfg_footer_adresse' => 'Adresse et téléphone',
'cfg_footer_baseline' => 'Nom de la structure ou Baseline',
'cfg_rezo' => 'Réseaux sociaux',
'cfg_rezo_facebook' => 'Facebook',
'cfg_rezo_twitter' => 'Twitter',
'cfg_rezo_linkedin' => 'LinkedIn',
'cfg_rezo_youtube' => 'Youtube',
'cfg_rezo_instagram' => 'Instagram',
'cfg_menu' => 'Bannière et menu',
'cfg_menu_rubriques' => 'Menu',
'cfg_menu_rubriques_explication' => 'Rubriques qui constituent le menu. Limiter le nombre à 4 maximum',
'cfg_intro' => 'Cette page vous permet de personnaliser votre squelette Oshiage',
'cfg_lien_doc' => 'Documentation en ligne',
'cfg_palette' => 'Palette',
'cfg_palette_explication' => 'Modifier le jeu des couleurs utilisées par Oshiage',
'cfg_palette_explication_suite' => 'Après avoir enregistré vos couleurs (en appuyant sur le bouton enregistrer en bas de ce formulaire), pensez à vider votre cache pour mettre à jour la feuille de style du site publique',
'cfg_page_demo' => 'Le squelette est livré avec une page démonstration qui permet de tester la mise en page avec des contenus factices :',
'cfg_page_demo_article' => 'Démo article',
'cfg_url_don' => 'Article du bouton "nous soutenir"',
'contact' => 'Contact',

// D
'decouvrir' => 'Découvrir',


// E
'et_aussi' => 'Voir aussi&nbsp;...',
'en_savoir_plus' => 'En savoir plus',

// F
'faire_don' => 'Nous soutenir',
'footer_titre_col1' => 'L\'association',
'footer_titre_col2' => 'Actualités de l\'association',
'footer_titre_col3' => 'Ressources',
'footer_titre_contact' => 'Contact',


// L
'lire_la_suite' => 'Lire la suite',
'lire_la_suite_decouvrir' => 'Découvrir',

// M
'menu' => 'Menu',
'mis_a_jour' => 'Mis à jour le',

// N
'newsletter_votre_email' => 'Votre email',


// O
'ours' => 'Le saviez-vous ?<br />Le nom du squelette <strong>Oshiage</strong> (押上) vient d\'un quartier de Tōkyō, célèbre pour sa tour Skytree, silhouette iconique du panorama du Nord-Est de la capitale nippone.',

// P
'publie_le' => 'Publié le',
'par' => 'par',
'pagination_pages' => 'Pages',
'pagination_gd_total' => 'articles disponibles',
'pagination_environ' => 'Environ',
'portfolio' => 'Portfolio',


// R
'resultats_out' => 'Résultat(s) disponible(s)',
'recherche_site' => 'Résultat sur  : ',
'recherche_recherche' => 'Rechercher sur le site',
'recherche_archive' => 'Rechercher',
'recherche_nomatch' => 'Désolé, aucun résultat disponible sur cette recherche ! ',
'resultats_articles' => 'Recherche sur les articles',
'recherche_dans_rubrique' => 'Rechercher dans cette rubrique',
'recherche_resultat' => 'Résultats de la recherche sur',
'recherche_titre' => 'Rechercher',
'recherche_cancel' => 'annuler cette recherche',
'retour_liste' => 'Retour à la liste',
'resultats' => '&nbsp;résultat(s)',


// T
'top' => 'Haut de page',
'titre_page_configurer_oshiage' => 'Configurer Oshiage',


];
