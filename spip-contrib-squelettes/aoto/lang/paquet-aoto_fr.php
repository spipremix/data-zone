<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'aoto_description' => 'Un squelette simple pour présenter un portfolio de photographies grâce à Fancybox 3',
	'aoto_slogan' => 'Squelette élégant pour vos photos',
);
