<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'article_intro_accueil' => 'Article d\'intro sur l\'accueil',
	'article_intro_accueil_explication' => 'L\'article utilisé en intro de la page d\'accueil',
	'article_intro_sidebar' => 'Article d\'intro de la sidebar',
	'article_intro_sidebar_explication' => 'L\'article utilisé en intro de la sidebar',
	'article_pied' => 'Article de pied de page',
	'article_pied_explication' => 'L\'article utilisé en pied de page',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',

	// E
	'entete' => 'Entête',

	// F
	'focus' => 'Focus',

	// G
	'graphisme' => 'Graphisme',
	'grille' => 'Grille',

	// H
	'html5up_escape_velocity_titre' => 'Html5up Escape Velocity',

	// I
	'image_entete' => 'Image d\'entête',
	'image_entete_explication' => "Numéro du document image utilisé en entête. Dans le template original, l'image a une taille de 1920x653 pixels.",
	'intro' => 'Intro',

	// M
	'masquer_texte_entete' => 'Masquer le texte',
	'masquer_texte_entete_texte' => 'Le nom du site et le slogan ne doivent pas s\'afficher dans le bandeau',
	'mode_accueil' => 'Mode de la page d\'accueil',
	'mode_accueil_blog' => 'Mode blog (derniers articles publiés)',
	'mode_accueil_explications' => 'Choisissez ce qui doit apparaître dans la grille',
	'mode_accueil_site' => 'Mode site (rubriques à la racine)',

	// P
	'pied_de_page' => 'Pied de page',
	
	// R
	'rubrique_focus' => 'Rubrique à mettre en avant',
	'rubrique_focus_explication' => 'La rubrique dont 3 articles seront mis en avant',
	'rubrique_focus_ordre' => 'Ordre des articles',
	'rubrique_focus_ordre_explications' => 'Choisissez l\'ordre d\'affichage des articles',
	'rubrique_focus_ordre_defaut' => 'Ordre par défaut (par num titre)',
	'rubrique_focus_ordre_antechronologique' => 'Ordre antéchronologique (!par date)',
	'rubrique_sidebar' => 'Rubrique à afficher',
	'rubrique_sidebar_explication' => 'La rubrique dont 3 articles seront affichés dans la sidebar',
	'rubrique_sidebar_ordre' => 'Ordre des articles',
	'rubrique_sidebar_ordre_explications' => 'Choisissez l\'ordre d\'affichage des articles',
	'rubrique_sidebar_ordre_defaut' => 'Ordre par défaut (par num titre)',
	'rubrique_sidebar_ordre_antechronologique' => 'Ordre antéchronologique (!par date)',
	'rubrique_sidebar_titre' => 'Titre de la rubrique affichée',
	'rubrique_sidebar_titre_explications' => 'Le titre qui apparaîtra au dessus des articles',
		
	// S
	'sidebar' => 'Sidebar',

	// T
	'theme_graphique_par_html5up' => 'Thème graphique par HTML5 UP',
	'titre_grille_accueil' => 'Titre de la grille',
	'titre_grille_accueil_explications' => 'Le titre qui apparaîtra au dessus de la grille',
	'titre_focus_accueil' => 'Titre du focus',
	'titre_focus_accueil_explications' => 'Le titre qui apparaîtra au dessus des articles mis en avant',
	'titre_page_configurer_html5up_escape_velocity' => 'Configurer le squelette HTML5up Escape Velocity',

	// V
	'voir_tout' => 'Voir tout',
);