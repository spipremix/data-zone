<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'html5up_escape_velocity_description' => 'Adaptation pour SPIP du squelette «Escape Velocity» de html5up https://html5up.net/escape-velocity',
	'html5up_escape_velocity_nom' => 'Html5up «Escape Velocity»',
	'html5up_escape_velocity_slogan' => 'Squelette responsive «Escape Velocity» de HTML5UP',
);
