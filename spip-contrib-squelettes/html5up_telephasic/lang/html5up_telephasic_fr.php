<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accueil_cta' => 'Entrée "call to action"',
	'accueil_cta_explication' => 'Sélectionner un article ou une rubrique',
	'apparence_entete' => 'Apparence de l\'entête',
	'apparence_entete_couleurs' => 'Afficher un dégradé de couleurs',
	'apparence_entete_couleur1' => '1ère couleur',
	'apparence_entete_couleur1_explication' => 'Par défaut :  #FF7088 (hexadécimal) / 255, 112, 136 (RVB)',
	'apparence_entete_couleur2' => '2nde couleur',
	'apparence_entete_couleur2_explication' => 'Par défaut :  #F2B69D (hexadécimal) / 242, 182, 157 (RVB)',
	'apparence_entete_image' => 'Afficher une image',
	'apparence_entete_image_label' => 'Image d\'entête',
	'apparence_entete_image_explication' => "Numéro du document image utilisé en entête. Taille minimale : 2000px de large",
	'article_pied' => 'Article de pied de page',
	'article_pied_explication' => 'L\'article utilisé en pied de page',

	// C
	'cta' => 'Call to action',
	'cfg_titre_parametrages' => 'Paramétrages',
	'couleur_emphase' => 'Couleur d\'emphase',
	'couleur_emphase_explication' => 'Par défaut : #f35858 (hexadécimal) / 243, 88, 88 (RVB)',

	// E
	'entete' => 'Entête',

	// F
	'focus' => 'Focus',

	// G
	'graphisme' => 'Graphisme',
	'generalites' => 'Généralités',

	// H
	'html5up_telephasic_titre' => 'HTML5up Telephasic',

	// P
	'pied_de_page' => 'Pied de page',

	// R
	'rubrique_accueil_focus' => 'Rubrique à mettre en avant',
	'rubrique_accueil_focus_explication' => 'La rubrique dont 3 articles seront mis en avant',
	'rubrique_accueil_focus_ordre' => 'Ordre des articles',
	'rubrique_accueil_focus_ordre_explications' => 'Choisissez l\'ordre d\'affichage des articles',
	'rubrique_accueil_focus_ordre_defaut' => 'Ordre par défaut (par num titre)',
	'rubrique_accueil_focus_ordre_antechronologique' => 'Ordre antéchronologique (!par date)',

	// T
	'theme_graphique_par_html5up' => 'Thème graphique par HTML5 UP',
	'titre_page_configurer_html5up_telephasic' => 'Configurer le squelette HTML5up Telephasic',

	// V
	'voir_tout' => 'Voir tout',
	
);