<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

//
// spip-core
//
'accueil_site' => 'Accueil',

// 0-9
'404_sorry' => 'Désolé, cette page n\'existe plus.',

// A
'actu' => 'Actualités',
'auteurs_les' => 'Les auteurs',
'articles_tous' => 'Tous les articles',



// C
'cfg_nb_articles_actu' => 'Nombre de cartes actualités',
'cfg_nb_articles_actu_explication' => 'Indiquer le nombre d\'articles à lister à Une. Valeur par défaut: 3. Il est conseillé d\'utiliser un multiple de 3.',
'cfg_home_outro' => 'Article de présentation générale',
'cfg_home_outro_explication' => 'Article en bas de la page d\'accueil',
'cfg_home_rubrique_outro' => 'Rubrique focus',
'cfg_home_rubrique_outro_explication' => 'Rubrique dont on met le dernier article en focus',
'cfg_article_contact' => 'Article contact',
'cfg_homepage' => 'Page d\'accueil',
'cfg_footer' => 'Pied de page',
'cfg_menu' => 'Bannière et menu',
'cfg_afficher_moteur_recherche' => 'Moteur de recherche',
'cfg_afficher_moteur_recherche_case' => 'Afficher le moteur de recherche',
'cfg_afficher_accueil' => 'Menu',
'cfg_afficher_accueil_case' => 'Afficher le lien "accueil"',
'cfg_footer_adresse' => 'Texte de la colonne centrale (adresse, copyright, ...)',
'cfg_titre_parametrages' => 'Choisir les contenus à afficher',
'cfg_footer_articles_contact' => 'Contact',
'cfg_footer_articles_contact_explication' => 'Article qui sert à vous contacter',
'cfg_footer_articles' => 'Liens',
'cfg_footer_articles_explication' => '(Facultatif) Articles à placer en liens. Il est conseillé de ne pas dépasser 2 ou 3 liens avec des titres courts',
'cfg_intro' => 'Cette page vous permet de personnaliser votre squelette Kamakura',

'cfg_lien_doc' => 'Documentation en ligne',
'cfg_page_demo' => 'Le squelette est livré avec une page démonstration qui permet de tester la mise en page avec des contenus factices :',
'cfg_page_demo_article' => 'Démo article',


// E
'en_savoir_plus' => 'En savoir plus',

// L
'lire_la_suite' => 'Lire la suite',


// M
'menu' => 'Menu',


// O
'ours' => 'Le saviez-vous ?<br />Le nom du squelette <strong>Kamakura</strong> (鎌倉市) vient d\'une station balnéaire près de Tōkyō célèbre pour sa plage et son grand bouddha. C\'est aussi la ville où se déroule le livre <i>La Papeterie Tsubaki</i> d\'Ito Ogawa',


// P
'publie_le' => 'Publié le',
'par' => 'par',
'pagination_pages' => 'Pages',
'pagination_gd_total' => 'articles disponibles',
'posted' => 'Posté le',
'posted' => 'Dernière modification de cette page',


// R
'resultats_out' => 'Résultat(s) disponible(s)',
'recherche_site' => 'Résultat sur  : ',
'recherche_recherche' => 'Rechercher sur le site',
'recherche_nomatch' => 'Désolé, aucun résultat disponible sur cette recherche ! ',
'resultats_articles' => 'Recherche sur les articles',
'recherche_resultat' => 'Résultats de la recherche sur',
'recherche_titre' => 'Rechercher',

// S
'kamakura_type_rubrique' => 'Type de rubrique',
'kamakura_type_rubrique_tri_date' => 'Articles listés par date (les articles les récents en premier)',
'kamakura_type_rubrique_tri_num' => 'Articles listés par numéro (10. xxx, 20. yyy, ...)',


// T
'top' => 'Haut de page',
'titre_page_configurer_kamakura' => 'Configurer Kamakura',


// V
'voir_aussi' => 'Voir aussi',
'videos_plus' => 'Plus d\'articles',


// X
'xxx' => '',




];
