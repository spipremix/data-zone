<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'charger_polices' => 'Polices du thème (Raleway et Source Sans Pro)',
	'charger_polices_explication' => 'Ne pas charger les fichiers du thème (si vous souhaitez les surcharger)',
	'configurer_html5up_future_imperfect' => 'Configurer Html5up Future Imperfect',

	// H
	'html5up_future_imperfect_titre' => 'Html5up Future Imperfect',

	// M
	'menu' => 'Menu',

	// R
	'rubrique_blog' => 'Rubrique blog',
	'rubrique_blog_explication' => 'Choisir la rubrique qui contient les articles à afficher sur l\'accueil',

	// T
	'theme_graphique_par_html5up' => 'Thème graphique par HTML5up',

	// V
	'voir_tous_les_articles' => 'Voir tous les articles',

);