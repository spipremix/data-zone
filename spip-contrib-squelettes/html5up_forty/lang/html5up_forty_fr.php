<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'article_pied' => 'Article de pied de page',
	'article_pied_explication' => 'L\'article utilisé en pied de page',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'contenu' => 'Contenu',

	// H
	'html5up_forty_titre' => 'Html5up Forty',

	// I
	'image_agenda_accueil' => 'Image de l\'agenda sur l\'accueil',
	'image_agenda_accueil_explication' => "Numéro du document image utilisé si les évennements sont activés.",
	'image_entete_accueil' => 'Image d\'entête de l\'accueil',
	'image_entete_accueil_explication' => "Numéro du document image utilisé en entête de l'accueil. Dans le template original, l'image a une taille de 1440x900 pixels.",

	// M
	'mode_accueil' => 'Mode de la page d\'accueil',
	'mode_accueil_blog' => 'Mode blog (derniers articles publiés)',
	'mode_accueil_explications' => 'Choisissez le mode d\'affichage de la page d\'accueil',
	'mode_accueil_site' => 'Mode site (rubriques à la racine)',

	// P
	'publie_le' => 'Publié le ',


	// T
	'theme_graphique_par_html5up' => 'Thème graphique par HTML5 UP',
	'titre_page_configurer_html5up_forty' => 'Configurer le squelette HTML5up Forty',
);