<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'html5up_forty_description' => 'Adaptation pour SPIP du squelette «FORTY» de html5up https://html5up.net/forty',
	'html5up_forty_nom' => 'Html5up FORTY',
	'html5up_forty_slogan' => 'Squelette responsive «FORTY» de HTML5UP',
);
