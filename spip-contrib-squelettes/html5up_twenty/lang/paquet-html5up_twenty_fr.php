<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'html5up_twenty_description' => 'Adaptation pour SPIP du squelette «Twenty» de html5up https://html5up.net/twenty',
	'html5up_twenty_nom' => 'Html5up «Twenty»',
	'html5up_twenty_slogan' => 'Squelette responsive «Twenty» de HTML5UP',
);
