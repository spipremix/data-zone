<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accueil_entete' => 'Entête',
	'accueil_entete_nom_site' => 'Afficher le nom du site et le slogan dans le bandeau',
	'accueil_entete_logo_site' => 'Afficher le logo du site uniquement',
	'accueil_entete_masquer' => 'Ne rien afficher du tout',
	'accueil_rubrique_special' => 'Rubrique mise en avant',
	'accueil_rubrique_special_explication' => 'La rubrique à afficher sous forme de grille',
	'accueil_rubrique_special_tri' => 'Ordre des articles dans la rubrique mise en avant',
	'accueil_rubrique_special_tri_defaut' => 'Par num titre (type site)',
	'accueil_rubrique_special_tri_inverse_chrono' => 'Ordre inverse chronologique (type blog)',
	'article_intro_accueil' => 'Article d\'intro sur l\'accueil',
	'article_intro_accueil_explication' => 'L\'article utilisé en intro de la page d\'accueil',
	'article_hero_accueil' => 'Article mis en avant sur l\'accueil',
	'article_hero_accueil_explication' => 'L\'article qui sera mis en avant sur la page d\'accueil',

	'article_cta_accueil' => 'Article call to action',
	'article_cta_accueil_explication' => 'L\'article call to action en bas de page',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'charger_polices' => 'Police du thème (Lato)',
	'charger_polices_explication' => 'Ne pas charger les fichiers du thème (si vous souhaitez les surcharger)',

	// E
	'entete' => 'Entête',

	// H
	'html5up_twenty_titre' => 'Html5up Twenty',

	// I
	'image_entete' => 'Image d\'entête',
	'image_entete_explication' => "Numéro du document image utilisé en entête. Dans le template original, l'image a une taille de 1920x653 pixels.",
	'intro' => 'Intro',
	
	// M
	'menu' => 'Menu',

	// P
	'picto_fa' => 'Picto Fontawesome dans l\'enête',
	'picto_fa_explication' => 'Indiquez ici le nom du picto que souhaitez voir afficher au dessus du titre sans le préfixe (fas, fab, fal...)',
	'picto_fa_placeholder' => 'grip-horizontal',

	// S
	'suite' => 'Suite',

	// T
	'theme_graphique_par_html5up' => 'Thème graphique par HTML5 UP',
	'titre_page_configurer_html5up_twenty' => 'Configurer le squelette HTML5up Twenty',
);