<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// S
	'soyezcreateurs_description' => 'Ce plugin active ou désactive le squelette {{SoyezCréateurs}}.

Un squelette
-* conforme aux bonnes pratiques en matière d\'accessibilité,
-* à destination des
-** associations,
-** collectivités,
-** entreprises,
-** particuliers

Pour disposer d\'un site SPIP incluant des fonctions génériques, largement configurable et adaptable.',
	'soyezcreateurs_slogan' => 'Largement configurable et adaptable, générique, conforme aux normes d\'accessibilité et RWD',
];
