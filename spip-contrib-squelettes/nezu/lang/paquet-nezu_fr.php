<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'nezu_description' => 'Un squelette sous Bootstrap 4',
	'nezu_slogan' => 'Squelette éditorial sous Bootstrap 4 compatible avec le plugin agenda',
);
