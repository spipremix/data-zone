<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

//
// spip-core
//
'accueil_site' => 'Accueil',

// 0-9
'404_sorry' => 'Désolé, cette page n\'existe plus.',

// A
'articles_tous' => 'Tous les articles',
'adresse' => 'Adresse',


// B
'but_faq_toutes' => 'Toutes vos questions',
'but_actu_plus' => 'Toutes les actualités',
'but_savoir_plus' => 'En savoir plus',
'but_evenement_tous' => 'Toutes les dates',
'but_evenement_agenda' => 'Agenda',
'but_evenement_inscrire' => 'S\'inscrire ',


// C
'cacher_a2a' => 'Ne pas afficher de suggestions en bas d\'article',
'cacher_date' => 'Ne pas afficher la date de cet article',
'cfg_color_white' => 'Blanc',
'cfg_color_black' => 'Noir',
'cfg_color_primary_color' => 'Couleur principale',
'cfg_color_primary_color_hover' => 'Couleur principale (au&nbsp;survol)',
'cfg_color_primary_color_light' => 'Couleur principale (éclaircie)',
'cfg_color_gray' => 'Gris',
'cfg_color_gray_light' => 'Gris (clair)',
'cfg_color_gray_super_light' => 'Gris (très clair)',
'cfg_color_gray_dark' => 'Gris (sombre)',
'cfg_contact' => 'Contact',
'cfg_titre_parametrages' => 'Configurer Nezu',
'cfg_homepage' => 'Page d\'accueil',
'cfg_home_soutenir' => 'Article Nous Soutenir',
'cfg_home_faq' => 'Article Foire aux questions (FAQ)',
'cfg_home_reassurance' => 'Article de présentation (réassurance)',
'cfg_home_evenement' => 'Rubrique Agenda',
'cfg_home_actu' => 'Rubrique Actualités',
'cfg_footer' => 'Pied de page',
'cfg_footer_contact' => 'Liens > Contact ',
'cfg_footer_col1' => 'Liens',
'cfg_footer_copyright_articles' => 'Liens ligne copyright',
'cfg_footer_copyright_articles_explication' => ' 2ème ligne du pied : ajouter les liens techniques (crédits, mentions légales ...)',
'cfg_footer_adresse' => 'Adresse et téléphone',
'cfg_footer_baseline' => 'Nom de la structure ou Baseline',
'cfg_rezo' => 'Réseaux sociaux',
'cfg_rezo_facebook' => 'Facebook',
'cfg_rezo_twitter' => 'Twitter',
'cfg_rezo_linkedin' => 'LinkedIn',
'cfg_rezo_youtube' => 'Youtube',
'cfg_rezo_instagram' => 'Instagram',
'cfg_menu' => 'Bannière et menu',
'cfg_menu_ids' => 'Menu',
'cfg_menu_ids_explication' => 'Rubriques et articles qui constituent le menu. Limiter le nombre à 4 maximum',
'cfg_intro' => 'Cette page vous permet de personnaliser votre squelette Nezu',
'cfg_lien_doc' => 'Documentation en ligne',
'cfg_liens' => 'Liens importants',
'cfg_palette' => 'Palette',
'cfg_palette_explication' => 'Modifier le jeu des couleurs utilisées par Nezu',
'cfg_palette_explication_suite' => 'Après avoir enregistré vos couleurs (en appuyant sur le bouton enregistrer en bas de ce formulaire), pensez à vider votre cache pour mettre à jour la feuille de style du site publique',
'cfg_page_demo' => 'Le squelette est livré avec une page démonstration qui permet de tester la mise en page avec des contenus factices :',
'cfg_page_demo_article' => 'Démo article',
'cfg_bouton1_article' => 'Article du bouton 1',
'cfg_bouton2_article' => 'Article du bouton 2',
'cfg_evenement' => 'evenement',
'cfg_evenement_nomatch' => 'Message à afficher lorsqu\'il n\'y pas de dates disponibles pour la evenement',
'cfg_evenement_nomatch_explication' => 'Syntaxe SPIP acceptée',
'contact' => 'Contact',
'nezu_type_rubrique' => 'Type de rubrique',
'nezu_type_rubrique_tri_date' => 'Articles listés par date (les articles les récents en premier)',
'nezu_type_rubrique_tri_num' => 'Articles listés par numéro (10. xxx, 20. yyy, ...)',
'nezu_type_rubrique_tri_faq' => 'Articles listés comme une base de connaissances (FAQ)',
'nezu_type_rubrique_tri_evenement' => 'Articles listés comme un agenda (liste d\'événements)',
'nezu_rubrique_surtitre' => 'Surtitre',
'nezu_rubrique_surtitre_explication' => '(Facultatif) Permet d\'afficher une phrase courte au dessus du titre notamment sur le page d\'accueil',
'nezu_rubrique_titre_long' => 'Titre long',
'nezu_rubrique_titre_long_explication' => '(Facultatif) Permet d\'afficher un titre long notamment sur le page d\'accueil',
'cfg_evenement_email_info' => 'Texte complémentaire à ajouter dans l\'email de confirmation des inscriptions',


// D
'dates_titre_a_venir' => 'Dates à venir',
'dates_titre_a_venir_singulier' => 'Prochaine date',
'decouvrir' => 'Découvrir',


// E
'et_aussi' => 'Voir aussi&nbsp;...',
'en_savoir_plus' => 'En savoir plus',

// F
'fiche_evenement' => 'Détails sur la evenement',
'fiche_evenement_dates' => 'Dates des prochaines evenements',
'fiche_evenement_no_dates' => 'Aucune date actuellement prévue actuellement.',
'footer_titre_col1' => 'L\'association',
'footer_titre_col2' => 'Actualités de l\'association',
'footer_titre_col3' => 'Ressources',
'footer_titre_contact' => 'Contact',
'filtres' => 'Filtres',
'filtres_retirer' => 'Supprimer la sélection',

// L
'liens' => 'Liens',
'lire_la_suite' => 'Lire la suite',
'lire_la_suite_decouvrir' => 'Découvrir',
'les_evenements' => 'Les événements',


// M
'menu' => 'Menu',
'mis_a_jour' => 'Mis à jour le',

// N
'newsletter_votre_email' => 'Votre email',


// O
'ours' => 'Le saviez-vous ?<br />Le nom du squelette <strong>Nezu</strong> (根津) vient d\'un quartier ancien de Tōkyō célèbre pour son sanctuaire et ses portiques rouges.',


// P
'publie_le' => 'Publié le',
'par' => 'par',
'pagination_pages' => 'Pages',
'pagination_gd_total' => 'articles disponibles',
'pagination_environ' => 'Environ',
'portfolio' => 'Portfolio',
'presentation' => 'Présentation',


// R
'resultats_out' => 'Résultat(s) disponible(s)',
'recherche_site' => 'Résultat sur  : ',
'recherche_recherche' => 'Rechercher',
'recherche_archive' => 'Rechercher',
'recherche_nomatch' => 'Désolé, aucun résultat disponible sur cette recherche ! ',
'resultats_articles' => 'Recherche sur les articles',
'recherche_dans_rubrique' => 'Rechercher dans cette rubrique',
'recherche_resultat' => 'Résultats de la recherche sur',
'recherche_titre' => 'Rechercher',
'recherche_cancel' => 'annuler cette recherche',
'retour_liste' => 'Retour à la liste',
'resultats' => '&nbsp;résultat(s)',


// T
'top' => 'Haut de page',
'titre_page_configurer_nezu' => 'Configurer le squelette Nezu',


);
