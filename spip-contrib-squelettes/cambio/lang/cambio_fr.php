<?php

return[

	'depuis_le' => 'depuis le',
	
	'export_prop' => 'Exporter les signatures en attente',
	'export_publie' => 'Exporter les signatures confirmées',
	'export_titre' => 'Exporter les signatures',

	'import_bouton' => 'Importer',
	'import_label_csv' => 'Fichier CSV',
	'import_message_ok' => '@nb@ signatures importées',
	'import_titre' => 'Importer des signatures',

	'signataire' => 'signataire',
	'signataires' => 'signataires',

	'titre_petitions_encours' => 'Pétitions en cours',
	'titre_petitions_archives' => 'Pétitions archivées',

];
