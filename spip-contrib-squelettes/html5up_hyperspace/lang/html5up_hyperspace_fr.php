<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A	
	'accueil_image_entete' => 'Image d\'entête de l\'accueil',
	'accueil_image_entete_explication' => 'Numéro du document image utilisé en entête de l\'accueil',
	'accueil_rubrique_features' => 'Rubrique features',
	'accueil_rubrique_features_explication' => 'La rubrique à afficher sous forme de grille',
	'accueil_rubrique_features_tri' => 'Ordre des articles features',
	'accueil_rubrique_features_tri_defaut' => 'Par num titre (type site)',
	'accueil_rubrique_features_tri_inverse_chrono' => 'Ordre inverse chronologique (type blog)',

	// B
	'bouton_calculer_css' => 'Recalculer les CSS',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'contenu' => 'Contenu',

	// H
	'html5up_hyperspace_titre' => 'Html5up Hyperspace',

	// T
	'theme_graphique_par_html5up' => 'Thème graphique par HTML5 UP',
	'titre_page_configurer_html5up_hyperspace' => 'Configurer le squelette HTML5up Hyperspace',

	//S
	'suite' => 'Suite',
	
);