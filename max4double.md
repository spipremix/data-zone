# max4double

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[ajuster_intertitres](https://git.spip.net/spip-contrib-extensions/ajuster_intertitres)|[3.2.0;4.*.*]|24|4.4;4.3;4.2;4.1|2024-05-07|
|[archivage](https://git.spip.net/spip-contrib-extensions/archive_objet)|[4.1.0;4.*.*]|57|4.3;2.1|2024-06-23|
|[bootstrap_scss](https://git.spip.net/spip-contrib-extensions/bootstrap_scss)|[4.1.0;4.*.*]|0|N/A|2023-12-04|
|[territoires_cartes](https://git.spip.net/spip-contrib-extensions/territoires_cartes)|[4.0.0;4.*.*]|0|N/A|2024-11-05|
|[ckeditor](https://git.spip.net/spip-contrib-extensions/ckeditor-spip-plugin)|[3.2.0;4.*.*]|204|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0|2024-05-28|
|[territoires_contours](https://git.spip.net/spip-contrib-extensions/territoires_contours)|[4.0.0;4.*.*]|0|N/A|2024-10-14|
|[convertisseur](https://git.spip.net/spip-contrib-extensions/convertisseur)|[4.2.0;4.*.*]|20|4.3;4.2;4.1;3.2;3.0;1.9|2025-02-12|
|[coupons](https://git.spip.net/spip-contrib-extensions/coupons)|[3.1.0;4.*.*]|2|3.2|2023-10-24|
|[cvtupload](https://git.spip.net/spip-contrib-extensions/cvt-upload)|[4.0.0;4.*.*]|375|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-10|
|[dictionnaires](https://git.spip.net/spip-contrib-extensions/dictionnaires)|[3.0.0;4.*.*]|232|4.4;4.3;4.2;4.1;3.2;3.1;3.0|2023-04-04|
|[empecher_double_clic](https://git.spip.net/spip-contrib-extensions/empecher_double_clic)|[4.0.0;4.*.*]|6|4.4;4.3;4.2;3.2|2024-05-07|
|[ezmashup](https://git.spip.net/spip-contrib-extensions/ezmashup)|[4.0.0;4.*.*]|4|4.4|2024-11-22|
|[fil_ariane](https://git.spip.net/spip-contrib-extensions/fil_ariane)|[4.2.0;4.*.*]|1|3.2|2024-04-28|
|[formidable](https://git.spip.net/spip-contrib-extensions/formidable)|[4.1.6;4.*.*]|819|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1|2025-02-12|
|[graphql](https://git.spip.net/spip-contrib-extensions/graphql)|[4.0.0;4.*.*]|0|N/A|2023-11-30|
|[imgcompare](https://git.spip.net/spip-contrib-extensions/images_compare)|[4.2.0;4.*.*]|3|4.3|2024-06-28|
|[introduction_explicite](https://git.spip.net/spip-contrib-extensions/introduction_explicite)|[4.2.0;4.*.*]|10|4.4;4.3|2024-05-10|
|[mes_fichiers](https://git.spip.net/spip-contrib-extensions/mes_fichiers)|[4.1.0;4.*.*]|117|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0;1.9|2024-06-22|
|[minus](https://git.spip.net/spip-contrib-extensions/minus)|[4.0.0;4.*.*]|11|4.4;4.3;3.2;3.1;3.0|2024-05-07|
|[modeles_avec_notes](https://git.spip.net/spip-contrib-extensions/modeles_avec_notes)|[4.0.0;4.*.*]|4|4.4;4.3|2024-05-07|
|[isocode](https://git.spip.net/spip-contrib-extensions/nomenclatures)|[4.0.0;4.*.*]|4|4.4|2024-08-20|
|[saisies](https://git.spip.net/spip-contrib-extensions/saisies)|[4.1.0;4.*.*]|3215|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1;2.0;1.9|2025-02-12|
|[pb_selection](https://git.spip.net/23forward/selection_articles)|[3.2.0;4.*.*]|314|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0|< 2023-02-23|
|[sidr](https://git.spip.net/spip-contrib-extensions/sidr)|[4.0.0;4.*.*]|213|4.3;4.2;4.1;3.2;3.1;3.0;2.1|2024-05-24|
|[statsjs](https://git.spip.net/spip-contrib-extensions/statsjs)|[4.2.0;4.*.*]|7|4.3;3.2;3.0;2.1|2024-10-03|
|[svpbase](https://git.spip.net/spip-contrib-extensions/svp_referentiel)|[4.1.0-dev;4.*.*]|0|N/A|2024-07-24|
|[territoires](https://git.spip.net/spip-contrib-extensions/territoires)|[4.0.0;4.*.*]|1|4.2|2024-11-11|
|[territoires_data](https://git.spip.net/spip-contrib-extensions/territoires_data)|[4.0.0;4.*.*]|0|N/A|2024-12-01|
|[thumbsites](https://git.spip.net/spip-contrib-extensions/thumbsites)|[4.0.0;4.*.*]|82|4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0;1.9|2024-06-24|
|[titres_typographies](https://git.spip.net/spip-contrib-extensions/titres_typographies)|[4.2.0;4.*.*]|75|4.4;4.3;4.2;3.2|2024-08-18|
|[initializr](https://git.spip.net/spip-contrib-themes/zpip_v2_Initializr)|[3.2.1;4.*.*]|0|N/A|2023-03-20|
|[ezmath](https://git.spip.net/spip-contrib-extensions/ezmath)|[4.0.0;4.*.*]|0|N/A|2024-12-21|
|[qrcode_form](https://git.spip.net/spip-contrib-extensions/qrcode_form)|[4.3.0;4.*.*]|3|4.4;4.3|2024-11-16|
