# min32

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[abonnements](https://git.spip.net/spip-contrib-extensions/abonnements)|[3.2.0;4.*]|25|4.3;4.2;4.1;3.2;3.1;3.0|2024-09-12|
|[abonnements_profils](https://git.spip.net/spip-contrib-extensions/abonnements_profils)|[3.2.3;4.*]|0|N/A|2024-07-05|
|[adminer](https://git.spip.net/spip-contrib-extensions/adminer)|[3.2.0;4.*]|296|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1|2024-07-05|
|[aeres](https://git.spip.net/spip-contrib-extensions/aeres)|[3.2.0;4.0.*]|0|N/A|< 2023-02-23|
|[agendaflou](https://git.spip.net/spip-contrib-extensions/agenda_dates_floues)|[3.2.0;3.2.*]|0|N/A|< 2023-02-23|
|[ajuster_intertitres](https://git.spip.net/spip-contrib-extensions/ajuster_intertitres)|[3.2.0;4.*.*]|24|4.4;4.3;4.2;4.1|2024-05-07|
|[archive](https://git.spip.net/spip-contrib-extensions/archive)|[3.2.0;4.*]|20|4.4;4.3;4.2;4.1;4.0;3.2;3.1;1.9|2025-01-13|
|[ascore](https://git.spip.net/spip-contrib-extensions/ascore)|[3.2.0;4.*]|0|N/A|2024-08-06|
|[askwiki](https://git.spip.net/spip-contrib-extensions/askwiki)|[3.2.7;3.2.*]|0|N/A|< 2023-02-23|
|[auteur_nom_prenom](https://git.spip.net/spip-contrib-extensions/auteur_nom_prenom)|[3.2.0;4.*]|2|4.3;4.1|2024-07-05|
|[autonumeric](https://git.spip.net/spip-contrib-extensions/AutoNumeric)|[3.2.0;3.2.*]|0|N/A|< 2023-02-23|
|[banniere_greve](https://git.spip.net/spip-contrib-extensions/banniere_greve)|[3.2.0;4.*]|0|N/A|2024-07-13|
|[bibliocheck](https://git.spip.net/spip-contrib-extensions/bibliocheck)|[3.2.0;4.0.*]|0|N/A|< 2023-02-23|
|[billetteries](https://git.spip.net/spip-contrib-extensions/billetteries)|[3.2.3;4.*]|1|4.3|2024-11-05|
|[billetteries_profils](https://git.spip.net/spip-contrib-extensions/billetteries_profils)|[3.2.3;4.*]|1|4.3|2024-07-05|
|[bootstrap4](https://git.spip.net/spip-contrib-extensions/bootstrap4)|[3.2.0;4.*]|97|4.4;4.3;4.2;4.1;4.0;3.2|2024-11-18|
|[bcd](https://git.spip.net/spip-contrib-extensions/boutons_contacts_et_dates)|]3.2.999;4.*]|0|N/A|2024-10-17|
|[bruteforcelogin](https://git.spip.net/spip-contrib-extensions/bruteforcelogin)|[3.2.0;4.*]|0|N/A|2024-10-07|
|[pb_cesure](https://git.spip.net/23forward/cesure)|[3.2.0;4.*]|29|4.3;4.2;3.2;3.1;2.1;2.0;1.9|2024-07-26|
|[chapitres](https://git.spip.net/spip-contrib-extensions/chapitres)|[3.2.0;4.1.*]|0|N/A|2024-07-24|
|[chartjs](https://git.spip.net/spip-contrib-extensions/chartjs)|[3.2.0;4.*]|22|4.4;4.3;4.2;4.1;4.0;3.2;3.1|2024-07-05|
|[cite](https://git.spip.net/spip-contrib-extensions/cite)|[3.2.0;4.*]|5|4.3;4.2;3.0;2.1|2023-09-13|
|[ckeditor](https://git.spip.net/spip-contrib-extensions/ckeditor-spip-plugin)|[3.2.0;4.*.*]|204|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0|2024-05-28|
|[clevermail](https://git.spip.net/spip-contrib-extensions/clevermail)|[3.2.0;4.*]|55|4.3;4.2;4.1;3.2;3.1;2.1;2.0|2024-07-05|
|[commandes](https://git.spip.net/spip-contrib-extensions/commandes)|[3.2.0;4.*]|40|4.3;4.2;4.1;3.2;3.1;3.0|2024-07-05|
|[commandes_abonnements](https://git.spip.net/spip-contrib-extensions/commandes_abonnements)|[3.2.0;4.*]|4|4.2;4.1|2024-08-28|
|[commandes_abonnements_fiscalite](https://git.spip.net/spip-contrib-extensions/commandes_abonnements_fiscalite)|[3.2.0;4.*]|0|N/A|2024-07-05|
|[contact](https://git.spip.net/spip-contrib-extensions/contact)|[3.2.0;4.*]|461|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1;2.0;1.9|2024-10-25|
|[creer_sprites](https://git.spip.net/23forward/creer_sprites_css)|[3.2.0;4.*]|2|4.2;3.2|2024-07-27|
|[cssvar](https://git.spip.net/spip-contrib-extensions/cssvar)|[3.2.0;4.*]|3|4.3;4.2;4.0|2024-12-02|
|[css_imbriques](https://git.spip.net/23forward/css-imbriques)|[3.2.0;4.*]|21|4.3;4.2;3.2;3.1;2.1|2024-08-01|
|[datatables](https://git.spip.net/spip-contrib-extensions/datatables)|[3.2.0;3.2.*]|1|3.2|< 2023-02-23|
|[dater_rubriques](https://git.spip.net/spip-contrib-extensions/dater_rubriques)|[3.2.0;4.*]|0|N/A|2024-07-05|
|[decoauto](https://git.spip.net/spip-contrib-extensions/deconnexion_auto)|[3.2.3;4.*]|1|3.2|2024-07-05|
|[devis](https://git.spip.net/spip-contrib-extensions/devis)|[3.2.1;4.*]|0|N/A|2024-07-05|
|[dons](https://git.spip.net/spip-contrib-extensions/dons)|[3.2.9;4.*]|0|N/A|2024-07-05|
|[emogrifier](https://git.spip.net/spip-contrib-extensions/emogrifier)|[3.2.0;4.*]|25|4.3;4.2;4.1;4.0;3.2;3.1|2024-07-13|
|[epilogue](https://git.spip.net/spip-contrib-extensions/epilogue)|[3.2.0;4.*]|1|4.3|2024-07-13|
|[exclure_sect](https://git.spip.net/spip-contrib-extensions/exclure_secteur)|[3.2.0;4.*]|43|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0|2024-07-05|
|[favicon](https://git.spip.net/spip-contrib-extensions/favicon)|[3.2.0;4.*]|280|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[fontawesome](https://git.spip.net/spip-contrib-extensions/fontawesome)|[3.2.7;4.*]|0|N/A|2024-07-05|
|[forkawesome](https://git.spip.net/spip-contrib-extensions/forkawesome)|[3.2.0;4.*]|10|4.4;4.3;4.2|2024-07-05|
|[formidablepaiement](https://git.spip.net/spip-contrib-extensions/formidablepaiement)|[3.2.0;4.*]|47|4.3;4.2;4.1;3.2|2025-01-24|
|[fulltext](https://git.spip.net/spip-contrib-extensions/fulltext)|[3.2.0;4.*]|462|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0|2025-01-29|
|[galleria](https://git.spip.net/spip-contrib-extensions/galleria)|[3.2.0;4.*]|0|N/A|2024-07-05|
|[gis_agenda](https://git.spip.net/spip-contrib-extensions/gis_agenda)|[3.2.0;4.*]|11|4.4;4.3;4.2;3.2|2025-02-10|
|[gis_plus](https://git.spip.net/spip-contrib-extensions/gis_plus)|[3.2.4;4.*]|2|4.3|2024-07-05|
|[gravatar](https://git.spip.net/spip-contrib-extensions/gravatar)|[3.2.0;4.*]|121|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0|2025-01-29|
|[gma](https://git.spip.net/spip-contrib-extensions/groupes_mots_arborescents)|[3.2.0;4.*]|34|4.3;4.2;4.1;3.2|2025-02-08|
|[histoconnexions](https://git.spip.net/spip-contrib-extensions/histoconnexions)|[3.2.0;4.*]|45|4.4;4.3;4.2;4.1;3.2|2024-07-05|
|[historique_spip32_h3](https://git.spip.net/spip-contrib-extensions/historique_spip32_h3)|]3.2.999;4.*]|1|4.3|2024-07-22|
|[historique_spip32_html4](https://git.spip.net/spip-contrib-extensions/historique_spip32_html4)|]3.2.999;4.0.*]|0|N/A|< 2023-02-23|
|[identite_extra](https://git.spip.net/spip-contrib-extensions/identite_extra)|[3.2.0;4.*]|294|4.4;4.3;4.2;4.1;4.0;3.2;3.1|2024-07-05|
|[ieconfigplus](https://git.spip.net/spip-contrib-extensions/ieconfigplus)|[3.2.1;3.2.*]|2|4.3;3.2|< 2023-02-23|
|[incarner](https://git.spip.net/spip-contrib-extensions/incarner)|[3.2.0;4.*]|29|4.3;4.2;3.2|2024-07-05|
|[inclureplus](https://git.spip.net/spip-contrib-extensions/inclureplus)|[3.2.0;4.99.*]|0|N/A|2024-09-15|
|[inscriptionconnexion](https://git.spip.net/spip-contrib-extensions/inscription_connexion)|[3.2.0;4.*]|5|4.3;4.2;3.2|2024-07-05|
|[inscriptionmotdepasse](https://git.spip.net/spip-contrib-extensions/inscription_motdepasse)|[3.2.0;4.*]|24|4.3;4.2;3.2;3.1|2024-11-25|
|[intl](https://git.spip.net/spip-contrib-extensions/intl)|[3.2.0;4.*]|68|4.3;4.2;4.1;3.2|2024-07-05|
|[ipset](https://git.spip.net/spip-contrib-extensions/ipset)|[3.2.0;4.0.*]|0|N/A|< 2023-02-23|
|[jquery_number](https://git.spip.net/spip-contrib-extensions/jQuery_number)|[3.2.0;3.2.*]|0|N/A|< 2023-02-23|
|[languepreferee](https://git.spip.net/spip-contrib-extensions/langue_preferee)|[3.2.0;4.*]|47|4.4;4.3;4.2;4.1;3.2;3.0|2024-07-05|
|[legal_en](https://git.spip.net/spip-contrib-extensions/legal_en)|[3.2.0;4.*]|29|4.3;4.2|2024-07-05|
|[libphonenumber](https://git.spip.net/spip-contrib-extensions/libphonenumber)|[3.2.4;4.*]|1|4.3|2024-07-05|
|[licence](https://git.spip.net/spip-contrib-extensions/licence)|[3.2.0;4.*]|41|4.3;4.2;4.1;3.2;3.1;3.0|2024-07-05|
|[livraison](https://git.spip.net/spip-contrib-extensions/livraison)|[3.2.0;4.*]|9|4.3;4.2;3.2|2024-07-05|
|[mailshot](https://git.spip.net/spip-contrib-extensions/mailshot)|[3.2.0;4.*]|521|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-01-27|
|[mailsubscribers](https://git.spip.net/spip-contrib-extensions/mailsubscribers)|[3.2.0;4.*]|515|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2024-12-24|
|[maintenancekit](https://git.spip.net/spip-contrib-extensions/maintenancekit)|[3.2.1;3.2.*]|0|N/A|< 2023-02-23|
|[manuelsite](https://git.spip.net/spip-contrib-extensions/manuel_site)|[3.2.0;4.*]|131|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1|2024-07-05|
|[masquer](https://git.spip.net/spip-contrib-extensions/masquer)|[3.2.0;4.*]|47|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1|2024-07-05|
|[mastodon](https://git.spip.net/spip-contrib-extensions/mastodon)|[3.2.0;4.*]|26|4.4;4.3;4.2;3.2|2025-01-20|
|[mediaboxavectexte](https://git.spip.net/spip-contrib-extensions/mediaboxavectexte)|[3.2.0;3.2.*]|15|3.2;3.1;3.0|< 2023-02-23|
|[memoization](https://git.spip.net/spip-contrib-extensions/memoization)|[3.2.0;4.*]|867|4.4;4.3;4.2;4.1;3.2;3.1;3.0|2025-01-29|
|[menu_boutique](https://git.spip.net/spip-contrib-extensions/menu_boutique)|[3.2.15;4.*]|1|4.3|2024-12-07|
|[mll](https://git.spip.net/spip-contrib-extensions/menu_langues_liens)|[3.2.0;4.*]|100|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1|2024-07-05|
|[messagerie](https://git.spip.net/spip-contrib-extensions/messagerie)|[3.2.0;4.*]|24|4.4;4.3;4.0;3.2;3.1;3.0;2.1;2.0|2024-07-09|
|[multilang](https://git.spip.net/spip-contrib-extensions/multilang)|[3.2.0;4.*]|73|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1|2025-01-29|
|[multilingue](https://git.spip.net/spip-contrib-extensions/multilingue)|[3.2.0;4.*]|20|4.4;4.3;4.2;4.1;3.2;3.0|2024-07-13|
|[newsletters](https://git.spip.net/spip-contrib-extensions/newsletters)|[3.2.0;4.*]|462|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2024-10-27|
|[nivoslider](https://git.spip.net/spip-contrib-extensions/nivoslider)|[3.2.0;4.*]|0|N/A|2024-07-05|
|[noizetier_extra](https://git.spip.net/spip-contrib-extensions/noizetier_extra)|[3.2.0;3.2.*]|1|3.2|< 2023-02-23|
|[noizetier_layout](https://git.spip.net/spip-contrib-extensions/noizetier_layout)|[3.2.0;4.*]|3|4.2;4.0;3.2|2025-02-15|
|[normalisation_unicode](https://git.spip.net/spip-contrib-extensions/normalisation_unicode)|[3.2.0;3.2.*]|1|3.2|< 2023-02-23|
|[notifavancees](https://git.spip.net/spip-contrib-extensions/notifications_avancees)|[3.2.0;4.*]|48|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[nuage](https://git.spip.net/spip-contrib-extensions/nuage)|[3.2.0;4.*]|266|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2025-01-29|
|[numero](https://git.spip.net/spip-contrib-extensions/numerotation)|[3.2.0;4.*]|180|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0|2024-07-05|
|[odt2spip](https://git.spip.net/spip-contrib-extensions/odt2spip)|[3.2.0;4.*]|511|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2024-12-09|
|[okpal](https://git.spip.net/spip-contrib-extensions/okpal)|[3.2.0;3.2.*]|0|N/A|< 2023-02-23|
|[opquast](https://git.spip.net/spip-contrib-extensions/opquast)|[3.2.0;4.0.*]|1|4.0|< 2023-02-23|
|[palette](https://git.spip.net/spip-contrib-extensions/palette)|[3.2.0;4.*]|596|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-05|
|[panolens](https://git.spip.net/spip-contrib-extensions/panolens)|[3.2.0;3.2.*]|0|N/A|< 2023-02-23|
|[pdfjs](https://git.spip.net/spip-contrib-extensions/pdfjs)|[3.2.0;4.*]|417|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;1.9|2024-07-05|
|[pgn4spip](https://git.spip.net/spip-contrib-extensions/pgn4spip)|[3.2.1;4.*]|5|4.4;4.3;4.2;3.2|2024-07-05|
|[phpcurl](https://git.spip.net/spip-contrib-extensions/phpcurl)|[3.2.0;4.*]|5|4.3;4.0;3.2|2024-07-13|
|[placeholder](https://git.spip.net/spip-contrib-extensions/placeholder)|[3.2.0;4.*]|0|N/A|2024-07-05|
|[plugonet](https://git.spip.net/spip-contrib-extensions/plugonet)|]3.2.999;4.*]|11|4.4;4.3;4.2;3.2;3.0|2025-01-29|
|[pp_loremipsum](https://git.spip.net/spip-contrib-extensions/porte_plume_lorem_ipsum)|[3.2.0;4.*]|17|4.3;4.2;4.1;3.2;3.1;3.0;2.0|2024-07-05|
|[precode](https://git.spip.net/spip-contrib-extensions/precode)|[3.2.0;4.*]|4|4.3;3.2|2024-07-05|
|[prix](https://git.spip.net/spip-contrib-extensions/prix)|[3.2.0;4.*]|70|4.3;4.2;4.1;3.2;3.1;3.0;2.1|2024-07-05|
|[prix_objets](https://git.spip.net/spip-contrib-extensions/prix_objets)|[3.2.0;4.*]|11|4.3;4.2;4.1;3.2|2024-07-13|
|[produits](https://git.spip.net/spip-contrib-extensions/produits)|[3.2.0;4.*]|23|4.3;3.2;3.1|2025-02-12|
|[pushsubscribers](https://git.spip.net/spip-contrib-extensions/pushsubscribers)|[3.2.7;3.2.*]|0|N/A|< 2023-02-23|
|[redirection](https://git.spip.net/spip-contrib-extensions/redirection)|[3.2.7;3.2.*]|0|N/A|< 2023-02-23|
|[rubrique_a_linscription](https://git.spip.net/spip-contrib-extensions/rubrique_a_linscription)|[3.2.9;4.*]|4|4.3;3.2;3.1;2.1|2024-07-13|
|[saisie_choix_couleur](https://git.spip.net/spip-contrib-extensions/saisie_choix_couleur)|[3.2.0;4.*]|4|4.2;3.2;2.1|2024-07-13|
|[saisie_nombre](https://git.spip.net/spip-contrib-extensions/saisie_nombre)|[3.2.0;3.2.*]|0|N/A|< 2023-02-23|
|[seances2agenda](https://git.spip.net/spip-contrib-extensions/seances2agenda)|[3.2.0;4.*]|0|N/A|2024-07-05|
|[secteur_langue](https://git.spip.net/spip-contrib-extensions/secteur_langue)|[3.2.0;4.*]|42|4.4;4.3;4.2;4.1;3.2|2024-07-13|
|[pb_selection](https://git.spip.net/23forward/selection_articles)|[3.2.0;4.*.*]|314|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0|< 2023-02-23|
|[seo](https://git.spip.net/spip-contrib-extensions/seo)|[3.2.0;4.*]|226|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0|2024-07-05|
|[simplecal](https://git.spip.net/spip-contrib-extensions/simple_calendrier)|[3.2.0;4.*]|5|4.3;4.2;3.2|2025-01-29|
|[sitemap_news](https://git.spip.net/spip-contrib-extensions/sitemap_news)|[3.2.0;4.*]|13|4.3;4.2;4.0;3.2|2024-07-05|
|[socicon](https://git.spip.net/spip-contrib-extensions/socicon)|[3.2.0;4.*]|7|4.3;4.1;4.0;3.2|2024-07-05|
|[souscription](https://git.spip.net/spip-contrib-extensions/souscription)|[3.2.0;4.*]|6|4.3;4.2;3.2|2024-07-05|
|[stocks](https://git.spip.net/spip-contrib-extensions/stocks)|[3.2.0;4.*]|6|4.3;3.2|2024-07-13|
|[superfish](https://git.spip.net/spip-contrib-extensions/superfish)|[3.2.0;4.*]|200|4.3;4.2;4.1;3.2|2024-07-05|
|[svpcvs](https://git.spip.net/spip-contrib-extensions/svpcvs)|[3.2.0-dev;4.0.*]|0|N/A|< 2023-02-23|
|[svpapi](https://git.spip.net/spip-contrib-extensions/svp_api)|]3.2.999;4.*]|5|4.4;4.0|2024-07-05|
|[svptype](https://git.spip.net/spip-contrib-extensions/svp_typologie)|]3.2.999;4.*]|5|4.4|2024-12-22|
|[swiper](https://git.spip.net/spip-contrib-extensions/swiper)|[3.2.0;4.*]|8|4.3;4.2|2024-07-05|
|[temps_lecture](https://git.spip.net/spip-contrib-extensions/temps_lecture)|[3.2.4;4.*]|5|4.4;4.3;4.2;4.1|2024-07-05|
|[timelineme](https://git.spip.net/spip-contrib-extensions/timelineme)|[3.2.0;4.*]|2|4.3|2024-07-05|
|[twitter](https://git.spip.net/spip-contrib-extensions/twitter)|[3.2.0;4.*]|52|4.3;4.2;4.1;3.2;3.1;3.0|2024-07-05|
|[typo_guillemets](https://git.spip.net/spip-contrib-extensions/typo_guillemets)|[3.2.0;4.*]|635|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2024-08-03|
|[varicelle](https://git.spip.net/spip-contrib-extensions/varicelle)|[3.2.0;4.*]|47|4.3;4.2;3.2;3.1;3.0;2.1|2024-07-05|
|[vimeo](https://git.spip.net/spip-contrib-extensions/vimeo)|[3.2.0;4.0.*]|1|3.2|< 2023-02-23|
|[visiteurs_connectes](https://git.spip.net/spip-contrib-extensions/visiteurs_connectes)|[3.2.0;4.*]|69|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-05|
|[webmanifest](https://git.spip.net/spip-contrib-extensions/webmanifest)|[3.2.0;4.*]|0|N/A|2024-09-16|
|[wikidata](https://git.spip.net/spip-contrib-extensions/Wikidata)|[3.2.0;4.3.*]|0|N/A|< 2023-02-23|
|[wp2spip](https://git.spip.net/spip-contrib-extensions/wp2spip)|[3.2.0;3.2.*]|0|N/A|< 2023-02-23|
|[zcore](https://git.spip.net/spip-contrib-extensions/z-core)|[3.2.0;4.*]|1691|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2025-01-29|
|[bazar](https://git.spip.net/spip-contrib-squelettes/bazar)|[3.2.0;4.3.*]|1|4.3|2024-09-13|
|[dist](https://git.spip.net/spip-contrib-squelettes/dist-html5)|[3.2.0-dev;3.2.*]|7|3.1;3.0;1.9|< 2023-02-23|
|[historique_dist_html4](https://git.spip.net/spip-contrib-squelettes/historique_dist_html4)|]3.2.999;4.0.*]|0|N/A|< 2023-02-23|
|[microedition](https://git.spip.net/spip-contrib-squelettes/microedition)|[3.2.0;4.*]|0|N/A|2024-09-13|
|[navigation_ep](https://git.spip.net/spip-contrib-squelettes/Navigation_EP)|[3.2.0;4.*]|0|N/A|2024-07-05|
|[spipr_blog](https://git.spip.net/spip-contrib-squelettes/spipr-blog)|[3.2.0;4.*]|7|4.4;4.3;4.2;3.2;3.0|2024-07-05|
|[sdn](https://git.spip.net/spip-contrib-squelettes/spipr-dane-noisettes)|[3.2.3;4.*]|32|4.4;4.2|2024-07-13|
|[spipr_dist](https://git.spip.net/spip-contrib-squelettes/spipr-dist)|[3.2.0;4.*]|650|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[spip_reset](https://git.spip.net/spip-contrib-squelettes/spip_reset)|[3.2.0;4.*]|80|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1|2024-07-05|
|[zboot](https://git.spip.net/spip-contrib-squelettes/zboot)|[3.2.0;4.*]|0|N/A|2025-02-02|
|[theme_bsamelia](https://git.spip.net/spip-contrib-themes/spipr_v1_amelia)|[3.2.0;4.*]|2|3.0|2024-07-05|
|[theme_bsblog](https://git.spip.net/spip-contrib-themes/spipr_v1_beige)|[3.2.0;4.*]|2|3.0|2024-07-05|
|[theme_bsbootplus](https://git.spip.net/spip-contrib-themes/spipr_v1_bootplus)|[3.2.0;4.*]|1|3.0|2024-07-05|
|[theme_bscerulean](https://git.spip.net/spip-contrib-themes/spipr_v1_cerulean)|[3.2.0;4.*]|2|3.0|2024-07-05|
|[theme_bscosmo](https://git.spip.net/spip-contrib-themes/spipr_v1_cosmo)|[3.2.0;4.*]|2|3.0|2024-07-05|
|[theme_bscyborg](https://git.spip.net/spip-contrib-themes/spipr_v1_cyborg)|[3.2.0;4.*]|3|3.0|2024-07-05|
|[theme_bsflatly](https://git.spip.net/spip-contrib-themes/spipr_v1_flatly)|[3.2.0;4.*]|3|3.1;3.0|2024-07-05|
|[theme_generic](https://git.spip.net/spip-contrib-themes/spipr_v1_generic)|[3.2.0;4.*]|1|3.2|2024-07-05|
|[theme_bsjournal](https://git.spip.net/spip-contrib-themes/spipr_v1_journal)|[3.2.0;4.*]|3|3.2;3.0|2024-07-05|
|[theme_marguerite](https://git.spip.net/spip-contrib-themes/spipr_v1_marguerite)|[3.2.0;4.*]|5|3.2;3.1;3.0|2024-07-05|
|[theme_bsreadable](https://git.spip.net/spip-contrib-themes/spipr_v1_readable)|[3.2.0;4.*]|4|4.1;3.2;3.0|2024-07-05|
|[theme_bsrosa](https://git.spip.net/spip-contrib-themes/spipr_v1_rosa)|[3.2.0;4.*]|0|N/A|2024-07-05|
|[theme_bssimplex](https://git.spip.net/spip-contrib-themes/spipr_v1_simplex)|[3.2.0;4.*]|2|3.0|2024-07-05|
|[theme_bsslate](https://git.spip.net/spip-contrib-themes/spipr_v1_slate)|[3.2.0;4.*]|1|3.0|2024-07-05|
|[theme_bsspacelab](https://git.spip.net/spip-contrib-themes/spipr_v1_spacelab)|[3.2.0;4.*]|5|3.1;3.0|2024-07-05|
|[theme_bsspruce](https://git.spip.net/spip-contrib-themes/spipr_v1_spruce)|[3.2.0;4.*]|1|3.0|2024-07-05|
|[theme_bssuperhero](https://git.spip.net/spip-contrib-themes/spipr_v1_superhero)|[3.2.0;4.*]|1|3.0|2024-07-05|
|[theme_bsunited](https://git.spip.net/spip-contrib-themes/spipr_v1_united)|[3.2.0;4.*]|4|4.3;3.1;3.0|2024-07-05|
|[theme_bs4blog](https://git.spip.net/spip-contrib-themes/spipr_v2_beige)|[3.2.0;4.0.*]|0|N/A|< 2023-02-23|
|[theme_bs4journal](https://git.spip.net/spip-contrib-themes/spipr_v2_journal)|[3.2.0;4.0.*]|1|4.3|< 2023-02-23|
|[theme_bs4marguerite](https://git.spip.net/spip-contrib-themes/spipr_v2_marguerite)|[3.2.0;4.*]|1|4.4|2024-07-05|
|[initializr](https://git.spip.net/spip-contrib-themes/zpip_v2_Initializr)|[3.2.1;4.*.*]|0|N/A|2023-03-20|
|[demo](https://git.spip.net/spip-contrib-extensions/demonstration)|[3.2.19;4.*]|0|N/A|2024-07-05|
|[decouleurs_spip](https://git.spip.net/spip-contrib-extensions/decouleurs_spip)|[3.2.0;4.*]|2|4.3|2024-07-05|
|[theme_bs4kit_de_demarrage](https://git.spip.net/spip-contrib-themes/spipr_v2_kit_de_demarrage)|[3.2.0;4.0.*]|0|N/A|2024-05-09|
|[ciautoriser](https://git.spip.net/giseh/ciautoriser)|[3.2.0;]|67|4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0|2024-11-27|
|[cioidc](https://git.spip.net/giseh/cioidc)|[3.2.0;]|3|4.4;4.3|2024-10-25|
|[ciag](https://git.spip.net/giseh/ciag)|[3.2.0;]|14|4.3;4.1;3.2;3.0;2.1|2024-11-27|
|[ciar](https://git.spip.net/giseh/ciar)|[3.2.0;]|18|4.3;4.1;3.2;3.0;2.1|2025-02-04|
|[ciarchive](https://git.spip.net/giseh/ciarchive)|[3.2.0;]|17|4.3;4.2;4.1;3.1;3.0|2024-11-27|
|[cicas](https://git.spip.net/giseh/cicas)|[3.2.0;]|327|4.3;4.2;4.1;3.2;3.0;2.1;2.0|2024-11-27|
|[ciimport](https://git.spip.net/giseh/ciimport)|[3.2.0;]|18|4.3;4.1;3.2;3.1;3.0;2.1|2025-02-07|
|[cilien](https://git.spip.net/giseh/cilien)|[3.2.0;]|8|4.3;4.1;3.0|2024-11-18|
|[cims](https://git.spip.net/giseh/cims)|[3.2.0;]|3|4.3;4.1|2025-02-03|
|[cinotif](https://git.spip.net/giseh/cinotif)|[3.2.0;]|11|4.3;4.1;3.2;3.1;3.0;2.1|2024-11-27|
|[ciparam](https://git.spip.net/giseh/ciparam)|[3.2.0;]|17|4.3;4.1;3.0;2.1;2.0|2024-11-18|
|[cipr](https://git.spip.net/giseh/cipr)|[3.2.0;]|8|4.3;4.1;2.1;2.0|2024-11-27|
|[cirr](https://git.spip.net/giseh/cirr)|[3.2.0;]|33|4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0|2024-11-27|
|[cirv](https://git.spip.net/giseh/cirv)|[3.2.0;]|19|4.3;4.1;3.2;3.0;2.1|2024-11-27|
|[cisec](https://git.spip.net/giseh/cisec)|[3.2.0;]|10|4.3;4.2;4.1;3.2;3.0|2024-11-18|
|[cisf](https://git.spip.net/giseh/cisf)|[3.2.0;]|21|4.3;4.1;3.2;3.0;2.1|2025-01-07|
|[citrace](https://git.spip.net/giseh/citrace)|[3.2.0;]|8|4.3;4.1;3.1;3.0|2024-11-18|
|[cibc](https://git.spip.net/giseh/cibc)|[3.2.0;]|55|4.3;4.2;4.1;4.0;3.2;3.0;2.1|2025-01-21|
|[cid](https://git.spip.net/giseh/cid)|[3.2.0;]|9|4.3;4.1;3.0;2.1|2024-12-04|
|[cifiltre](https://git.spip.net/giseh/cifiltre)|[3.2.0;]|9|4.3;4.1;3.0;2.1;2.0|2024-12-04|
|[ciform](https://git.spip.net/giseh/ciform)|[3.2.0;]|12|4.3;4.1;3.0;2.1|2024-12-04|
|[cioaitse](https://git.spip.net/giseh/cioaitse)|[3.2.0;]|0|N/A|2024-12-04|
|[cispam](https://git.spip.net/giseh/cispam)|[3.2.0;]|8|4.3;4.1;3.0;2.1|2025-01-21|
|[cisquel](https://git.spip.net/giseh/cisquel)|[3.2.0;]|17|4.3;4.1;3.0;2.1;2.0|2024-12-04|
|[cibloc](https://git.spip.net/giseh/cibloc)|[3.2.0;]|53|4.4;4.3;4.2;4.1;3.2;3.1;3.0|2024-12-04|
|[cistyle](https://git.spip.net/giseh/cistyle)|[3.2.0;]|53|4.4;4.3;4.2;4.1;3.2;3.0|2024-12-05|
|[ciwidget](https://git.spip.net/giseh/ciwidget)|[3.2.0;]|16|4.3;4.2;4.1;3.2|2025-02-14|
