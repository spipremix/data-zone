# min50

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[aide](https://git.spip.net/spip/aide)|[5.0.0-dev;5.0.*]|3346|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;2.1|2025-02-06|
|[archiviste](https://git.spip.net/spip/archiviste)|[5.0.0-dev;5.0.*]|3346|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1|2025-01-21|
|[bigup](https://git.spip.net/spip/bigup)|[5.0.0-dev;5.*]|2610|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1|2025-02-14|
|[compagnon](https://git.spip.net/spip/compagnon)|[5.0.0-dev;5.0.*]|4104|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-03|
|[compresseur](https://git.spip.net/spip/compresseur)|[5.0.0-dev;5.0.*]|4810|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1;1.9|2025-02-03|
|[dist](https://git.spip.net/spip/dist)|[5.0.0-dev;5.0.*]|7|3.1;3.0;1.9|2025-02-06|
|[dump](https://git.spip.net/spip/dump)|[5.0.0-dev;5.*]|4106|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-03|
|[images](https://git.spip.net/spip/images)|[5.0.0-dev;5.0.*]|4812|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1;1.9|2024-12-26|
|[forum](https://git.spip.net/spip/forum)|[5.0.0-dev;5.0.*]|4118|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1;2.0;1.9|2025-01-29|
|[mediabox](https://git.spip.net/spip/mediabox)|[5.0.0-dev;5.0.*]|4141|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1|2024-12-26|
|[medias](https://git.spip.net/spip/medias)|[5.0.0-dev;5.*]|4190|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1|2025-02-09|
|[mots](https://git.spip.net/spip/mots)|[5.0.0-dev;5.*]|4106|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-07|
|[plan](https://git.spip.net/spip/plan)|[5.0.0-dev;5.0.*]|3575|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1|2025-02-09|
|[porte_plume](https://git.spip.net/spip/porte-plume)|[5.0.0-dev;5.0.*]|4842|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1;2.0;1.9|2025-02-10|
|[revisions](https://git.spip.net/spip/revisions)|[5.0.0-dev;5.0.*]|4106|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-09|
|[safehtml](https://git.spip.net/spip/safehtml)|[5.0.0-dev;5.0.*]|4812|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0;2.1;1.9|2025-02-09|
|[sites](https://git.spip.net/spip/sites)|[5.0.0-dev;5.*]|4105|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-09|
|[stats](https://git.spip.net/spip/stats)|[5.0.0-dev;5.0.*]|4104|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-05|
|[svp](https://git.spip.net/spip/svp)|[5.0.0-dev;5.*]|4105|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-05|
|[tw](https://git.spip.net/spip/tw)|[5.0.0-dev;5.*]|4103|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-05|
|[urls](https://git.spip.net/spip/urls)|[5.0.0-dev;5.0.*]|4106|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-02-05|
|[surligne](https://git.spip.net/spip-contrib-extensions/surligne)|[5.0.0-dev;5.0.*]|0|N/A|2024-10-01|
