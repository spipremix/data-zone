<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/rainette_histo.git

return [

	// R
	'rainette_histo_description' => 'Ce plugin est un add-on du plugin Rainette. Il permet d’historiser les données issues des observations météorologigues de différents lieux.',
	'rainette_histo_nom' => 'Historisation pour Rainette',
	'rainette_histo_slogan' => 'Historiser les observations de Rainette',
];
