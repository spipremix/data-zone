<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// I
	'itineraires_description' => 'Ce plugin ajoute un nouvel objet éditorial permettant de décrire un itinéraire (distance, difficulté, etc).',
	'itineraires_nom' => 'Itinéraires',
	'itineraires_slogan' => 'Lister des itinéraires de randonnées, vélo, cheval…',
);
