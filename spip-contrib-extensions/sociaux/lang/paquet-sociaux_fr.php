<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// S
	'sociaux_description' => 'Permet de gérer les liens sociaux d\'un site internet',
	'sociaux_nom' => 'Liens vers les réseaux sociaux',
	'sociaux_slogan' => 'Vos liens vers les réseaux sociaux',
];

