<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'motpasseexpirable_titre' => 'Mots de passe expirables',

	// C
	'cfg_delai' => 'Durée d\'expiration des mots de passe',
	'cfg_delai_explication' => 'Nombre de jours',
	'cfg_titre_parametrages' => 'Paramétrages',
	'cfg_status' => 'Qui ?',
	'cfg_status_explication' => 'Quelles sont les personnes dont on doit réinitialiser les mots de passe régulièrement ?',

	// T
	'titre_page_configurer_motpasseexpirable' => 'Configuration de l\'expiration des mots de passe',

	// P
	'pass_mail_expire' => '(ceci est un message automatique)

Votre mot de passe vient d\'expirer.

Pour retrouver votre acc&egrave;s au site
@nom_site_spip@ (@adresse_site@)

Veuillez vous rendre &agrave; l\'adresse suivante :

    @sendcookie@

Vous pourrez alors entrer un nouveau mot de passe
et vous reconnecter au site.

'

);

