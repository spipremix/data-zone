<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'motpasseexpirable_description' => 'Force les auteurs à renouveler leur mot de passe régulièrement.',
	'motpasseexpirable_nom' => 'Mots de passe expirables',
	'motpasseexpirable_slogan' => 'Mettre à jour les mots de passe régulièrement',
);

