<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
	'configurer' => 'Configurer Images optimisées',

	'configurer_sauvegarde_label_case' => 'Générer une sauvegarde de l\'image avant optimisation',

	'configurer_optimizers_label' => 'Configuration des <i>optimizers</i>',
	'configurer_jpegoptim_label' => 'Traitement des JPG',
	'configurer_jpegoptim_attention' => 'L\'optimisation des jpeg utilise le binaire <strong>jpegoptim</strong> qui semble être disponible sur votre serveur.',
	'configurer_jpegoptim_attention_absent' => 'L\'optimisation des jpeg nécessite <strong>jpegoptim</strong> qui semble ne pas être installé sur votre serveur.',
	'configurer_jpegoptim_activer_label_case' => 'Activer l\'optimisation des jpeg avec jpegotpim',
	'configurer_jpegoptim_options_explication' => 'Retrouvez les arguments de jpegoptim sur <a href="https://github.com/tjko/jpegoptim">le Github du projet</a>.',
	'configurer_jpegoptim_options_label' => 'Arguments pour jpegoptim',

	'configurer_png_label' => 'Traitement des PNG',
	'configurer_png_explication' => 'L\'optimisation des png peut se faire en deux étapes : la première utilise le compresseur <strong>pngquant</strong>, la seconde optimise le résultat avec <strong>optipng</strong>. Vous pouvez cependant décider de ne traiter vos png que partiellement.',
	'configurer_png_attention' => 'L\'optimisation des png doit donc trouver sur votre serveur les binaires <strong>pngquant</strong> et <strong>optipng</strong>.',
	'configurer_pngquant_activer_label_case' => 'Activer l\'optimisation des png avec pngquant',
	'configurer_pngquant_attention' => '<strong>pngquant</strong> semble être disponible sur votre serveur.',
	'configurer_pngquant_attention_absent' => '<strong>pngquant</strong> semble ne pas être disponible sur votre serveur.',
	'configurer_pngquant_options_explication' => 'Les arguments de la commande pngquant sont disponibles sur <a href="https://github.com/kornelski/pngquant">le Github du projet</a>',
	'configurer_pngquant_options_label' => 'Arguments pour pngquant',
	'configurer_optipng_activer_label_case' => 'Activer l\'optimisation des png avec optipng',
	'configurer_optipng_attention' => '<strong>optipng</strong> semble être disponible sur votre serveur.',
	'configurer_optipng_attention_absent' => '<strong>optipng</strong> semble ne pas être disponible sur votre serveur.',
	'configurer_optipng_options_explication' => 'La documentation de optipng est disponible depuis <a href="http://optipng.sourceforge.net/">son site officiel</a>.',
	'configurer_optipng_options_label' => 'Arguments pour optipng',

	'configurer_gifsicle_label' => 'Traitement des GIF',
	'configurer_gifsicle_attention' => 'L\'optimisation des gif utilise le binaire <strong>gifsicle</strong> qui semble être installé sur votre serveur.',
	'configurer_gifsicle_attention_absent' => 'L\'optimisation des gif utilise le binaire <strong>gifsicle</strong> qui semble ne pas être installé sur votre serveur.',
	'configurer_gifsicle_activer_label_case' => 'Activer l\'optimisation des gif avec gifsicle',
	'configurer_gifsicle_options_explication' => 'Pour les options de gifsicle, consulter <a href="https://www.lcdf.org/gifsicle/man.html">son manuel</a>.',
	'configurer_gifsicle_options_label' => 'Arguments pour gifsicle',


];
