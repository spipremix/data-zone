<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
	'optimages_description' => 'Images optimisées permet de réduire le poids des images lors de leur téléversement.',
	'optimages_nom' => 'Images optimisées',
	'optimages_slogan' => 'Optimisation automatisée des images'
];
