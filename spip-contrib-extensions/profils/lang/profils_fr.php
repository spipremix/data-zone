<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/profils.git

return [

	// B
	'bouton_previsu' => 'Aperçu',

	// C
	'configurer_email_unique_explication' => 'SPIP bloque déjà l’inscription d’une personne lorsque l’email est utilisé dans autre un compte. Cette option permet en plus d’interdire le changement après-coup de son email pour un déjà utilisé ailleurs, lorsqu’on met à jour son profil.',
	'configurer_email_unique_label_case' => 'Interdire l’utilisation du même email dans plusieurs comptes.',
	'configurer_email_unique_prevenir_explication' => 'Par défaut, lors de la création par une autre personne, si l’email existe déjà, on remplace les infos par celles venant d’être envoyées dans le profil existant. Cette option force l’affichage d’une erreur.',
	'configurer_email_unique_prevenir_label_case' => 'Toujours prévenir que le compte existe déjà, plutôt que remplacer les infos du profil déjà existant.',
	'configurer_id_profil_defaut_label' => 'Profil par défaut',
	'configurer_titre' => 'Configuration des Profils',
	'configurer_utiliser_annuaires_co_non' => '(les annuaires sont actuellement désactivés).',
	'configurer_utiliser_annuaires_co_oui' => '(les annuaires sont actuellement activés).',
	'configurer_utiliser_annuaires_explication' => 'Si les annuaires sont activés dans le plugin Contacts & organisations, alors chaque profil aura automatiquement un annuaire attribué',
	'configurer_utiliser_annuaires_label_case' => 'Utiliser des annuaires',

	// E
	'erreur_autoriser_profil' => 'Ce compte n’existe pas ou vous n’avez pas le droit de le modifier',
	'erreur_email_unique' => 'Cette adresse est déjà utilisée dans un compte existant. Veuillez en saisir une autre.',
	'erreur_email_unique_admin' => 'Cette adresse est déjà utilisée <a href="@url@">dans un compte existant</a>. Veuillez en saisir une autre.',

	// M
	'message_erreur_importer_format' => 'Format des entêtes non supporté.',
	'message_erreur_importer_jobs' => '@nb_jobs@ importations lancées avec succès MAIS @nb_erreurs@ n’ont pas pu être lancées !',
	'message_ok_importer' => 'Lancement de @nb@ importations de profils dans la liste des travaux.',

	// P
	'profils_titre' => 'Profils',
];
