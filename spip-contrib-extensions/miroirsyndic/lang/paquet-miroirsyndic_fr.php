<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-miroirsyndic
// Langue: fr
// Date: 29-01-2025 17:36:27
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

return array(

	// M
	'miroirsyndic_description' => 'Ce plugin recopie les articles syndiqués {et publiés} de spip_syndic_articles vers spip_articles. Le code est suffisamment simple pour être modifiable en fonction des besoins.

Les articles sont organisés automatiquement selon un rubriquage «intelligent»: au départ, SPIP positionne l\'article dans la rubrique où se trouve le site syndiqué. Si l\'article à recopier dispose d\'un tag de \'category\', SPIP crée la sous-rubrique correspondante (voire, arborescente si le tag s\'écrit sous la forme «Chemin/Vers/Rubrique»). S\'il n\'y a pas de tag de ce type, la sous-rubrique "année/année-mois" est créée et reçoit l\'article. (Un paramètre _MODE_RUBRIQUE_MIROIR permet de supprimer ce comportement, ou de se limiter au tri par mois.)',
	'miroirsyndic_slogan' => 'Ce plugin recopie les articles syndiqués {et publiés} de spip_syndic_articles vers spip_articles',
);
