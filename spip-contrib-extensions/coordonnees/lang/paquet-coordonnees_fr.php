<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'coordonnees_nom' => "Coordonnées",
	'coordonnees_slogan' => "Adresses postales, numéros de téléphone, courriels et adresses web sur les objets",
	'coordonnees_description' => "Ajoute la possibilité de donner des coordonnées aux objets de SPIP, et tout particulièrement aux auteurs : adresses postales, courriels, numéros de téléphone et adresses web. Plusieurs coordonnées peuvent ainsi être données à chaque auteur.",
);
