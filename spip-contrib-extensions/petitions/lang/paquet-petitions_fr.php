<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/petitions.git

return [

	// P
	'petitions_description' => 'Pétitions dans SPIP',
	'petitions_slogan' => 'Gestion des pétitions dans SPIP',
];
