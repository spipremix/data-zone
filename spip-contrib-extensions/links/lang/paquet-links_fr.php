<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-links
// Langue: fr
// Date: 16-09-2019 18:49:39
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'links_description' => 'Styles et pictos explicitant les liens du site : documents à télécharger, liens externes, mailto, etc. (avec ouverture possible en nouvelle fenêtre).',
	'links_slogan' => 'Styles et pictos explicitant les liens du site',
);
?>