<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/gis_geometries.git

return [

	// G
	'gisgeom_description' => 'Ce plugin ajoute la prise en charge des formes géométriques à GIS. Il utilise les fonctions spatiales de MySQL disponibles à partir de la version 4.1.',
	'gisgeom_slogan' => 'Prise en charge de formes géométriques dans GIS',
];
