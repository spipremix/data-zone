<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = [

	// exemple d'idiome de langue pour une aide par objet (et ici l'exec configurer_aide_par_objet)
	'configurer_aide_par_objet_texte' => 'L’aide est une boite qui apparaitra à gauche lors de la consultation de l’objet ou de l’exec. Pour la mettre en oeuvre, il faut définir deux idiomes de langue, l’un intitulé <code>OBJET_titre</code> - qui sera le titre de la boite -, l’autre intitulé <code>OBJET_texte</code> - qui sera le contenu de la boite. Cette présente bulle d’aide est un exemple que vous trouverez dans le fichier <code>lang/aide_par_objet_fr</code> !<br><br>L’idiome <code>OBJET_titre</code> peut vous permettre de transmettre des arguments permettant de personnaliser la boite, en les plaçant dans les balises suivantes :<br><small>&lt;img>cadenas-24.svg&lt;/img><br>&lt;class>basic inverse&lt;/class><br>&lt;class_titre>centered&lt;/class_titre></small>', 
	'configurer_aide_par_objet_titre' => 'Comme ça marche ?',

	// autres idiome de langue nécessaire au plugin
	'cfg_label_aides' => 'Afficher l’aide',
	'cfg_label_sans_aide' => 'Désactivée',
	'cfg_label_titre' => 'Aide par Objet',
	'cfg_auteur_label' => 'L’affichage de l’aide est :',
	'cfg_aide_par_objet_forcee' => 'Forcer l’aide.',
	'cfg_aide_par_objet_explication' => 'Vous pouvez accepter ou refuser l’affichage de l’aide dans vos préférences, à moins que l’affichage ne soit forcé dans la configuration.',
	'cfg_titre_configurations' => 'Configuration du plugin Aide par Objet',
	'cfg_titre_parametrages' => 'Paramétrages',

];
