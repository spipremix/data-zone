<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/filtres_images.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'aide_par_objet_description' => 'Une aide aux utilisateurs, par objet ou exec, dans la partie privée, à gauche.',
	'aide_par_objet_slogan' => 'Une aide pour vos objets dans la partie gauche.',
	'aide_par_objet_nom' => 'Aide par Objet', 
];
