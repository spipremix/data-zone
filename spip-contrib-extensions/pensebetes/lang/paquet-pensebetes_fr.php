<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/pensebetes.git

return [

	// P
	'pensebetes_description' => 'Un Plugin destinée à se rappeler ce qu’on a l’intention de faire ou à rappeler à quelqu’un ce qu’il doit faire : Pense-bête mural pour la partie privée de SPIP.',
	'pensebetes_nom' => 'Pense-bêtes',
	'pensebetes_slogan' => 'Un plugin qui accroche !',
];
