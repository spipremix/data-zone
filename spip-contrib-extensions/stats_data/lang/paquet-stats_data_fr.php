<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-stats_data
// Langue: fr
// Date: 21-06-2024 14:13:46
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'stats_data_description' => 'Ajouter les vistes en cours aux stats etc...',
	'stats_data_slogan' => 'Ajouter les vistes en cours aux stats etc',
);
?>