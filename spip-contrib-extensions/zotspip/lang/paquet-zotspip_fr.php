<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/zotspip.git

return [

	// Z
	'zotspip_description' => 'Synchronise Spip avec une bibliothèque (personnelle ou collective) de références bibliographiques Zotero. Utilisez Zotero pour gérer / importer / rédiger vos références bibliographiques, puis incorporez vos références bibliographiques dans votre SPIP avec ZotSpip au travers de modèles dédiés.',
	'zotspip_slogan' => 'La puissance de Zotero dans votre SPIP',
];
