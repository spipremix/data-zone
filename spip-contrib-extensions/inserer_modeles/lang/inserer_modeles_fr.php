<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/inserer_modeles.git

return [

	// B
	'bouton_choisir' => 'Choisir',
	'bouton_inserer' => 'Insérer',

	// C
	'choisir_modele' => 'Que souhaitez-vous insérer ?',
	'choix_objets_editable' => 'Objets où proposer l’insertion de modèles.',
	'choix_objets_editable_explication' => 'Veuillez sélectionner un ou plusieurs objets sur lesquels vous désirez que le bloc d’insertion des modèles apparaissent. À noter que dans tous les cas, le porte-plume (barre typographique) proposera les modèles.',

	// E
	'erreur_choix_modele' => 'Vous devez choisir un modèle.',
	'explication_alt' => 'Si vide, le texte alternatif du document sera utilisé.',
	'explication_credits' => 'Si vide, les crédits du document seront utilisés.',
	'explication_descriptif' => 'Si vide, le descriptif du document sera utilisé.',
	'explication_titre' => 'Si vide, le titre du document sera utilisé.',

	// I
	'ignorer_modeles' => 'Ne pas proposer les modèles suivants',
	'item_center' => 'au centre',
	'item_left' => 'à gauche',
	'item_right' => 'à droite',

	// L
	'label_alignement' => 'Alignement',
	'label_alt' => 'Texte alternatif si l’image n’est pas visible',
	'label_credits' => 'Crédits',
	'label_descriptif' => 'Descriptif',
	'label_hauteur' => 'Hauteur max (px)',
	'label_id_document' => 'Document numéro',
	'label_largeur' => 'Largeur max (px)',
	'label_titre' => 'Titre',

	// M
	'masquer' => 'Masquer les champs',
	'masquer_legende' => 'Légende complète',
	'message_code_insere' => 'La balise a été insérée dans le texte.',
	'message_double_clic' => 'Double-cliquez pour insérer le modèle dans le texte.',
	'message_inserer_code' => 'Vous pouvez copier/coller le code du modèle dans votre texte. Un double-clic sur le code l’insérera automatiquement dans le champ Texte.',

	// N
	'nom_media' => 'Document',

	// O
	'outil_inserer_modeles' => 'Insérer un modèle',

	// T
	'titre_inserer' => 'Insérer @modele@',
	'titre_inserer_modeles' => 'Insérer un modèle',
	'titre_page_configurer_inserer_modeles' => 'Configurer le plugin « Insérer un modèle »',
];
