<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/inserer_modeles.git

return [

	// I
	'inserer_modeles_description' => 'Ce plugin fournit une aide à l’insertion des modèles dans vos textes à travers des formulaires de saisies. Ces formulaires sont accessibles dans la colonne de droite ou via un bouton dans le porte-plume.',
	'inserer_modeles_slogan' => 'Une aide à l’insertion des modèles dans vos textes',
];
