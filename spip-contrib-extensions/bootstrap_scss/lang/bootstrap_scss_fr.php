<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Uniquement pour le backend
// Comme les publicateurs parlent français, ce fichier n'existe que pour le français

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array();
