<?php

return [

	'contact_libre_description' => 'Formulaire de contact libre auquel on peut passer directement une adresse mail et une url de r&#233;f&#233;rence (ic&#244;ne de http://www.androidicons.com/).',
	'contact_libre_nom' => 'Contact libre',
	'contact_libre_slogan' => 'Un formulaire de contact simple'

];
