<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mesabonnes_description' => 'Module d\'abonnement pour logiciels de publipostages de type MaxBulker Mailer',
	'mesabonnes_slogan' => 'Gestion d\'abonnement pour logiciels de publipostages',
);
