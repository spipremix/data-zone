<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/organiseur.git

return [

	// O
	'organiseur_description' => 'Outils de travail éditorial en groupe',
	'organiseur_slogan' => 'Outils de travail éditorial en groupe',
];
