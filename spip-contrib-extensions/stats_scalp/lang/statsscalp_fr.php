<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// C
	'choix_jour' => 'Sélectionner le jour pour lequel vous voulez effacer les statistiques de SPIP',

	// E
	'effacer_journee' => 'Effacer les statistiques de cette journée',

	// J
	'jour_efface' => 'Les statistiques de la journée du @jour@ ont bien été effacé.',


	// S
	'statsscalp_titre' => 'Scalper les statistiques',

];
