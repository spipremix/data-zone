<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/sms.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sms_description' => 'API pour envoyer des sms simplement et utiliser des listes',
	'sms_nom' => 'SMS avec listes',
	'sms_slogan' => 'Envoyer des SMS avec votre site sous SPIP'
);
