<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable_tablesorter.git

return [

	// A
	'aide' => '{{Tri des colonnes}}

	Appuyer sur le titre d’une colonne pour la trier. Utiliser la touche <code>⇧</code> pour utiliser les autres colonnes comme tri secondaire.

	{{Filtre}}

	Dans le champ de filtre, vous pouvez :

-* Saisir une série de caractère
-* Utiliser des comparateurs mathématiques : < ; <= ; > ; >= ; = ;  ==
',
	'auteur' => 'Auteur·trice',

	// C
	'cextra' => 'Champ extra',
	'checkall' => 'Tout cocher',
	'colonnes' => 'Choix des colonnes',

	// D
	'data_pager_output' => 'De {startRow:input} à {endRow} sur {filteredRows} réponses',
	'data_pager_output_filtered' => 'De {startRow:input} à {endRow} sur {filteredRows} réponses ({totalRows} sans les filtres)',

	// E
	'exporter_csv' => 'Exporter en CSV',
	'exporter_ods' => 'Exporter en ODS (LibreOffice)',
	'exporter_xlsx' => 'Exporter en XLSX (Excel)',

	// F
	'filtre' => 'Filtre',
	'filtrer_colonne' => 'Filtrer la colonne {{label}}',

	// I
	'imprimer' => 'Imprimer le tableau',

	// N
	'nb_lignes' => 'Nombre de lignes affichées :',

	// P
	'pagination_dernier' => 'Dernier >>',
	'pagination_precedent' => '< Précédent',
	'pagination_premier' => '<< Premier',
	'pagination_suivant' => 'Suivant >',

	// R
	'resetall' => 'Réinitialiser tous les paramètres',
	'resetallconfirm' => 'Voulez-vous vraiment réinitialiser tous les réglages d’affichage du tableau ?',
	'resetfilter' => 'Réinitialiser les filtres',

	// T
	'tableau_reponses' => 'Tableau des réponses',

	// U
	'uncheckall' => 'Tout décocher',
];
