<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable_tablesorter.git

return [

	// F
	'formidable_ts_description' => 'Affichage des réponses formidable sous forme de tableaux triables et filtrables.',
	'formidable_ts_slogan' => 'Visualiser synthétiquement les réponses',
];
