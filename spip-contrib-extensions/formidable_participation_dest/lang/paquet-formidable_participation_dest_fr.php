<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable_participation_dest.git

return [

	// F
	'formidable_participation_dest_description' => 'Lorsqu’un formulaire formidable est prévu pour enregistrer la participation à un évènement, prévenir certaines personnes lors de l’inscription à l’évènement.',
	'formidable_participation_dest_slogan' => 'Optimiser le suivi des inscriptions !',
];
