<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'adminergobandeau_description' => 'SPIP 4.3 a apporté une refonte du bandeau de l\'espace privé, changeant de manière importante les zones où aller chercher telle ou telle fonction.

Ce plugin modifie l\'ordre des éléments dans la première ligne du bandeau en fonction de leur fréquence d\'utilisation et remet le lien vers la modification de l\'identité du site.',
	'adminergobandeau_nom' => 'Interface admin ergonomique pour SPIP 4.3+',
	'adminergobandeau_slogan' => 'Pour ne pas perdre les habitudes',
];
