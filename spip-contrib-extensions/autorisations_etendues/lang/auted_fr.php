<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auted_titre' => 'Autorisations étendues',
	'associer_modele' => 'Associer un modèle',
	'ajouter_un_modele' => "Ajouter un modèle",
	'auteurs' => 'Auteurs',

	// C
	'configuration_auted' => 'Autorisations étendues',
	'confirmer_dissocier_modele' => 'Confirmez-vous la dissociation de ce modèle ?',
	'charger_un_modele_pour' => 'Charger un modèle pour',

	// D
	'dissocier_modele' => 'Dissocier ce modèle',

	// E
	'editer_modeles_autorisations' => 'Editer les modèles d\'autorisations',
	'enregistrement_impossible' => 'Enregistrement impossible',
	'enregistrement_valide' => 'Enregistrement validé',

	// G
	'gerer_modele' => "Gérer les modèles d'autorisation",
	'gerer_autorisation' => "Gérer les autorisations",

	//M
	'modele' => 'Modèle',
	'modeles' => 'Modèles',

	// S
	"supprimer_modele_autorisation" => "Supprimer ce modèle d'autorisation",
	"supprimer_modele_autorisation_confirm" => "Confirmez la suppression de ce modèle d'autorisation",

	// T
	'titre_page_configurer_auted' => 'Autorisations étendues',

	//V
	'valider' => 'Valider',
);
