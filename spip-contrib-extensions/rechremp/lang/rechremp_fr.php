<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// 0-9
	'1_occurrence_dans' => '1 occurrence dans',

	// A
	'aucun_remplacement_sur' => 'Aucun remplacement sur les @objets@',
	'aucune_occurrence_trouvee' => 'Aucune occurrence trouvée dans le site.',

	// B
	'bouton_rechercher' => 'Rechercher',
	'bouton_remplacer' => 'Remplacer',

	// E
	'explication_rechremp' => 'Recherche et remplace du texte dans les objets éditoriaux de SPIP (article, rubrique...). Seuls les champs de type texte sont pris en compte.',

	// L
	'label_replace' => 'Remplacer par',
	'label_replace_yes' => 'Remplacer',
	'label_regexp' => 'La recherche est une expression régulière ?',
	'label_regexp_explication' => 'Il sera possible d’utiliser les captures dans le champ de remplacement ($0, $1, ...)',
	'label_search' => 'Rechercher',
	'label_search_tables' => 'Tables',
	'label_search_tables_explication' => 'Cocher pour limiter à certaines tables (sinon toutes)',
	'label_sensible' => 'Sensible à la casse',

	// N
	'nb_occurrences_dans' => '@nb@ occurrences dans',

	// R
	'resultat_remplacement' => 'Résultat du remplacement',

	// T
	'titre_rechremp' => 'Rechercher/Remplacer',

);
?>
