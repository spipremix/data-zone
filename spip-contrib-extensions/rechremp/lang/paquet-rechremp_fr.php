<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'rechremp_description' => 'Rechercher les occurrences d\'un texte et les remplacer par un autre texte, dans les champs de type texte des objets SPIP.',
	'rechremp_slogan' => 'Rechercher/Remplacer en base',
);
?>