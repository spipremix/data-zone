<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/rainette_themes.git

return [

	// R
	'rainette_theme_description' => 'Ce plugin est un add-on du plugin Rainette. Il fournit des jeux d’icônes et des fonctions de visualisation de ces jeux afin de choisir celui qui convient le mieux.',
	'rainette_theme_nom' => 'Thèmes d’icônes pour Rainette',
	'rainette_theme_slogan' => 'Consulter les jeux d’icônes de Rainette',
];
