<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/spip-pmb.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a' => 'à',
	'accedez_compte' => 'Accédez à votre compte',
	'accueil_site' => 'Accueil',
	'actualites' => 'Actualités',
	'adress_opac' => 'Adresse de l’OPAC',
	'ajouter_tag' => 'Etiquetez cet ouvrage',
	'annee_publication' => 'Année de publication',
	'auteur' => 'Auteurs',
	'auteurs' => 'Auteurs',
	'author_comment' => 'Commentaire',
	'author_date' => 'Date',
	'author_lieu' => 'Lieu',
	'author_pays' => 'Pays',
	'author_ville' => 'Ville',
	'author_web' => 'Site internet',
	'autres_lecteurs' => 'Les abonnés qui ont emprunté ce document ont également emprunté',

	// B
	'bonjour' => 'Bonjour',

	// C
	'catalogue' => 'Catalogue',
	'changer_logo' => 'Votre photo',
	'code_barre' => 'Code barre',
	'code_barre_abbr' => 'CB',
	'collection' => 'Collection',
	'collection_issn' => 'ISSN',
	'collection_web' => 'Site internet',
	'collections' => 'Collections',
	'collectivite' => 'Collectivité',
	'collectivite_autre' => 'Autre collectivité',
	'comment_ca_fonctionne' => 'Comment ça fonctionne ?',
	'compte_lecteur' => 'Votre compte de lecteur',
	'conferences' => 'P.U.L.P.',
	'config_pmb_logo' => 'Logo de PMB',
	'config_pmb_logo_explication' => 'Afficher le logo de PMB dans la colonne ?',
	'config_pmb_partout' => 'Afficher la colonne de PMB partout sur le site ?',
	'config_pmb_partout_explication' => 'PMB peut surcharger les fichiers de Zpip pour
	afficher sa colonne quelque soit la page du site. Par défaut sa colonne n’est
	affichée que pour les pages concernant PMB. Mettre oui ici, l’affichera partout (ou presque).',
	'configurer_pmb' => 'Configuration de SPIP-PMB',
	'configurer_rpc_json' => 'JSON-RPC (plus rapide, choisir par défaut)',
	'configurer_rpc_soap' => 'SOAP',
	'cote' => 'Cote',

	// D
	'date' => 'Date',
	'decouvrir' => 'découvrir',
	'derniers_articles' => 'Actualités',
	'disponiblite' => 'Disponibilité',
	'disponiblite_abbr' => 'Disp.',
	'donner_avis' => 'Donnez votre avis',

	// E
	'ecrire_a' => 'Écrire à',
	'editeur' => 'Éditeur',
	'editeurs' => 'Éditeurs',
	'edition' => 'Mention d’édition',
	'en_savoir_plus' => 'en savoir +',
	'exemplaires' => 'Exemplaires',

	// F
	'format' => 'Format',

	// I
	'importance' => 'Importance',
	'info_recherche_avancee' => 'Vous pouvez lancer une recherche portant sur un ou plusieurs mots<br />(titre, auteur, éditeur, ...)',
	'isbn' => 'ISBN/ISSN/EAN',

	// J
	'jsonrpc' => 'Adresse du service JSON-RPC',
	'jsonrpc_pw' => 'Mot de passe',
	'jsonrpc_user' => 'Nom de l’utilisateur JSON-RPC externe',

	// L
	'lire_la_suite' => 'Lire la suite',
	'localisation' => 'Localisation',
	'localisation_abbr' => 'Loc.',

	// M
	'message_compte_lecteur' => 'Prochainement, vous disposerez d’une int\\351gration compl\\350te\\nde votre compte lecteur sur ce site. \\n\\nEn attendant, vous allez \\352tre redirig\\351s vers l’interface standard.',
	'message_recherche_avancee' => 'Prochainement, vous disposerez d’une int\\351gration compl\\350te\\nde la recherche avanc\\351e sur ce site. \\n\\nEn attendant, vous allez \\352tre redirig\\351s vers l’interface standard.',
	'mon_compte' => 'Mon compte',
	'mon_compte_necessite_identification' => 'Vous devez vous identifier pour accéder à votre compte lecteur.',
	'mots_cles' => 'Mots clés',

	// N
	'newsletter' => 'Newsletter',
	'note_contenu' => 'Note de contenu',
	'note_generale' => 'Note générale',
	'notices_consultees' => 'Notices consultées',
	'nouveautes' => 'Nouveautés du catalogue',
	'numero' => 'Numéro',

	// O
	'ouvrage_trouve' => 'ouvrage trouvé',
	'ouvrages' => 'Ouvrages',
	'ouvrages_trouves' => 'ouvrages trouvés',

	// P
	'parametrage_catalogue' => 'Paramétrage du catalogue',
	'parametrage_options' => 'Options du plugin SPIP-PMB',
	'parametrage_plugin' => 'Paramétrage du plugin',
	'pas_d_ouvrages_trouves' => 'Aucun ouvrage trouvé dans le catalogue',
	'pmb_login' => 'Identifiant',
	'pmb_motdepasse' => 'Mot de passe',
	'presentation' => 'Présentation',
	'prets_en_cours' => 'Prêts en cours',
	'prets_en_retard' => 'Prêts en retard',
	'prix' => 'Prix',
	'publisher_address1' => 'Adresse',
	'publisher_address2' => 'Adresse (suite)',
	'publisher_city' => 'Ville',
	'publisher_country' => 'Pays',
	'publisher_web' => 'Site internet',
	'publisher_zipcode' => 'CP',

	// R
	'rang' => 'Rang',
	'recherche' => 'Recherche',
	'recherche_avancee' => 'Recherche avancée',
	'recherche_catalogue' => 'Dans le catalogue',
	'recherche_dans' => 'Recherche dans',
	'recherche_portail_web' => 'Dans le site',
	'rechercher_dans_catalogue' => 'Rechercher dans le catalogue',
	'reservation_ko' => 'Votre réservation à n’a pas pu être prise en compte.',
	'reservation_ok' => 'Votre réservation à été prise en compte.',
	'reservations' => 'Réservations',
	'reserver_ouvrage' => 'Réservez cet ouvrage',
	'resultats' => 'Résultats',
	'resultats_dans_catalogue' => 'Résultats dans le catalogue',
	'resultats_dans_site' => 'Résultats dans le site',
	'resultats_precedents' => 'Résultats précédents',
	'resultats_suivants' => 'Résultats suivants',
	'retour' => 'Retour',
	'retour_recherche' => '[Retour aux résultats de la recherche]',
	'rpc_type' => 'Type de serveur',
	'rubriques' => 'Qui sommes-nous ?',

	// S
	'se_deconnecter' => 'Se déconnecter',
	'section' => 'Section',
	'serie' => 'Série',
	'session_expire' => 'Votre session de lecteur a expiré. Veuillez vous déconnecter, puis vous identifier de nouveau.',
	'source' => 'Lien sur cette ressource',
	'sous_collection' => 'Sous collection',
	'support' => 'Support',
	'sur' => 'sur',

	// T
	'titre' => 'Titre',
	'titre_lien_pmb' => 'PMB, un SIGB entièrement libre',
	'tous_coupsdecoeur' => 'toutes les critiques',
	'toute_actualite' => 'toute l’actualité',
	'toutes_nouveautes' => 'toutes les nouveautés',
	'type' => 'Type',

	// U
	'unite_materielle' => 'Unité matérielle',
	'url' => 'Adresse',

	// V
	'vous_etes_identifies' => 'Vous êtes identifié',

	// W
	'wsdl' => 'Adresse du WSDL SOAP'
);
