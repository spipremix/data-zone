<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/svp.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_actualiser' => 'Actualiser',
	'bouton_actualiser_tout' => 'Actualiser les dépôts',
	'bouton_supprimer' => 'Supprimer',
	'bulle_actualiser_depot' => 'Actualiser les paquets du dépôt',
	'bulle_actualiser_tout_depot' => 'Actualiser les paquets de tous les dépôts',
	'bulle_aller_depot' => 'Se rendre sur la page de ce dépôt',
	'bulle_aller_documentation' => 'Se rendre sur la page de documentation',
	'bulle_aller_plugin' => 'Se rendre sur la page du plugin',
	'bulle_supprimer_depot' => 'Supprimer le dépôt et ses paquets',
	'bulle_telecharger_archive' => 'Télécharger l’archive',
	'bulle_telecharger_fichier_depot' => 'Télécharger le fichier XML du dépôt',

	// C
	'config_depot_editable' => 'Permettre l’édition des dépôts ?',
	'config_depot_editable_explication' => 'Cela rend éditable les informations d’un dépôt et permet ainsi de leur joindre éventuellement des mots-clés ou documents.',

	// I
	'info_aucun_depot_ajoute' => 'Aucun dépôt disponible !<br /> Utilisez le formulaire ci-dessous pour ajouter le dépôt le dépôt principal des plulgins SPIP dont l’url est déjà pré-remplie ou un autre dépôt de votre choix.',
	'info_boite_depot_gerer' => '<strong>Cette page est uniquement accessible aux webmestres du site.</strong><p>Elle permet l’ajout et l’actualisation des dépôts de plugins.</p>',
	'info_fichier_depot' => 'Saisissez l’url du fichier de description du dépôt à ajouter',

	// L
	'label_1_autre_contribution' => 'autre contribution',
	'label_actualise_le' => 'Actualisé le',
	'label_modifie_le' => 'Modifié le',
	'label_n_autres_contributions' => 'autres contributions',
	'lien_documentation' => 'Documentation',

	// M
	'message_nok_aucun_paquet_ajoute' => 'Le dépôt « @url@ » ne fournit aucun nouveau paquet par rapport à la base déjà enregistrée. Il n’a donc pas été ajouté',
	'message_nok_champ_obligatoire' => 'Ce champ est obligatoire',
	'message_nok_depot_deja_ajoute' => 'L’adresse « @url@ » correspond à un dépôt déjà ajouté',
	'message_nok_sql_insert_depot' => 'Erreur SQL lors de l’ajout du dépôt @objet@',
	'message_nok_url_depot_incorrecte' => 'L’adresse « @url@ » est incorrecte',
	'message_nok_xml_non_conforme' => 'Le fichier XML « @fichier@ » de description du dépôt n’est pas conforme',
	'message_nok_xml_non_recupere' => 'Le fichier XML « @fichier@ » n’a pas pu être récupéré',
	'message_ok_depot_ajoute' => 'Le dépôt « @url@ » a été ajouté.',

	// O
	'onglet_depots' => 'Gestion des dépôts',
	'onglet_configuration' => 'Configuration du plugin',

	// T
	'titre_menu' => 'Référentiel des plugins',
	'titre_form_ajouter_depot' => 'Ajouter un dépôt',
	'titre_form_configurer' => 'Configuration du plugin',
	'titre_page_configurer' => 'Référentiel des Plugins',
	'titre_page_depots' => 'Référentiel des plugins',
);
