<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/svp.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'svpbase_description' => 'Ce plugin construit le référentiel des plugins SPIP (équivalent à l\'annuaire Plugins SPIP) et fournit une API REST pour interroger le référentiel. SVP Référentiel est donc utilisé par le site Plugins SPIP pour présenter les plugins disponibles et par SVP pour obtenir les plugins disponibles avec une version de SPIP.',
	'svpbase_slogan' => 'Référentiel des plugins & API REST'
);
