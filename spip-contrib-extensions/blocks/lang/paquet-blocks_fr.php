<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// B
	'blocks_description' => '',
	'blocks_nom'         => 'Blocks',
	'blocks_slogan'      => 'Composer des pages avec des blocs',
];
