<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//C
	"titre" => "Modération modérée",
	"0minirezo_config" => "Activer la modération modérée pour les administrateurs enregistrés",
	"1comite_config" => "Activer la modération modérée pour les rédacteurs enregistrés",
	"6forum_config" => "Activer la modération modérée pour les visiteurs enregistrés",

	);
?>
