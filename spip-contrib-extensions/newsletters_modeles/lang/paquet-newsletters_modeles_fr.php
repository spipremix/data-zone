<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'newsletters_modeles_description' => 'Des modèles pour mettre en forme le contenu des newsletters du plugin Newsletters.',
	'newsletters_modeles_nom' => 'Modèles pour Newsletters',
	'newsletters_modeles_slogan' => 'Mettez en forme le contenu de vos newsletters',
);
