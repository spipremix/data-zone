<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/grenier.git

return [

	// G
	'grenier_description' => 'Fonctions et API dépréciées',
	'grenier_slogan' => 'Fonctions et API SPIP dépréciées',
];
