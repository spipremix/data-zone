<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'pdform_description' => 'Pour tester le plugin, installer l’exemple via le menu Edition|Patrons, dans la proposition de peuplement qui sera faite sur la colonne de gauche.',
	'pdform_nom' => 'PDForm',
	'pdform_slogan' => 'Créer un PDF au milimètre près en utilisant des objets.',
];