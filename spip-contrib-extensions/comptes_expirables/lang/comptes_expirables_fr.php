<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// B
	'bouton_reactiver' => 'Réactiver le compte',
	'bouton_configurer' => 'Configurer les comptes expirables',
	'bouton_programmer' => 'Programmer',
	'bouton_afficher_auteurs_expirables' => 'Afficher les comptes expirables',
	'bouton_job_retrograder' => 'Rétrograder les comptes expirés',
	'bouton_job_retrograder_verif' => 'Rétrograder tous les comptes ayant dépassé leur date d’expiration et sans le statut rétrogradé ?',
	'bouton_job_verifier' => 'Vérifier les comptes expirables',
	'bouton_job_verifier_verif' => 'Poser une date d’expiration aux comptes vérifiant les conditions ?',

	// C
	'comptes_expirables_titre' => 'Comptes expirables',
	'champ_expiration_label' => 'Expiration',
	'champ_en_ligne_label' => 'Activité',
	'champ_date_expiration_label' => 'Date d’expiration',
	'champ_date_expiration_explication' => 'Date d’expiration programmée pour ce compte',
	'champ_raison_expiration_label' => 'Raison de l’expiration',
	'champ_statut_label' => 'Statut',
	'cfg_champ_statuts_label' => 'Statuts rétrogradés',
	'cfg_champ_statuts_explication' => 'Choisissez les statuts à appliquer aux comptes une fois la date d’expiration dépassée.
	<br>À défaut de statut indiqué, ces types de comptes ne pourront pas avoir de date d’expiration.',
	'cfg_champ_fieldset_automatismes_label' => 'Automatismes',
	'cfg_champ_fieldset_automatismes_explication' => 'Les options suivantes permettent de poser automatiquement des dates d’expiration',
	'cfg_champ_age_nouveaux_max_label' => 'Nouveaux comptes',
	'cfg_champ_age_nouveaux_max_explication' => 'Poser automatiquement une date d’expiration aux nouveaux comptes : délai en nombre de jours',
	'cfg_champ_age_en_ligne_max_label' => 'Inactivité',
	'cfg_champ_age_en_ligne_max_explication' => 'Poser automatiquement une date d’expiration en cas d’inactivité : délai en nombre de jours',
	'cfg_champ_fieldset_notifications_label' => 'Notifications',
	'cfg_champ_notifications_label' => 'Notifications',
	'cfg_champ_notifications_explication' => 'Destinataires à notifier lors de l’expiration des comptes.',
	'cfg_champ_notifications_admins' => 'Notifier les administrateurs',
	'cfg_champ_notifications_users' => 'Notifier les possesseurs des comptes désactivés',
	'cfg_champ_email_admins_label' => 'Email des admins',
	'cfg_champ_email_admins_explication' => 'Emails des admins à notifier, séparés par des virgules. Par défaut il s’agit de l’email du webmestre.',
	'cfg_titre_parametrages' => 'Paramétrages',

	// E
	'explication_page_auteurs_expirables' => 'Cette page page affiche uniquement les comptes concernés par les dates d’expiration.

	Les critères de sélection dépendent de la configuration du plugin.

	{{Rétrogradés}} = statut expiré + date dépassée
	{{Inactifs}} = jamais connectés ou il y a trop longtemps',

	// F
	'filtre_en_ligne' => 'Activité',
	'filtre_en_ligne_non' => 'Inactifs',
	'filtre_en_ligne_oui' => 'Actifs',
	'filtre_statut' => 'Statut',
	'filtre_statuts_expires' => 'Expirés',
	'filtre_statuts_expires_non' => 'Non expirés',
	'filtre_retrogrades' => 'Rétrogradés',
	'filtre_retrogrades_oui' => 'Rétrogradés',
	'filtre_retrogrades_non' => 'Non rétrogradés',
	'filtre_date' => 'Date d’expiration',
	'filtre_date_avec' => 'Avec date',
	'filtre_date_sans' => 'Sans date',
	'filtre_date_future' => 'Date future',
	'filtre_date_passee' => 'Date dépassée',
	'filtre_autres' => 'Autres',
	'filtre_tous' => 'Tous',
	'filtre_toutes' => 'Toutes',
	'filtre_null' => '∅',

	// I
	'info_expiration_future' => 'Expiration programmée le @date@',
	'info_expiration_passee' => 'Date d’expiration passée le @date@',
	'info_retrograde' => 'Compte rétrogradé le @date@',
	'info_en_ligne_jamais' => '∅',
	'info_en_ligne_maintenant' => 'Aujourd’hui',
	'info_en_ligne_1_jour' => 'Il y a 1 jour',
	'info_en_ligne_nb_jours' => 'Il y a @nb@ jours',
	'info_raison' => 'Raison : @raison@',
	'info_droits_statut' => '@statut@ : @droits@',
	'info_droits_statut_0minirezo' => 'droits complets',
	'info_droits_statut_1comite' => 'connexion au privé et au public',
	'info_droits_statut_6forum' => 'connexion au public',
	'info_droits_statut_7desactive' => 'aucune connexion',
	'info_droits_statut_nouveau' => 'aucune connexion',
	'info_droits_statut_5poubelle' => 'suppression du compte',

	// N
	'notification_retrograder_admins_sujet' => 'Rétrogradation de @nb@ compte(s)',
	'notification_retrograder_admins_texte' => 'Les comptes suivants ont dépassé leur date d’expiration, ils ont été rétrogradés : @ids@',
	'notification_retrograder_admins_notif_auteurs' => 'Les personnes ont été notifiées individuellement par mail.',
	'notification_retrograder_admins_reinstituer' => 'Ils peuvent être réinstitués manuellement en changeant leurs statuts dans l’espace privé : @url_admin@',
	'notification_retrograder_auteur_sujet' => 'Modification de vos droits d’administration',
	'notification_retrograder_auteur_greetings' => 'Bonjour @nom@,',
	'notification_retrograder_auteur_intro' => 'Vos droits d’administration ont été modifiés.',
	'notification_retrograder_auteur_raison' => 'Vos droits d’administration ont été modifiés pour la raison suivante : @raison@.',
	'notification_retrograder_auteur_droits' => 'Nouveaux droits : ',
	'notification_retrograder_auteur_statut_5poubelle' => 'plus de connexion au site, compte supprimé.',
	'notification_retrograder_auteur_statut_6forum' => 'connexion au site public mais pas à l’espace privé.',
	'notification_retrograder_auteur_statut_7desactive' => 'plus de connexion au site.',
	'notification_retrograder_auteur_contact' => 'Contactez-nous si vous souhaitez retrouver un accès.',
	'notification_retrograder_auteur_contact_email' => 'Contactez-nous si vous souhaitez retrouver un accès : @email@',

	// R
	'raison_date_manuelle' => 'Date d’expiration programmée manuellement',
	'raison_echeance_inactif' => 'Compte jamais connecté',
	'raison_echeance_inactif_jours' => 'Compte non connecté pendant @jours@ jour(s)',
	'raison_echeance_creation' => 'Échéance de @jours@ jour(s) après création du compte',

	// S
	'statut_7desactive' => 'desactivé',

	// T
	'titre_page_configurer_comptes_expirables' => 'Configuration des comptes expirables',
	'titre_page_comptes_expirables' => 'Comptes expirables',
	'titre_droits' => 'Droits des statuts',
];
