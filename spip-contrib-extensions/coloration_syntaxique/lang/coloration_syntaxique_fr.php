<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'copie' => 'Copié !',
	'copier' => 'Copier',

	// E
	'erreur' => 'Erreur !',
	'exemple_d_apercu_des_notes' => 'Exemple d\'aperçu des notes',
	'exemple_de_code' => 'Exemple de code : @nom_du_langage@',

	// I
	'inserer_un_code_preformate' => 'Insérer un code @nom_du_langage@ préformaté',
	'inserer_un_exemple_de_code' => 'Insérer un exemple de code (@raccourci@)',

	// L
	'la_librairie_prismjs_est_manquante' => 'La librairie PrismJS est manquante !',
	'le_chargement_du_fichier_a_echoue' => 'Le chargement du fichier @type_du_fichier@ @chemin_du_fichier@ a échoué !',
	'le_chemin_du_fichier_est_manquant' => 'Le chemin du fichier @type_du_fichier@ est manquant !',
	'le_type_de_fichier_est_invalide' => 'Le type de fichier @type_du_fichier@ est invalide !',
);