<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'coloration_syntaxique_description' => '
Le plugin {{Coloration Syntaxique}} ajoute la coloration syntaxique à tous les langages de programmation supportés par la librairie [PrismJS->https://github.com/PrismJS/].

Le plugin inclut aussi la prise en charge des langages `spip` (pour la typographie SPIP) et `spip-squelette` (pour les squelettes SPIP).

Un nouveau raccourci SPIP `<exemple_de_code>` est disponible pour présenter proprement du code `html` ou `spip` dans vos contenus éditoriaux.
Ce raccourci affichera successivement le rendu puis le code source de l\'exemple.
',
	'coloration_syntaxique_nom' => 'Coloration Syntaxique',
	'coloration_syntaxique_slogan' => 'Du code en couleur pour plus de 500 langages de programmation avec PrismJS',
);