<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'matomo_nom' => 'Matomo',
	'matomo_slogan' => 'Interface avec Matomo',
	'matomo_descriiptiopn' => '
			<p>Interface pour configurer la remontée 
			de statistiques vers Matomo</p>
			<p>La configuration du plugin requert le numéro
			de site configuré dans Matomo, ainsi que 
			le nom DNS du serveur Matomo.</p>
			<p>Remplace le plugin spip_piwik et récupère
			sa configuration.</p>
			
		',
);

?>
