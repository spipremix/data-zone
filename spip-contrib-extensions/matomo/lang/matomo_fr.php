<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'configurer' => 'Configurer Matomo',
	'configuration' => 'Configuration',
	'site_id' => 'Id du site',
	'url' => 'Adresse Matomo',
);

?>
