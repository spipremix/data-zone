<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/archive_objet.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_archiver_label' => 'Archiver',
	'action_definir_motif_label' => 'Définir un motif',
	'action_desarchiver_label' => 'Désarchiver',
	'action_effacer_label' => 'Effacer les données d’archivage',
	'action_modifier_motif_label' => 'Modifier le motif',
	'action_vider_label' => 'Effacer toutes les traces d’archivage de la base',
	'archives_boite_info_message' => 'Cette page permet aux administrateurs du site de consulter l’ensemble des archives pour les types de contenu archivables.',
	'archives_onglet_titre' => 'Contenus archivés',
	'archives_page_titre' => 'Liste des contenus archivés',

	// C
	'configuration_archivage_enfant_label' => 'Permettre de modifier, indépendamment de la racine initiatrice, l’état d’archivage d’un descendant',
	'configuration_avertissement' => 'Le paramétrage de ce plugin est extrêmement sensible car il peut induire une <strong>réinitialisation partielle ou totale du contexte d’archivage des contenus</strong>.
	Veillez donc à bien lire les avertissements liés à chaque paramètre.',
	'configuration_desarchivage_explication' => 'Si vous désactivez la consignation du désarchivage, les contenus désarchivés seront réinitialisés.',
	'configuration_desarchivage_label' => 'Consigner le désarchivage',
	'configuration_motif_explication' => 'Si vous désactivez l’utilisation du motif, les contenus avec motif verront leur motif supprimé.',
	'configuration_motif_label' => 'Préciser le motif de chaque archivage ou désarchivage (sauf pour les descendants)',
	'configuration_onglet_titre' => 'Paramétrage de l’archivage',
	'configuration_page_titre' => 'Plugin @plugin@',
	'configuration_reinit_consigner_desarchivage' => 'la date d’archivage des contenus désarchivés à été remise à zéro : @nb@',
	'configuration_reinit_desactiver_enfants' => 'les types de contenus enfants suivants ont été totalement réinitialisés : @nb@',
	'configuration_reinit_desactiver_tables' => 'les types de contenus suivants ont été totalement réinitialisés : @nb@',
	'configuration_reinit_utiliser_motif' => 'le motif d’archivage à été effacé des contenus en possédant : @nb@',
	'configuration_tables_autorisees_enfants' => 'enfants : @enfants@',
	'configuration_tables_autorisees_erreur' => 'Les contenus @objets@ ne sont pas autorisés alors que les contenus parents le sont',
	'configuration_tables_autorisees_explication' => 'Si vous enlevez l’autorisation d’archivage d’un type de contenu, les contenus de ce type possédant une trace d’archivage seront réinitialisés ainsi que leurs enfants d’autres types.',
	'configuration_tables_autorisees_label' => 'Choisissez les contenus que vous voulez autoriser à l’archivage',

	// D
	'date_label' => 'Depuis le',
	'desarchives_onglet_titre' => 'Contenus désarchivés',
	'desarchives_page_titre' => 'Liste des contenus désarchivés',

	// E
	'edition_motif_archive_titre' => 'Modifier le motif d’archivage',
	'edition_motif_desarchive_titre' => 'Modifier le motif de désarchivage',
	'edition_motif_label' => 'Choisir le motif',
	'erreur_modifier_archivage_motif' => 'Erreur lors de la modification du motif en base.',
	'erreur_modifier_archivage_non_autorisee' => 'Vous n’avez pas les droits nécessaires pour modifier le motif.',

	// L
	'liste_archive_prefixe_titre' => 'Archives',

	// M
	'menu_titre' => 'Suivi de l’archivage',
	'motif_archive_defaut_label' => 'archivage standard',
	'motif_archive_racine_label' => 'archivage de la racine <a href="@url_racine_archive@">@titre_racine_archive@ (@objet_racine_archive@-@id_racine_archive@)</a>',
	'motif_desarchive_defaut_label' => 'erreur d’archivage',
	'motif_desarchive_racine_label' => 'désarchivage de la racine @objet_racine_archive@-@id_racine_archive@',

	// O
	'objet_message_archive' => 'Ce contenu a été archivé le @date@.',
	'objet_message_desarchive' => 'Ce contenu a été désarchivé le @date@.',
	'objet_message_motif' => 'Motif : @motif@.',

	// R
	'racine_label' => 'Racine'
);
