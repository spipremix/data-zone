<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/archive_objet.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'archivage_description' => 'Ce plugin fournit un mécanisme réversible pour archiver un contenu SPIP sans toucher à son éventuel statut de publication. Il enregistre la date d’archivage et permet la saisie du motif qui a conduit à l’archivage du contenu.',
	'archivage_nom' => 'Archivage de contenus',
	'archivage_slogan' => 'Un mécanisme pour archiver les différents contenus d’un site SPIP'
);
