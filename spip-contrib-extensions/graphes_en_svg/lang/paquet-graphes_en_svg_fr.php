<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'graph_en_modele_description' => 'Un Plugin destiné à permettre d’afficher des graphiques sans utiliser de librairie javascript par des modèles SPIP utilisant des SVG.',
	'graph_en_modele_nom' => 'Graph en modèle',
	'graph_en_modele_slogan' => 'Des graphs sans lib !'
];
