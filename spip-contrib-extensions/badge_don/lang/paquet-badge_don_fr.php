<?php

return [

	// B
	'badge_don_description' => 'Ce plugin permet d\'afficher un badge dans les pages publiques de votre site pour inciter les visiteurs à faire des dons.
_ Crédit icône : izo.
_ Image du badge par défaut : CC-BY Les petits débrouillards Bretagne.',
	'badge_don_slogan' => 'Un badge pour appeler au don',
];