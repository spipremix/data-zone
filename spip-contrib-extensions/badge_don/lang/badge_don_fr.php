<?php

return [

	'explication_class_don' => 'Classe(s) CSS à utiliser (exemple : "laclasse" ou "classe1 classe2").',
	'label_class_don' => 'Classe CSS',
	'explication_url_don' => 'URL ou raccourci de type SPIP (art23 ou rub32 par exemple).',
	'label_url_don' => 'Adresse de la page de dons',
	'titre_badge_don' => 'Badge pour dons',
	'titre_lien' => 'Soutenir par un don'
	
];