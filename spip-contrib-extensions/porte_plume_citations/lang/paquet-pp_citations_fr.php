<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// P
	'pp_citations_description' => 'Améliorer les citations de SPIP avec les balises q, cite et l\'attribut cite',
	'pp_citations_nom' => 'porte_plume_citations',
	'pp_citations_slogan' => 'Citer en long en large et avec les détails',
];
