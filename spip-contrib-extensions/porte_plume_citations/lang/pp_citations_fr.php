<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// P
	'pp_citations_titre' => 'porte_plume_citations',

	// O
	'outil_inserer_citer_auteur' => 'Insérer le nom d’un auteur entre les balises &lt;figcaption&gt;Un auteur&lt;/figcaption&gt;',
	'outil_inserer_balise_cite' => 'La balise &lt;cite&gt;Une référence&lt;/cite&gt; pour une référence',
	'outil_inserer_q_tag' => 'Une citation courte entre les balises &lt;q&gt;Une citation courte &lt;/q&gt;',

	// T
	'titre_page_configurer_pp_citations' => 'Configurer Porte-Plume Citations',
];