<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
	'config_espace_prive' => 'Espace privé',
	'config_espace_public' => 'Espace public',
	'config_explication_prive' => 'Rendre disponible les feuilles de style et les javascripts d’OrgChart dans l’espace privé.',
	'config_explication_public' => 'Rendre disponible les feuilles de style et les javascripts d’OrgChart dans l’espace public.',
		'demo_jquery' => '<a href="https://dabeng.github.io/OrgChar/">OrgChart</a> est un plugin pour jQuery. <a href="https://jquery.com">jQuery</a> doit donc être <a href="https://plugins.spip.net/jquery.html">installé</a> sur votre site.<br>Le fonctionnement natif de la librairie est très simple : un javascript appelle la librairie Orgchart.js qui à partir d’un flux de donnée en json propulse dans une balise HTML DIV l’organigramme.',
	'demo_js_oeuvre' => 'a) Mise en oeuvre',
	'demo_js_plus' => 'b) Pour aller plus loin...',
	'demo_spip_appel_article' => 'Appel du modèle dans un article',
	'demo_spip_appel_squelette' => 'Appel du modèle dans un squelette',
	'demo_spip_arbo_squel' => 'Un appel sans utilisation d’une boucle pourrait se faire de la façon suivante :',
	'demo_spip_arbo_titre' => 'b) Utilisation du modèle avec un objet éditorial',
	'demo_spip_code' => 'Code :',
	'demo_spip_explication' => 'Un <a href="https://www.spip.net/fr_article3454.html">modèle</a> est un petit squelette SPIP qui décrit un fragment de HTML facile à insérer dans un autre squelette ou dans le texte d’un article. Le plugin Orgchart.js vous propose un modele qui embarque :<br>1°) L’appel en javascript de la librairie Org Chart;<br>2°) des arguments pour une utilisation conviviale avec SPIP.',
	'demo_spip_titre' => 'a) Utilisation du modèle avec des données',
	'demo_spip_utilisation_avancee_boucle_objet_hasard' => 'Dans page de démonstration, nous avons affiché (ci-dessus) l’arborescence menant à l’article n°@id@ de votre site. La façon dont nous avons procédé est la suivante :',
	'demo_spip_utilisation_avancee_boucle_objet' => '1°) utilisation d’une boucle avec le modèle',
	'demo_spip_utilisation_avancee_personnalisation_article' => '2°) la personnalisation du squelette article.html',
	'demo_spip_utilisation_avancee' => 'c) Utilisation avancée du #MODELE',
	'demo_spip_utilisation_ligne' => 'La visualisation graphique de cette hiérarchie peut replacer la visualisation textuelle de l’arborescence menant à l’article. Pour ce faire, il convient de modifier le squelette article.html en retirant l’affichage textuel au dessus de l’article (p class="arbo") et en permettant l’affichage graphique sur son côté (div class="aside"). Une façon de faire est proposée ci-dessous.',
	'demo_spip_utilisation_ligne2' => 'En remplacement de la partie affichant l’arborescence de façon textuelle :',
	'demo_spip_utilisation_ligne3' => 'En ajout sur la partie latérale :',
	'demo_spip_utilisation' => 'Utilisation possible :',
	'demo_spip' => 'I - Utilisation simplifiée par un #MODELE de SPIP',
	'demo_titre' => 'Démonstration Orgchart.js',
	'demo_variante' => '(variante dépréciée)',
	'label_desactiver' => 'Désactiver',
	'label_disponibilite_orgchartjs' => 'Disponibilité de la librairie',
	'label_hierarchie_orgchartjs' => 'Hiérarchie graphique',
	'non' => 'Non',
	'oui' => 'Oui',
	'titre_orgchartjs' => 'OrgChart.js',
	'titre_page_configurer_orgchartjs' => 'Configuration d’OrgChart.js',
    
	
 );

?>