<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/depublie.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'depublie_description' => 'Permet de dépublier les articles à la date de votre choix.',
	'depublie_nom' => 'Dépublie',
	'depublie_slogan' => 'Programmer les dépublications'
);
