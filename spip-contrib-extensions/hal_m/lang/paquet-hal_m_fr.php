<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP


return [

	// H
	'hal_m_description' => 'Affiche les ressources bibliographiques HAL',
	'hal_m_nom' => 'HAL',
	'hal_m_slogan' => 'Affiche les publications HAL à travers un modèle SPIP',
];
