<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/legendes.git

return [

	// L
	'legendes_description' => 'Un plugin pour ajouter des légendes à ses images comme sur Flickr. Inspiré de [Fotonotes->http://www.fotonotes.net/] et basé sur le script [jQuery Image Annotation->http://www.flipbit.co.uk/jquery-image-annotation.html].',
	'legendes_nom' => 'Légendes',
	'legendes_slogan' => 'Légender ses photos',
];
