<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/dev.git

return [

	// D
	'dev_description' => 'Outils de développement',
	'dev_slogan' => 'Outils de développement',
];
