<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// D
	'date_de_mise_a_jour_des_informations' => 'Date de mise à jour des informations',
	'date_heure' => 'heure',
	'date_heures' => 'heures',
	'date_jour' => 'jour',
	'date_jours' => 'jours',
	'date_minute' => 'minute',
	'date_minutes' => 'minutes',
	'date_seconde' => 'seconde',
	'date_secondes' => 'secondes',
	'dimension_illimitee' => 'Dimension illimitée',
	// I
	'impossible_de_charger_la_date_de_mise_a_jour_des_informations' => 'Impossible de charger la date de mise à jour des informations !',
	'informations_du_systeme' => 'Informations du système',
	// nom des informations du système
	'information_apache_nom' => 'Apache',
	'information_base_de_donnees_nom' => 'Base de données',
	'information_php_nom' => 'PHP',
	'information_php_fuseau_horaire_par_defaut_nom' => 'Fuseau horaire par défaut',
	'information_serveur_nom' => 'Serveur',
	'information_spip_nom' => 'SPIP',
	// P
	'pixel' => 'pixel',
	'pixels' => 'pixels',
	// T
	'taille_illimitee' => 'Taille illimitée',
);