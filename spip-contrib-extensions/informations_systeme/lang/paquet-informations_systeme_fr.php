<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'informations_systeme_description' => 'Visualiser rapidement plusieurs informations concernant SPIP et le système sur lequel SPIP est installé.',
	'informations_systeme_nom' => 'Informations du système',
	'informations_systeme_slogan' => 'Tout ce que vous avez toujours voulu savoir votre SPIP en un clin d\'œil !',
);