<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-jquerysuperfish
// Langue: fr
// Date: 22-01-2012 18:19:53
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// J
	'jquerysuperfish_description' => 'Une adaptation pour Spip du plugin [jQuery Superfish->https://superfish.joelbirch.design/] à partir d\'une structure ul/li générée par exemple par le plugin [Menus->https://contrib.spip.net/Menus,3139]',
	'jquerysuperfish_slogan' => 'Affiche un menu déroulant',
);
?>
