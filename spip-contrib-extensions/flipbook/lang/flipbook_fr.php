<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


return [
	'activer_son' => 'Activer/désactiver le son',
	'afficher_vignettes' => 'Afficher les vignettes',
	'plan_vignettes' => 'Basculer entre le plan et les signets',
	'page_precedente' => 'Page précédente',
	'page_suivante' => 'Page suivante',
	'plein_ecran' => 'Basculer en mode plein écran',
	'zoom_avant' => 'Zoom avant',
	'zoom_arriere' => 'Zoom arrière',
	'afficher_aide' => 'Aide',	
	'page_unique' => 'Mode page unique',	
	'page_double' => 'Mode double page',	
	'telecharger_pdf' => 'Télécharger le PDF',
	'premiere_page' => 'Première page',
	'derniere_page' => 'Dernière page',
	'demarrer_auto' => 'Démarrer la lecture automatique',	
	'pause_auto' => 'Mettre en pause la lecture automatique',	
	'partager' => 'Partager'		
];

