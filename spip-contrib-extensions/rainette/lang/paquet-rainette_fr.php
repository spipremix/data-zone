<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/rainette.git

return [

	// R
	'rainette_description' => 'Cette nouvelle version de Rainette permet de choisir son service météo parmi OpenWeather, Weatherbit.io, WeatherAPI, AccuWeather ou World Weather Online. Une configuration est disponible pour chaque service en particulier pour saisir une clé d’enregistrement. Les affichages proposées par cette version sont incompatibles avec ceux des branches v1 et v2.

Ce plugin permet d’afficher les conditions et les prévisions météorologiques d’un lieu donné à partir du flux fourni par un des services méteorologiques supportés.
Il ne stocke aucune information en base de données ni ne gère le choix des lieux.

L’affichage des données météorologiques se fait principalement via l’utilisation de modèles dans les squelettes. Le plugin propose des 
modèles par défaut comme {{rainette_previsions}} et {{rainette_conditions}}. Il est possible aussi d’afficher les informations sur le lieu choisi soit via le modèle {{rainette_infos}},
soit via la balise <code>#RAINETTE_INFOS</code>. Tous les affichages proposés par Rainette sont personnalisables (icônes, libellés, unités, présentation...).',
	'rainette_slogan' => 'La météo au quotidien',
];
