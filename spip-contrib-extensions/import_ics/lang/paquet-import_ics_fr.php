<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/import_ics.git

return [

	// I
	'import_ics_description' => 'Importez les événements de sites distants dans votre base de données d’événements SPIP',
	'import_ics_nom' => 'Import ics',
	'import_ics_slogan' => 'Importez vos événements',
];
