<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_descriptif' => 'Indiquer votre identifiant Google Analytics pour activer Google Analytics sur votre site.',
	'cfg_titre' => 'Google Analytics',

	// E
	'explication_id_google' => 'Si "_" ou vide, supprime la fonctionnalit&eacute; (vide reviendra au d&eacute;faut).',

	// L
	'label_id_google' => 'Votre identifiant Google Analytics du type "G-1234567-1"',
	'label_ga_universal' => 'Utiliser <a href="https://support.google.com/analytics/answer/2790010">Google Analytics Universal</a>',
);
?>
