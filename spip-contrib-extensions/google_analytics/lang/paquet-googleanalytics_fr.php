<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'googleanalytics_description' => 'Ajoute automatiquement le script Google Analytics dans l\'espace public.',
	'googleanalytics_slogan' => 'Utiliser le service Google Analytics',
);
?>
