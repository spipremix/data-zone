<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable_participation.git

return [

	// T
	'traiter_participation_champ_choix_participation_explication' => 'Le champ qui propose inscription ou désinscription',
	'traiter_participation_champ_choix_participation_label' => 'Champ d’inscription',
	'traiter_participation_champ_email_label' => 'Champ de l’Email',
	'traiter_participation_champ_evenement_participation_label' => 'Champ d’évènement',
	'traiter_participation_champ_evenement_type_fixe_label' => 'Évènement fixe',
	'traiter_participation_champ_evenement_type_label' => 'Type d’évènement',
	'traiter_participation_champ_evenement_type_multichamp_label' => 'Évènements multiples - dépendants de plusieurs champs',
	'traiter_participation_champ_evenement_type_variable_label' => 'Évènement variable - dépendant d’un champ',
	'traiter_participation_champ_participant' => 'Participant·e',
	'traiter_participation_champ_participation_auto_choix_auto_label' => 'Remplir le formulaire suffit à l’inscription',
	'traiter_participation_champ_participation_auto_choix_variable_label' => 'L’inscription dépend de certains champs',
	'traiter_participation_champ_participation_auto_label' => 'Conditions d’inscription',
	'traiter_participation_champ_participation_label' => 'Champs d’inscription',
	'traiter_participation_champ_participation_non' => 'Valeur à poster pour se désinscrire',
	'traiter_participation_champ_participation_oui' => 'Valeur à poster pour s’inscrire',
	'traiter_participation_description' => 'Inscrire à un évènement',
	'traiter_participation_evenement_label' => 'Évènement',
	'traiter_participation_fieldset_participation_label' => 'Inscription',
	'traiter_participation_fieldset_plusieurs_fois_label' => 'Inscription de groupe',
	'traiter_participation_id_evenement_participation' => 'id_evenement',
	'traiter_participation_id_evenement_participation_explication' => 'Indiquer l’identifiant de l’évènement auquel s’inscrire. Possibilité d’indiquer plusieurs évènements, séparés par des virgules.',
	'traiter_participation_option_champ_nb_inscriptions_explication' => 'Cocher le ou les champs qui totalisent le nombre d’inscriptions.',
	'traiter_participation_option_champ_nb_inscriptions_label' => 'Champ indiquant le nombre d’inscriptions ou désinscriptions',
	'traiter_participation_option_champ_nom_label' => 'Champ du Nom',
	'traiter_participation_option_champ_prenom_label' => 'Champ du Prénom',
	'traiter_participation_option_plusieurs_fois_label_case' => 'Remplir une seule fois le formulaire pour plusieurs (dés)inscriptions',
	'traiter_participation_titre' => 'Inscription à un évènement',
];
