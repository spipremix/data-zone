<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

return [

	//  G
	'geographie_description' => 'Ce plugin installe 4 bases géographiques contenant Pays, Noms et indicatifs des régions, départements et commnunes de France',
	'geographie_nom' => 'Géographie',
	'geographie_slogan' => 'Bases géographiques de pays, régions, départements et communes',
];
