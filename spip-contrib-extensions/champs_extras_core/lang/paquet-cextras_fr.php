<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/champs_extras_core.git

return [

	// C
	'cextras_description' => 'Offre une API simple permettant de créer de nouveaux champs dans les objets éditoriaux.
					Il est donc le socle pour d’autres plugins notamment pour « Champs Extras Interface » qui donne
					une interface graphique de gestion de ces nouveaux champs.',
	'cextras_nom' => 'Champs Extras',
	'cextras_slogan' => 'API de gestion de nouveaux champs dans les objets éditoriaux.',
	'cextras_titre' => 'Champs Extras',
];
