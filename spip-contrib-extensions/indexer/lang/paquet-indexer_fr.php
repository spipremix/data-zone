<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// I
	'indexer_description' => 'Gestion d\'Indexation de contenus SPIP ou autre.
Intégration et requêtes possibles avec Sphinx. Nécessite Sphinx 2.2 ou version ultérieure
',
	'indexer_nom' => 'Indexer',
	'indexer_slogan' => 'Indexeur de contenus',
];
