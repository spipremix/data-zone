<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/zippeur.git

return [

	// Z
	'zippeur_description' => 'Il est parfois utile, notamment quand un article comprend beaucoup de documents joints, de proposer l’ensemble des documents sous forme de zip. Ce plugin permet à SPIP de générer une archive zip à partir d’une liste de documents.',
	'zippeur_slogan' => 'Zippez facilement vos fichiers',
];
