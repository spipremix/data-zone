<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'animation_aos_description' => 'Implémentation de la librairie Javascript AOS - Animation on Scroll.',
	'animation_aos_slogan' => 'Animez vos scrolls !',
);
