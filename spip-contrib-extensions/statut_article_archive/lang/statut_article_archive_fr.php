<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/statut_article_archive.git

return [

	// A
	'articles_archives' => 'Articles archivés',

	// C
	'cet_article_est_archive_depuis_le' => 'Cet article est archivé depuis le @date@.',
	'cet_article_est_archive_depuis_une_date_indeterminee' => 'Cet article est archivé depuis une date indéterminée !',
	'cette_configuration_est_verrouillee_vous_ne_pouvez_pas_la_modifier' => 'Cette configuration est verrouillée, vous ne pouvez pas la modifier !',
	'configuration_globale_du_plugin' => 'Configuration globale du plugin : @nom_plugin@',
	'configurer_le_plugin' => 'Configurer le plugin : @nom_plugin@',

	// E
	'erreur' => 'Erreur',

	// F
	'fermer' => 'Fermer',

	// I
	'il_n_apparait_pas_sur_le_site_public' => 'Il n’apparaît pas sur le site public.',
	'il_sera_automatiquement_supprime_dans_les_prochaines_24_heures' => 'Il sera automatiquement supprimé dans les prochaines 24 heures !',
	'il_sera_automatiquement_supprime_dans_nombre_jours_le' => 'Il sera automatiquement supprimé dans @nombre_jours@ jours, le @date@.',
	'info_article_archive' => 'Article archivé',

	// L
	'la_valeur_de_la_configuration_est_invalide' => 'La valeur de la configuration `@config@` est invalide.',
	'le_nombre_de_jours_doit_etre_superieur_a' => 'Le nombre de jours doit être supérieur à @nombre@.',
	'les_articles_archives' => 'Les articles archivés',
	'les_articles_archives_seront_automatiquement_supprimes_au_bout_de' => 'les articles « archivés » seront automatiquement supprimés au bout de @nombre_jours@ jours.',

	// M
	'mes_articles_archives' => 'Mes articles archivés',

	// N
	'nombre_de_jours_avant_la_suppression_automatique' => 'Nombre de jours avant la suppression automatique :',
	'non' => 'Non',

	// O
	'oui' => 'Oui',

	// S
	'si_la_suppression_automatique_est_activee_les_articles_avec_le_statut_archive_seront_automatiquement_supprimes_des_que_la_date_d' => 'Si la suppression automatique est activée, les articles avec le statut « archivé » seront automatiquement supprimés dès que la date « d’archivage » est dépassée du nombre de jours indiqués.',
	'succes' => 'Succès !',
	'suppression_automatique_des_articles_avec_le_statut_archive' => 'Suppression automatique des articles avec le statut « archivé »',
	'supprimer_automatiquement_les_articles_avec_le_statut_archive_apres_un_certain_temps' => 'Supprimer automatiquement les articles avec le statut « archivé » après un certain temps ?',

	// T
	'texte_statut_archive' => 'archivé',
	'tous_les_articles_archives' => 'Tous les articles archivés',

	// U
	'un_article_archive_n_apparait_pas_sur_le_site_public' => 'un article « archivé » n’apparaît pas sur le site public.',
	'une_rubrique_ayant_uniquement_des_articles_archives_n_apparait_pas_sur_le_site_public' => 'une rubrique ayant uniquement des articles « archivés » n’apparaît pas sur le site public.',
];
