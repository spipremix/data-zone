<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/statut_article_archive.git

return [

	// S
	'statut_article_archive_description' => 'Plugin minimaliste qui ajoute un statut « archivé » à la liste de choix des statuts de vos articles.',
	'statut_article_archive_nom' => 'Statut « archivé » pour les articles',
	'statut_article_archive_slogan' => 'Un statut supplémentaire pour faciliter la gestion de vos articles.',
];
