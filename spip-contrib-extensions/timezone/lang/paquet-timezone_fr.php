<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/timezone.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'timezone_description' => 'Permettre de choisir le fuseau horaire du site lorsqu’il n’est pas, par exemple, hébergé sur un serveur du même fuseau horaire.',
	'timezone_slogan' => 'Choisir le fuseau horaire du site'
);
