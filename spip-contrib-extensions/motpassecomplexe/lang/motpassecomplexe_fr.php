<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'motpassecomplexe_titre' => 'Mot de passe complexe',

	// I
	'info_passe_trop_court' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s),  @nb_maj@ majuscules(s), @nb_int@ chiffes(s) et @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb' => 'Le mot de passe doit contenir au moins @nb@ caractères',

	'info_passe_trop_court_chi' => 'Le mot de passe doit contenir au moins @nb_int@ chiffes(s).',
	'info_passe_trop_court_nb_chi' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_int@ chiffes(s).',

	'info_passe_trop_court_chi_spe' => 'Le mot de passe doit contenir au moins @nb_int@ chiffes(s) et @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb_chi_spe' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_int@ chiffes(s) et @nb_spe@ caractère(s) spéciaux.',

	'info_passe_trop_court_maj' => 'Le mot de passe doit contenir au moins @nb_maj@ majuscules(s).',
	'info_passe_trop_court_nb_maj' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_maj@ majuscules(s).',

	'info_passe_trop_court_maj_chi' => 'Le mot de passe doit contenir au moins @nb_maj@ majuscules(s) et @nb_int@ chiffes(s).',
	'info_passe_trop_court_nb_maj_chi' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_maj@ majuscules(s) et @nb_int@ chiffes(s).',

	'info_passe_trop_court_maj_chi_spe' => 'Le mot de passe doit contenir au moins @nb_maj@ majuscules(s), @nb_int@ chiffes(s) et @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb_maj_chi_spe' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_maj@ majuscules(s), @nb_int@ chiffes(s) et @nb_spe@ caractère(s) spéciaux.',

	'info_passe_trop_court_maj_spe' => 'Le mot de passe doit contenir au moins @nb_maj@ majuscules(s) et @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb_maj_spe' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_maj@ majuscules(s) et @nb_spe@ caractère(s) spéciaux.',

	'info_passe_trop_court_min_maj_chi_spe' => 'Le mot de passe doit contenir au moins @nb_min@ minuscule(s),  @nb_maj@ majuscules(s), @nb_int@ chiffes(s) et @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb_min_maj_chi_spe' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s),  @nb_maj@ majuscules(s), @nb_int@ chiffes(s) et @nb_spe@ caractère(s) spéciaux.',

	'info_passe_trop_court_min_maj_chi' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s), @nb_maj@ majuscules(s) et @nb_int@ chiffes(s).',
	'info_passe_trop_court_nb_min_maj_chi' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s), @nb_maj@ majuscules(s) et @nb_int@ chiffes(s).',

	'info_passe_trop_court_min_maj_spe' => 'Le mot de passe doit contenir au moins @nb_min@ minuscule(s), @nb_maj@ majuscules(s) et @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb_min_maj_spe' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s), @nb_maj@ majuscules(s) et @nb_spe@ caractère(s) spéciaux.',

	'info_passe_trop_court_min' => 'Le mot de passe doit contenir au moins @nb_min@ minuscule(s).',
	'info_passe_trop_court_nb_min' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s).',

	'info_passe_trop_court_min_maj' => 'Le mot de passe doit contenir au moins @nb_min@ minuscule(s) et @nb_maj@ majuscules(s).',
	'info_passe_trop_court_nb_min_maj' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s) et @nb_maj@ majuscules(s).',

	'info_passe_trop_court_min_chi' => 'Le mot de passe doit contenir au moins @nb_min@ minuscule(s) et @nb_int@ chiffes(s).',
	'info_passe_trop_court_nb_min_chi' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s) et @nb_int@ chiffes(s).',

	'info_passe_trop_court_min_spe' => 'Le mot de passe doit contenir au moins @nb_min@ minuscule(s) et @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb_min_spe' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s) et @nb_spe@ caractère(s) spéciaux.',

	'info_passe_trop_court_min_chi_spe' => 'Le mot de passe doit contenir au moins @nb_min@ minuscule(s), @nb_int@ chiffes(s)  et @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb_min_chi_spe' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_min@ minuscule(s), @nb_int@ chiffes(s) et @nb_spe@ caractère(s) spéciaux.',

	'info_passe_trop_court_spe' => 'Le mot de passe doit contenir au moins @nb_spe@ caractère(s) spéciaux.',
	'info_passe_trop_court_nb_spe' => 'Le mot de passe doit contenir au moins @nb@ caractères dont @nb_spe@ caractère(s) spéciaux.',
);
