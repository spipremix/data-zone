<?php

return [
	'url_label' => 'Adresse de la page',
	'texte_label' => 'Texte',
	'taille' => 'Taille de chaque élèment (px)',
	'ecc' => 'Correction d\'erreurs',
	'telecharger' => 'Télécharger le qrcode',
];
