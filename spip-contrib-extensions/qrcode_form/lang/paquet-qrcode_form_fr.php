<?php

return [
	'qrcode_form_description' => 'Ce plugin permet d\'insérer un formulaire générateur de QRcode dans son site.',
	'qrcode_form_slogan' => 'Autonomie pour les QRcodes !',
	'qrcode_form_nom' => 'Formulaire générateur de QRcode',
];
