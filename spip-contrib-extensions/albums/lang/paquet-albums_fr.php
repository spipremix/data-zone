<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/albums.git

return [

	// A
	'albums_description' => 'Le plugin Albums étend la gestion des documents : il ajoute la possibilité de les regrouper sous forme d’albums.
		Ce sont des conteneurs de documents pouvant être autonomes ou associés aux autres contenus.
		En complément, un modèle permet de les insérer dans le texte des contenus et les afficher de diverses façons : mini galerie d’images, liste de documents à télécharger, etc.',
	'albums_nom' => 'Albums',
	'albums_slogan' => 'Gestion de groupes de documents',
];
