<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// A
	'agenda_inscription_titre' => 'Inscription libre aux événements',
	'action_inscription_complet' => 'Désolé, l\'événement "@titre@"" est maintenant complet. Votre inscription avec l\'email @email@ n\'a pas été confirmée',
	'action_inscription_alea_invalide' => 'Désolé, votre demande d\'inscription à l\'événement "@titre@" avec l\'email @email@ n\'est plus valide ou a déjà été confirmé',
	'action_inscription_confirmee' => 'Votre inscription à l\'événement "@titre@" avec l\'email @email@ est maintenant confirmée.',
	'action_inscription_confirmee_liste_attente' => 'Vous êtes maintenant ajouté à la liste d\'attente de l\'événement "@titre@" avec l\'email @email@.',

	// B
	'bouton_inscrire_evt' => 'M\'inscrire à cet événement',
	'bouton_inscrire_evt_confirme' => 'Confirmer mon inscription',
	'bouton_inscrire_evt_confirme_attente' => 'M\'ajouter sur la liste d\'attente',

	// C
	'cfg_utiliser_reglages_facteur' => 'Utiliser les réglages du Facteur',
	'cfg_email_confirme_complement' => 'Email  de confirmation : compléments d\'informations',
	'cfg_email_confirme_complement_explication' => '(facultatif) Texte complémentaire que vous pouvez ajouter à l\'email confirmant l\'inscription à un événement',
	'cfg_titre_parametrages' => 'Paramétrages',
	'cfg_liste_attente' => 'Liste d\'attente',
	'cfg_liste_attente_explication' => 'Si vous activez le mode liste d\'attente, les personnes qui s\'ajoutent à un événement complet sont ajoutées à la liste d\'attente au lieu d\'être refusées',
	'cfg_champ_telephone_explication' => 'Activer le champ téléphone dans le formulaire d\'inscription public',
	'cfg_champ_fonction_explication' => 'Activer le champ fonction dans le formulaire d\'inscription public',
	'cfg_mode_notification' => 'Notifications et suivis',
	'cfg_mode_fonctionnement' => 'Mode de fonctionnement',
	'cfg_destinataires' => 'Personnes qui recoivent une notification lorsqu\'une inscription est confirmée',
	'cfg_destinataires_explication' => '(facultatif) Indiquer les emails séparés par des virgules. Si vide = pas de notifications',
	'corbeille_evenements_inscrits_un' => 'Une inscription dans la corbeille',
	'corbeille_evenements_inscrits_tous' => '@nb@ inscriptions dans la corbeille',


	// E
	'evenement_complet' => 'Cet événement est complet. Les inscriptions sont closes.',
	'evenement_complet_liste_attente' => 'Cet événement est complet. Vous pouvez toutefois vous inscrire sur la liste d\'attente.',
	'envoi_email_confirmation' => 'Vous allez recevoir un email sur l’adresse @email@ pour confirmer votre inscription.',
	'envoi_email_confirmation_attente' => 'Vous allez recevoir un email sur l’adresse @email@ pour confirmer votre ajout à la liste d\'attente.',
	'email_confirmation_titre' => 'Demande de confirmation d\'inscription à l\'événement',
	'email_confirmation_titre_attente' => 'Demande de confirmation d\'ajout à la liste d\'attente de l\'événement',
	'email_confirmation_email_body' => 'Bonjour, nous avons enregistré une demande d\'inscription sur notre site à l\'événement suivant',
	'email_confirmation_email_body_liste_attente' => 'Bonjour, nous avons enregistré une demande d\'ajout à la liste d\'attente pour l\'événement suivant',
	'email_confirmation_email_titre_evt' => 'Titre de l\'événement',
	'email_confirmation_email_titre_date' => 'Date',
	'email_confirmation_click' => 'Pour confirmer votre inscription, merci de cliquer sur le lien suivant :',
	'email_confirmation_click_attente' => 'Pour confirmer votre demande, merci de cliquer sur le lien suivant :',
	'email_confirme_titre' => 'Inscription confirmée à l\'événement',
	'email_confirme_email_body' => 'Bonjour, votre inscription à l\'événement suivant est maintenant confirmée',
	'email_confirme_titre_liste_attente' => 'Ajout confirmé sur la liste d\'attente de l\'événement',
	'email_confirme_email_body_attente' => 'Bonjour, vous avez bien été ajouté sur la liste d\'attente pour l\'événement suivant ',
	'email_confirme_notif_titre' => '[Inscription Agenda] Inscription confirmée à l\'événement',
	'email_confirme_notif_email_body' => 'Bonjour, une nouvelle inscription à l\'événement suivant est maintenant confirmée',
	'email_confirme_notif_titre_liste_attente' => '[Inscription Agenda] Ajout confirmé sur la liste d\'attente de l\'événement',
	'email_confirme_notif_email_body_attente' => 'Bonjour, une nouvelle inscription a été ajouté sur la liste d\'attente pour l\'événement suivant ',
	'email_voir_evt_ligne' => 'Voir l\'événement en ligne',
	'email_detail_personne_inscrit' => 'Détails sur l\'inscription',

	// L
	'label_vous_inscrire' => 'S\'incrire',

	// T
	'titre_page_configurer_agenda_inscription' => 'Configuration des inscriptions aux événements de l\'agenda',


];
