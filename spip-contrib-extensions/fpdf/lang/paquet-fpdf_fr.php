<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-fpdf
// Langue: fr
// Date: 05-09-2013 04:23:56
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// F
	'fpdf_description' => 'Ce plugin sert à générer des pdfs via la librairie fpdf.
		Adapter le en vous servant des tutoriaux du dossier demo.',
	'fpdf_slogan' => 'Librairie FPDF / FPDI pour fabriquer des pdfs.',
);
?>