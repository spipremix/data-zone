<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'ervd_dashboard_description' => 'Administration du plugin Des rendez-vous économes.',
	'ervd_dashboard_nom' => 'Des rendez-vous économes - Tableau de bord',
	'ervd_dashboard_slogan' => 'Un tableau de bord pour le pilote des calendriers et rendez-vous !',
);
