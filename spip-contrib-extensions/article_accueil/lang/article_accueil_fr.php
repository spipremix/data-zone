<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/article_accueil.git

return [

	// A
	'article_accueil' => 'Article d’accueil',
	'aucun_article_accueil' => 'Aucun article',

	// L
	'label_id_article_accueil' => 'Choisir un article',

	// R
	'rubrique_article_en_accueil' => 'Article d’accueil :',
];
