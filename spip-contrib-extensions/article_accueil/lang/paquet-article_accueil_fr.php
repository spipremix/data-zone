<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/article_accueil.git

return [

	// A
	'article_accueil_description' => 'Ce plugin vous permet de d’attribuer un article d’accueil aux rubriques. Le champ id_article_accueil peut ensuite être utilisé par les squelettes.',
	'article_accueil_nom' => 'Articles d’accueil',
	'article_accueil_slogan' => 'Attribuer un article d’accueil aux rubriques',
];
