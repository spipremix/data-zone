<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/minibando.git

return [

	// M
	'minibando_description' => 'Le plugin minibando remplace les boutons d’administration de SPIP par une barre d’outils qui reprend les éléments disponibles dans la navigation haute de l’espace privé de SPIP.',
	'minibando_slogan' => 'Un minibando pour un maximum de fonctionnalités !',
];
