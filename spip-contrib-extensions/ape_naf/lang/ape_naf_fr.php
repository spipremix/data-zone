<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'erreur_ape_naf' => 'Le code APE ou NAF est invalide.',
	'erreur_fichier' => 'L’accès au fichier constituant le répertoire est impossible.',
	'erreur_format' => 'Le fichier constituant le répertoire a un problème de format',
	'erreur_non_repertorie' => 'Le code APE ou NAF n’est pas répertorié.',
	'saisie_datas_dev' => 'La liste est établie par une fonction de l’API, <code>lire_ape_naf(\'*\',[\'libelle\' => \'avec_code\'])</code>. Il est possible de la modifier et de transmettre une liste modifiée qui se substituera à la liste par défaut.',
	'saisie_datas' => 'La liste des choix possibles sera par défaut celle du répertoire des codes APE et NAF.',
	'saisie_explication' => 'Une saisie pour entrer un ou des codes APE ou NAF.',
	'saisie_repertoire_explication' => 'La valeur <code>non</code> permet de désactiver la liste de choix et autorise une saisie libre du code (la saisie devient alors héritière d’un <code>INPUT</code>).',
	'saisie_repertoire' => 'Répertoire',
	'saisie' => 'Les codes APE ou NAF',
];