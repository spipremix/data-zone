<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/selections_editoriales.git

return [

	// C
	'configurer_accueil_prive_orphelines_label_case' => 'Afficher les sélections autonomes en page d’accueil de l’espace privé.',
	'configurer_objets_label' => 'Activer l’ajout de sélections sur les contenus :',
	'configurer_reutilisation_explication' => 'Si cette option est activée, lors de l’ajout de sélections à des contenus, l’interface permettra d’associer des sélections existantes.',
	'configurer_reutilisation_label_case' => 'Permettre de réutiliser des sélections liées',

	// I
	'info_aucune_utilisation' => 'Cette sélection n’est liée à aucun objet.',
	'info_des_utilisations' => '@nb@ utilisations',
	'info_une_utilisation' => '1 utilisation',

	// S
	'selection_vue' => 'Sélection vue dans le texte',
	'selections_editoriales_titre' => 'Sélections éditoriales',

	// T
	'titre_page_configurer_selections_editoriales' => 'Configuration des séléctions éditoriales',
];
