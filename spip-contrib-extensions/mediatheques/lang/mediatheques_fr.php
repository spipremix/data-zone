<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// M
	'mediatheques_titre' => 'Médiathèques',

	// T
	'titre_page_configurer_mediatheques' => 'Médiathèques',
];
