<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// R
	'recommandations_description' => '',
	'recommandations_nom' => 'Recommandations',
	'recommandations_slogan' => 'API de recommandation de contenus intéressants en rapport avec un contenu source',
];
