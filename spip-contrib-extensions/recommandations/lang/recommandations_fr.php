<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// R
	'recommandations_titre' => 'Recommandations',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	
	// R
	'reindexer' => 'Réindexer les attributs de recommandation',

	// T
	'titre_page_configurer_recommandations' => 'Configurer les recommandations',
];
