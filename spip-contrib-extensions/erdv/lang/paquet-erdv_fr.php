<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'erdv_description' => 'Gérer des rendez-vous de façon responsable.',
	'erdv_nom' => 'Des rendez-vous, économes',
	'erdv_slogan' => 'Une gestion des rendez-vous minimisant la collecte et la durée de conservation des données !',
];