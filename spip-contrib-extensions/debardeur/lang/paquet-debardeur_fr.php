<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

return [

	// D
	'debardeur_description' => '',
	'debardeur_nom' => 'Débardeur',
	'debardeur_slogan' => 'Charger les paquets pour SVP',
];
