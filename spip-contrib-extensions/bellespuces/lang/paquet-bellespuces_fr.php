<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-bellespuces
// Langue: fr
// Date: 03-02-2020 10:53:37
// Items: 2


return [

	// B
	'bellespuces_description' => 'Ce plugin traite le raccourci SPIP « <code>-</code> » comme « <code>-*</code> », générant de vraies listes (<code>ul/li</code>) dans les deux cas.',
	'bellespuces_slogan' => 'De vraies listes à puces avec « <code>-</code> », comme « <code>-*</code> » !',
];
