<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/image_typo.git

return [

	// I
	'image_typo_description' => 'Fournit un filtre `|image_typo` pour transformer un texte en une image.',
	'image_typo_nom' => 'Image typo',
	'image_typo_slogan' => 'Du texte à l’image !',
];
