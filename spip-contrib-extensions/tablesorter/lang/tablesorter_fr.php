<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/tablesorter.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_tablesorter' => 'Configuration du plugin tablesorter',

	// N
	'no_insert' => 'Ne pas insérer automatiquement le script et css sur les pages publiques et privées',

	// T
	'toutes_tables' => 'Appliquer tablesorter sur toutes les tables',
	'toutes_tables_explication' => 'Sinon uniquement celles qui ont une classe tablesorter'
);
