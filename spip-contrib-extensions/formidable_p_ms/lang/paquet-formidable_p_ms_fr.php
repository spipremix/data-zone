<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// F
	'formidable_p_ms_description' => 'Permet lors de l\'inscription à un évènement via formidable d\'inscrire automatiquement la personne à un segment de liste regroupants toutes les personnes qui participent à l\'évènement',
	'formidable_p_ms_nom' => 'Formidable participation : mailsubscriber',
	'formidable_p_ms_slogan' => 'Gérer la correspondance avec les participant·es'
];
