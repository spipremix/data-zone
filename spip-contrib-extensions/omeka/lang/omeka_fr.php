<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'configurer_instances_nom_label' => 'Nom de l’instance',
	'configurer_instances_nom_new' => 'Nouvelle instance',
	'configurer_instances_url_label' => 'URL de l’API',
	'configurer_titre' => 'Configurer Omeka',
	'configurer_sites_actifs_label' => 'Sites actifs sur cette instance',
	'configurer_synchro_meta_label' => 'Synchroniser les informations',
	'configurer_synchro_meta_label_case' => 'Les informations venant d’Omeka écrasent toujours celles de SPIP',
	'methode_bouton_label' => 'Choisir',
	'methode_lien_label' => 'omeka',
	'ajouter_bouton_label' => 'Ajouter un document sur cette instance',
);
