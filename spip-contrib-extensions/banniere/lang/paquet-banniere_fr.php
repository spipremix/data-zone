<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'banniere_description' => 'Le plugin {{Bannière}} ajoute la gestion d\'une image de bannière pour vos objets éditoriaux',
	'banniere_nom' => 'Bannière',
	'banniere_slogan' => 'Une bannière pour vos objets éditoriaux',
);