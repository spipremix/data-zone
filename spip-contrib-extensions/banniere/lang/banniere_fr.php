<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// B
	'banniere' => 'Bannière',
	'banniere_article' => 'Bannière de l’article',
	'banniere_auteur' => 'Bannière de l’auteur',
	'banniere_groupe' => 'Bannière de ce groupe',
	'banniere_mot_cle' => 'Bannière du mot-clé',
	'banniere_rubrique' => 'Bannière de la rubrique',
	'banniere_site' => 'Bannière du site',
	'banniere_standard_rubrique' => 'Bannière standard des rubriques',
	// D
	'deposer_la_banniere_ici' => 'Déposer la BANNIÈRE ici',
	// E
	'etes_vous_sur_de_vouloir_supprimer_la_banniere' => 'Êtes-vous sûr de vouloir supprimer la bannière ?',
	// L
	'la_banniere_a_ete_envoyee' => 'La bannière a été envoyée',
	'les_bannieres' => 'Les bannières',
	// T
	'telecharger_nouvelle_banniere' => 'Télécharger une nouvelle bannière',
	'texte_configurer_bannieres' => 'Choisissez les objets éditoriaux qui peuvent avoir une bannière. Si la bannière est active pour un objet, il disposera d\'un formulaire pour téléverser une éventuelle bannière.',
	'texte_configurer_bannieres_erreur_aucun_objet_declare' => 'Aucun objet éditorial ne peut avoir de bannière !',
	'texte_configurer_bannieres_explication' => 'Activer la bannière des objets éditoriaux suivants :',
);