<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

  // A
  'archives' => 'Archives',
  'archives_gerer' => 'Gérer les archives',
  'archives_placer' => 'Lettre placée en archive',
  'autre' => 'Autre :',
  'autres' => 'Autres :',
  'apercu' => 'Apercu de la lettre',	
  'adresse'=> 'Adresse @num@',#NEW

  // C
  'charger_derniere' => 'Charger la dernière lettre',
  'config' => 'Configurer ma lettre',
  'config_malettre'=>'Choisir l\'édito et les adresses emails courantes pour vos envois',
  'consulter' => 'Consulter',
  'compose_sujet' => 'Sujet du mail',
  'compose_non_spip' => 'raccourcis SPIP non supportés',
  'compose_contenu' => 'Texte d\'introduction',
  'compose_edito' => 'éditer ce texte',
  'compose_cochant' => 'Choisissez les articles que vous voulez publier dans la lettre en les cochant ...',
  'compose_cochant_eve' => 'Choisissez les événements que vous voulez publier dans la lettre en les cochant ...',
  'compose_liste' => 'ET / OU <br />indiquer les numéros des articles à publier séparés par une virgule',
  'compose_liste_eve' => 'ET / OU <br />indiquer les numéros des événements à publier séparés par une virgule',
  'compose_submit' => 'Ajouter à la lettre',
  'choix_lang' => 'Composer la lettre en ',
  'choix_objets'=>'Les listes d\'objets à cocher',#NEW
  
  // D
  'destinataires' => "Destinataires",

  // E
  'ecrire_nouvelle' => 'Écrire une lettre',
  'ecrire_nouvelle2' => 'Écrire une nouvelle lettre',
  'edito'=>'Édito', #NEW
  'edito_inconnu' => 'L\'édito que vous avez indiqué n\'existe pas',
  'edito_article_numero'=>'Numéro de l\'article édito<br />(<i>0: pas d\'édito</i>)',#NEW
  'erreur_ecriture' => '<strong>erreur:</strong> impossible de créer la lettre au format HTML, vérifier le paramètre chemin d\'accès et les droits en écriture (chmod 777)',
  'erreur_ecriture_stockage' => 'Répertoire de stockage de la lettre impossible à créer',
  'erreur_envoi' => 'Erreur lors de l\'envoi du mail',
  'erreur_no_dest' => 'Erreur: aucun destinataire',
  'erreur_lecture' => 'Erreur: impossible de lire le dossier',
  'expediteur' => 'Expéditeur',
  'email'=>'Email',#NEW
  'email_seulement' => "email seulement",
  'email_seulement_autres' => "emails séparés par des virgules",
  'envoi' => 'Envoi',
  'effacer' => 'Effacer',
  'effacer_confirm' => 'Etes vous sur de vouloir effacer cette lettre ?',
  'expediteurs'=>'Expéditeurs',#NEW
  'envoyer_lettre' => 'Envoyer la lettre',
 
  // I 
  'info' => 'Créer votre lettre en sélectionnant les éléments.',
  'info_archive' => 'Visualiser et gérer les lettres envoyées', #NEW
  'info_envoi' => 'Prévisualiser votre lettre et envoyer-la à ces destinataires', #NEW
  
  // L    
  'lang_toute' => 'toutes les langues',
  'lettres_dispo' => 'lettre(s) disponible(s)',
  'lettre_du' => 'lettre du',
  'lettre_txt_auto' => 'Ceci est une message automatique - ne pas repondre',
  'lettre_txt_titre' => 'LETTRE D\'INFORMATION DE',
  'lettre_txt_html_dispo' => 'La version HTML de cette lettre est disponible en ligne: ',
  'lettre_txt_unsub' => 'Modifier son abonnement: ',
  'lettre_html_unsub' => 'se désinscrire de la lettre',
  'lettre_envoyer' => 'Envoyer la lettre', #NEW
  'lien' => 'Lien',
  'lire_en_ligne' => 'Lire l\'article en ligne',
  'lister_articles'=>'Lister les articles',
  'lister_evenements'=>'Lister les événements',
  'lien_documentation' => '<a href="https://contrib.spip.net/2803" class="spip_out">Documentation</a>',#NEW

  // M
  'ma_lettre'=>'Ma lettre',
  'ma_lettre_warning' => 'Si vous n\'arrivez pas à lire correctement cette lettre, allez directement sur',
  'mes_abonnes' => 'Mes abonnés (@inscrits@ inscrits)',
  
  //N
  'nom'=>'Nom',#NEW
  'no_archive' => 'Mode test', #NEW
  'no_archive_info' => 'Ne pas archiver cet envoi', #NEW
  
  // 0
  'obligatoire' => 'Champs obligatoire',
  
  // P
  'lettre_preview_open' => 'Ouvrir dans une nouvelle fenêtre',
  
  // R
  'recharger_texte' => 'Recharger ce texte',
  
  // S
  'succes_envoi' => 'Lettre bien envoyée !',
  
  // V
  'version_html' => 'Version HTML',
  'version_txt' => 'Version Texte',
  'voir' => 'Voir'

);

