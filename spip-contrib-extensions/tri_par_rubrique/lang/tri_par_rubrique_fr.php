<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'appliquer_tri_global' => 'Appliquer un tri à toutes les rubriques du site',
	'aucun_changement' => 'Aucun changement appliqué.',
	
	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'titre_page_configurer_tri_par_rubrique' => 'Configuration du tri par rubrique',
	
	// T
	'tri_par_rubrique_titre' => 'Tri des articles par rubrique',
	'tri_sens_inverse' => 'Trier en sens inverse',
	'tri_articles' => 'Trier les articles :',
	'tri_articles_par' => 'par',
	'tri_articles_defaut' => 'défaut',
	'tri_articles_date' => 'date de publication',
	'tri_articles_id_article' => 'numéro d\'article (identifiant)',
	'tri_articles_maj' => 'date de mise à jour',
	'tri_articles_titre' => 'titre',
	'tri_articles_num_titre' => 'numéro dans le titre (10. )',
	'tri_articles_rang' => 'rang',
	'tri_articles_inverse' => 'inverse',
	'tri_des_articles' => 'Tri des articles :',
	'tri_global_applique' => 'Le tri global a été appliqué',
);
