# min33

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[adaptive_images](https://git.spip.net/spip-contrib-extensions/adaptive_images)|[3.3.0;4.*]|480|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-11-12|
|[creat2id](https://git.spip.net/spip-contrib-extensions/creat2id)|[3.3.0-dev;4.*]|0|N/A|2024-10-11|
|[rang_mots](https://git.spip.net/spip-contrib-extensions/rang_mots)|]3.3.0-dev;4.0.*]|0|N/A|2023-04-09|
