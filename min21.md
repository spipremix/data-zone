# min21

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[changestatut](https://git.spip.net/spip-contrib-extensions/change_statut)|[2.1.0;4.*]|11|4.3;4.2;4.0;3.2;3.0;2.1|2024-07-05|
|[fbantispam](https://git.spip.net/spip-contrib-extensions/fbantispam)|[2.1.0;4.*]|33|4.3;4.1;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-13|
|[forumsectorise](https://git.spip.net/spip-contrib-extensions/forum_sectorise)|[2.1.0;3.2.*]|1|3.2|< 2023-02-23|
|[gviewer](https://git.spip.net/spip-contrib-extensions/google-viewer)|[2.1.0;4.*]|41|4.3;4.2;3.2;3.1;3.0;2.1;2.0|2024-07-13|
|[greves](https://git.spip.net/spip-contrib-extensions/greves)|[2.1.0;3.2.*]|1|3.2|< 2023-02-23|
|[image_cliquable](https://git.spip.net/spip-contrib-extensions/image_cliquable)|[2.1.14;4.*]|47|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-05|
|[lazyload](https://git.spip.net/spip-contrib-extensions/jquery_lazyload)|[2.1.0;3.2.*]|6|4.3;3.2;3.0;2.1;2.0;1.9|2024-07-29|
|[medias_nettoyage](https://git.spip.net/spip-contrib-extensions/medias_nettoyage)|[2.1.0;3.2.*]|34|3.2;3.1;3.0;2.1|< 2023-02-23|
|[babbi](https://git.spip.net/spip-contrib-extensions/menu_babbibel)|[2.1.0;4.*]|69|4.3;4.2;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-13|
|[paypal](https://git.spip.net/spip-contrib-extensions/paypal)|[2.1.8;3.2.*]|6|4.1;3.2;3.1|< 2023-02-23|
|[redirhttps](https://git.spip.net/spip-contrib-extensions/redirhttps)|[2.1.11;3.2.*]|11|3.2;3.1;3.0|< 2023-02-23|
|[ressource](https://git.spip.net/spip-contrib-extensions/ressource)|[2.1.14;4.*]|9|4.3;4.2;3.2;3.1|2024-07-05|
|[varnish](https://git.spip.net/spip-contrib-extensions/varnish)|[2.1.0;3.2.99]|1|3.2|2024-03-23|
