# min30

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[abozones](https://git.spip.net/spip-contrib-extensions/abonnements_zones)|[3.0.5;4.*]|14|4.3;4.2;4.1;3.2;3.1|2025-01-23|
|[accesrestreintdate](https://git.spip.net/spip-contrib-extensions/acces_restreint_date)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[accesrestreintip](https://git.spip.net/spip-contrib-extensions/acces_restreint_ip)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[arp](https://git.spip.net/spip-contrib-extensions/acces_restreint_partiel)|[3.0.9;4.*]|7|4.4;4.2;4.1;3.2;3.0|2021-03-21|
|[accesrestreintvideos](https://git.spip.net/spip-contrib-extensions/acces_restreint_videos)|[3.0.5;3.2.*]|0|N/A|< 2023-02-23|
|[activite_editoriale](https://git.spip.net/spip-contrib-extensions/activite_editoriale)|[3.0.0;3.2.*]|5|3.2;3.1;2.1|< 2023-02-23|
|[afficher_logo](https://git.spip.net/spip-contrib-extensions/afficher_logo)|[3.0.0;4.*]|2|4.1;3.2|2024-07-13|
|[alertes](https://git.spip.net/spip-contrib-extensions/alertes)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[alerte_urgence](https://git.spip.net/spip-contrib-extensions/alerte_urgence)|[3.0.0;4.*]|27|4.4;4.3;4.2;4.1;3.2;3.1|2024-07-05|
|[api_syntaxe](https://git.spip.net/spip-contrib-extensions/api_syntaxe)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[autolang](https://git.spip.net/spip-contrib-extensions/autolang)|[3.0.0;4.*]|9|4.3;4.2;3.2|2024-07-05|
|[bannieres](https://git.spip.net/spip-contrib-extensions/bannieres)|[3.0.0;3.2.*]|12|4.3;3.2;2.1;2.0;1.9|2021-03-21|
|[blocsdepliables](https://git.spip.net/spip-contrib-extensions/blocsdepliables)|[3.0.0;4.*]|152|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-12-10|
|[bootstrap](https://git.spip.net/spip-contrib-extensions/bootstrap2)|[3.0.2;4.*]|0|N/A|2024-07-05|
|[bootstrap3](https://git.spip.net/spip-contrib-extensions/bootstrap3)|[3.0.2;4.*]|22|4.3;4.2;3.2;3.1|2024-07-05|
|[bootstrap3_sass](https://git.spip.net/spip-contrib-extensions/bootstrap3_sass)|[3.0.2;4.0.*]|1|3.2|< 2023-02-23|
|[cachefix](https://git.spip.net/spip-contrib-extensions/cachefix)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[campagnes](https://git.spip.net/spip-contrib-extensions/campagnes)|[3.0.5;4.*]|11|4.3;4.2;3.2|2024-12-04|
|[cartes_choroplethes](https://git.spip.net/spip-contrib-extensions/cartes_choroplethes)|[3.0.0;3.2.*]|0|N/A|2024-10-07|
|[cci](https://git.spip.net/spip-contrib-extensions/cci)|[3.0.5;]|0|N/A|2024-07-01|
|[chatbox](https://git.spip.net/spip-contrib-extensions/chatbox)|[3.0.0;4.0.*]|1|3.1|< 2023-02-23|
|[chats](https://git.spip.net/spip-contrib-extensions/chats)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[chosen](https://git.spip.net/spip-contrib-extensions/chosen)|[3.0.0;4.*]|33|4.3;4.1;3.2;3.1;3.0|2025-01-29|
|[clients](https://git.spip.net/spip-contrib-extensions/clients)|[3.0.0;4.*]|2|4.3;3.0|2024-07-13|
|[clil](https://git.spip.net/spip-contrib-extensions/clil)|[3.0.18;4.*]|4|4.2;3.2|2024-07-13|
|[cog](https://git.spip.net/spip-contrib-extensions/COG)|[3.0.0;3.2.*]|3|4.0;3.2|< 2023-02-23|
|[coggps](https://git.spip.net/spip-contrib-extensions/COG_gps)|[3.0.0;3.2.*]|1|4.0|< 2023-02-23|
|[collectionjson](https://git.spip.net/spip-contrib-extensions/collection_json)|[3.0.0;4.*]|4|4.2;4.1|2024-07-28|
|[comarquage](https://git.spip.net/spip-contrib-extensions/comarquage)|[3.0.0;4.*]|7|4.3;3.2;3.0;2.1|2025-01-10|
|[cookiebar](https://git.spip.net/spip-contrib-extensions/cookiebar)|[3.0.17;4.*]|83|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1|2024-07-05|
|[cookiechoices](https://git.spip.net/spip-contrib-extensions/cookiechoices)|[3.0.17;4.*]|37|4.3;4.1;3.2;3.1;3.0|2024-07-13|
|[couleurs_spip](https://git.spip.net/spip-contrib-extensions/couleurs_spip)|[3.0.0;4.*]|375|4.4;4.3;4.2;4.0;3.2;3.1;3.0;2.1;2.0|2024-07-05|
|[couleur_objet](https://git.spip.net/spip-contrib-extensions/couleur_objet)|[3.0.0;4.*]|284|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[pb_couleur_rubrique](https://git.spip.net/23forward/couleur_rubrique)|[3.0.0;4.*]|82|4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0|2024-07-26|
|[balai](https://git.spip.net/spip-contrib-extensions/coup_de_balai)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[courtjus](https://git.spip.net/spip-contrib-extensions/courtjus)|[3.0.16;3.2.*]|3|3.2|< 2023-02-23|
|[critere_mots](https://git.spip.net/spip-contrib-extensions/critere_mots)|[3.0.0;4.*]|220|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0|2024-07-05|
|[cvt_rechercher](https://git.spip.net/spip-contrib-extensions/cvt_rechercher)|[3.0.0;4.*]|4|4.3;4.2;4.0|2024-07-05|
|[dame](https://git.spip.net/spip-contrib-extensions/dame_blanche)|[3.0.0;4.*]|4|4.4;4.3;3.2|2024-07-13|
|[dates_outils](https://git.spip.net/spip-contrib-extensions/dates_outils)|[3.0.0;4.*]|2|4.1;3.2|2024-07-13|
|[date_creation](https://git.spip.net/spip-contrib-extensions/date_creation)|[3.0.0;4.*]|5|4.3;4.2|2024-07-05|
|[dd](https://git.spip.net/spip-contrib-extensions/dd)|[3.0.0;4.*]|24|4.3;4.2;3.2;3.0;2.1;2.0|2025-02-08|
|[declinaisons](https://git.spip.net/spip-contrib-extensions/declinaisons)|[3.0.5;4.*]|2|4.3;3.2|2024-07-13|
|[devise](https://git.spip.net/spip-contrib-extensions/devise)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[diagrammes](https://git.spip.net/spip-contrib-extensions/diagrammes)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[diapo](https://git.spip.net/spip-contrib-extensions/diapo)|[3.0.0;4.*]|64|4.4;4.3;4.2;4.0;3.2;3.0;2.1;2.0;1.9|2024-07-05|
|[dictionnaires](https://git.spip.net/spip-contrib-extensions/dictionnaires)|[3.0.0;4.*.*]|232|4.4;4.3;4.2;4.1;3.2;3.1;3.0|2023-04-04|
|[doc2img](https://git.spip.net/spip-contrib-extensions/doc2img)|[3.0.0;4.*]|10|4.2;3.2;3.0|2024-07-13|
|[docker](https://git.spip.net/spip-contrib-extensions/docker)|[3.0.2;4.1.*]|0|N/A|2024-07-24|
|[dompdf](https://git.spip.net/spip-contrib-extensions/dompdf)|[3.0.0;4.*]|8|4.3;4.0;3.2|2024-09-03|
|[edition_directe](https://git.spip.net/spip-contrib-extensions/edition_directe)|[3.0.17;4.*]|10|4.3;3.2;3.0|2024-07-13|
|[elasticsearch](https://git.spip.net/spip-contrib-extensions/elasticsearch)|[3.0.0;3.2.*]|3|4.0;3.2|< 2023-02-23|
|[emballe_medias](https://git.spip.net/spip-contrib-extensions/emballe_medias)|[3.0.3;3.1.*]|10|3.2;3.1;3.0|< 2023-02-23|
|[collections](https://git.spip.net/spip-contrib-extensions/emballe_medias_collections)|[3.0.5;3.2.*]|2|3.2;3.1|< 2023-02-23|
|[swfupload](https://git.spip.net/spip-contrib-extensions/emballe_medias_swfupload)|[3.0.0;3.2.*]|26|3.2;3.1;3.0;2.1;2.0;1.9|< 2023-02-23|
|[evamentions](https://git.spip.net/spip-contrib-extensions/eva-web-mentions)|[3.0.0;4.*]|623|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0|2024-07-13|
|[facd](https://git.spip.net/spip-contrib-extensions/facd)|[3.0.0;4.*]|15|4.3;4.2;3.2;3.0|2024-07-05|
|[facteurmonitoring](https://git.spip.net/spip-contrib-extensions/facteurmonitoring)|[3.0.19;4.*]|8|4.3;3.2;3.1|2024-07-13|
|[feuillederoute](https://git.spip.net/spip-contrib-extensions/feuillederoute)|[3.0.0;4.*]|19|4.3;4.2;3.2;3.1|2024-07-13|
|[ffedata](https://git.spip.net/spip-contrib-extensions/ffedata)|[3.0.0;4.*]|5|4.4;4.3;3.1|2024-07-05|
|[flickr_rand](https://git.spip.net/spip-contrib-extensions/flickr_hasard)|[3.0.5;3.2.*]|0|N/A|< 2023-02-23|
|[fonctions_images](https://git.spip.net/spip-contrib-extensions/fonctions_images)|[3.0.0;4.*]|276|4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-05|
|[fonds](https://git.spip.net/spip-contrib-extensions/fonds)|[3.0.0;4.*]|3|4.3|2024-07-05|
|[formatspip](https://git.spip.net/spip-contrib-extensions/format_spip)|[3.0.0;4.*]|1|4.3|2024-07-05|
|[formifusion](https://git.spip.net/spip-contrib-extensions/formidable_fusion)|[3.0.0;4.1.*]|1|3.2|2024-07-24|
|[formidableinscription](https://git.spip.net/spip-contrib-extensions/formidable_inscription)|[3.0.0;4.*]|1|4.3|2024-07-05|
|[formidable_quizz](https://git.spip.net/spip-contrib-extensions/formidable_quizz)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[formidable_reponses_commentees](https://git.spip.net/spip-contrib-extensions/formidable_reponses_commentees)|[3.0.0;3.2.*]|2|3.2|< 2023-02-23|
|[formulaireupload](https://git.spip.net/spip-contrib-extensions/formulaire_upload)|[3.0.0;4.*]|24|4.3;4.0;3.2;3.1;3.0;1.9|2024-07-13|
|[fusionmots](https://git.spip.net/spip-contrib-extensions/fusion_mots)|[3.0.0;3.1.*]|1|3.0|2023-04-09|
|[gcalendar](https://git.spip.net/spip-contrib-extensions/gcalendar)|[3.0.1;3.2.*]|14|3.0;2.1;2.0;1.9|< 2023-02-23|
|[geoip](https://git.spip.net/spip-contrib-extensions/geoip)|[3.0.0;3.2.*]|1|3.1|< 2023-02-23|
|[gestionml](https://git.spip.net/spip-contrib-extensions/gestion_ml)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[gisban](https://git.spip.net/spip-contrib-extensions/gisban)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[grigri](https://git.spip.net/spip-contrib-extensions/grigri)|[3.0.0;4.*]|8|4.3;4.1;3.2|2024-07-05|
|[griseusico](https://git.spip.net/spip-contrib-extensions/griseus_icones)|[3.0.0;3.2.*]|2|3.2;3.0|< 2023-02-23|
|[griseusicobarre](https://git.spip.net/spip-contrib-extensions/griseus_icones_barre)|[3.0.0;3.2.*]|6|3.2;3.1;3.0|< 2023-02-23|
|[griseusvignettes](https://git.spip.net/spip-contrib-extensions/griseus_vignettes)|[3.0.0;3.2.*]|2|3.2;3.0|< 2023-02-23|
|[gtm](https://git.spip.net/spip-contrib-extensions/gtm)|[3.0.0;4.*]|2|4.3;4.2|2024-07-13|
|[horlogeflash](https://git.spip.net/spip-contrib-extensions/horloge_flash)|[3.0.0;4.*]|0|N/A|2024-07-05|
|[hotspots](https://git.spip.net/spip-contrib-extensions/Hotspots)|[3.0.0;4.*]|1|4.3|2024-09-26|
|[html5_responsive](https://git.spip.net/spip-contrib-extensions/html5_responsive)|[3.0.0;4.*]|20|4.4;4.3;4.2;4.1;4.0;3.2;3.1|2024-07-05|
|[ieconfig](https://git.spip.net/spip-contrib-extensions/ieconfig)|[3.0.0;4.*]|293|4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1|2024-07-05|
|[image_par_defaut](https://git.spip.net/spip-contrib-extensions/image_par_defaut)|[3.0.5;3.2.*]|0|N/A|< 2023-02-23|
|[image_responsive](https://git.spip.net/23forward/image_responsive)|[3.0.0;4.*]|211|4.4;4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2025-01-08|
|[image_split](https://git.spip.net/spip-contrib-extensions/image_split)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[immeubles](https://git.spip.net/spip-contrib-extensions/immeubles)|[3.0.0;4.0.*]|0|N/A|< 2023-02-23|
|[indexerdoc](https://git.spip.net/spip-contrib-extensions/indexer-documents)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[ingredient](https://git.spip.net/spip-contrib-extensions/ingredient)|[3.0.17;3.2.*]|0|N/A|< 2023-02-23|
|[intranet](https://git.spip.net/spip-contrib-extensions/intranet)|[3.0.0;4.*]|6|4.3;3.2;3.0|2024-07-05|
|[jaz](https://git.spip.net/spip-contrib-extensions/joindre_auto_zone)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[jfu](https://git.spip.net/spip-contrib-extensions/jquery_file_upload)|[3.0.0;3.2.*]|10|3.2;3.1;3.0|< 2023-02-23|
|[jqvmap](https://git.spip.net/spip-contrib-extensions/jqvmap)|[3.0.10;4.0.*]|1|3.2|< 2023-02-23|
|[documentation2latex](https://git.spip.net/spip-contrib-extensions/latex)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[latexwheel](https://git.spip.net/spip-contrib-extensions/latexwheel)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[lazysizes](https://git.spip.net/spip-contrib-extensions/lazysizes)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[liaison_objet](https://git.spip.net/spip-contrib-extensions/liaison_objet)|[3.0.5;4.*]|5|4.3;4.2;4.0|2024-07-13|
|[lienscontenus](https://git.spip.net/spip-contrib-extensions/liens_contenus)|[3.0.0;4.*]|2|4.3;4.2|2025-01-29|
|[lire_aussi](https://git.spip.net/23forward/lire_aussi)|[3.0.0;4.*]|14|4.3;3.1;2.1;2.0|2024-07-26|
|[lister_plugins](https://git.spip.net/spip-contrib-extensions/lister_plugins)|[3.0.0;4.*]|19|4.3;4.2;3.2;3.1;3.0|2024-07-05|
|[listes_articles_completes](https://git.spip.net/23forward/listes_articles_completes)|[3.0.0;4.*]|9|4.3;3.2;3.0;2.1;2.0|2024-07-28|
|[location_objets](https://git.spip.net/spip-contrib-extensions/location_objets)|[3.0.0;4.0.*]|0|N/A|< 2023-02-23|
|[macros](https://git.spip.net/spip-contrib-extensions/macros)|[3.0.11;3.2.*]|1|4.3|< 2023-02-23|
|[maquettes](https://git.spip.net/spip-contrib-extensions/maquettes_multiples)|[3.0.0;4.*]|4|4.3|2024-07-24|
|[massicot](https://git.spip.net/spip-contrib-extensions/massicot)|[3.0.17;4.*]|67|4.3;4.2;3.2;3.1|2024-07-05|
|[mediaspip_player](https://git.spip.net/spip-contrib-extensions/mediaspip_player)|[3.0.0;3.2.*]|72|4.3;3.2;3.1;3.0|2024-07-29|
|[medias_dereferencer](https://git.spip.net/spip-contrib-extensions/medias_dereferencer)|[3.0.9;3.2.*]|1|3.1|< 2023-02-23|
|[medias_logos](https://git.spip.net/spip-contrib-extensions/medias_logos)|[3.0.0;4.*]|11|3.2|2024-07-13|
|[medias_responsive_mod](https://git.spip.net/23forward/medias_responsive_mod)|[3.0.0;4.*]|65|4.4;4.3;4.2;4.1;3.2;3.1|2024-07-27|
|[medoc](https://git.spip.net/spip-contrib-extensions/medoc)|[3.0.0;3.2.*]|23|4.3;3.2;3.1;3.0|< 2023-02-23|
|[accordeon](https://git.spip.net/spip-contrib-extensions/menu_accordeon)|[3.0.0;3.2.*]|12|3.2;3.0;2.1|< 2023-02-23|
|[menu_anime](https://git.spip.net/spip-contrib-extensions/menu_anime)|[3.0.0;4.*]|30|4.3;4.2;4.1;3.2;3.1;3.0|2024-07-05|
|[mesfavoris_collections](https://git.spip.net/spip-contrib-extensions/mesfavoris_collections)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[photo_infos](https://git.spip.net/spip-contrib-extensions/metadonnees_photo)|[3.0.0;3.2.*]|15|3.2;3.1;3.0;2.1|< 2023-02-23|
|[metas](https://git.spip.net/spip-contrib-extensions/metas)|[3.0.0;4.*]|28|4.3;4.2;4.1;3.2;3.0;2.1;2.0|2024-07-13|
|[modelexergue](https://git.spip.net/spip-contrib-extensions/modelexergue)|[3.0.0;4.*]|19|4.4;4.3;4.2;3.2;3.1|2024-07-05|
|[moncompte](https://git.spip.net/spip-contrib-extensions/moncompte)|[3.0.0;4.*]|1|4.2|2024-07-05|
|[motscreer](https://git.spip.net/spip-contrib-extensions/mots_creer)|[3.0.0;4.*]|4|4.3;4.2;3.2|2024-07-05|
|[multidomaines](https://git.spip.net/spip-contrib-extensions/multidomaines)|[3.0.0;4.*]|18|4.4;4.3;4.2;3.2|2025-01-31|
|[mutualisation](https://git.spip.net/spip-contrib-extensions/mutualisation)|[3.0.0;4.*]|0|N/A|2024-07-05|
|[nemequittepas](https://git.spip.net/spip-contrib-extensions/nemequittepas)|[3.0.0;4.*]|52|4.4;4.3;4.1;3.2|2024-07-05|
|[noie](https://git.spip.net/spip-contrib-extensions/noie)|[3.0.0;3.2.*]|47|3.2;3.1;3.0;2.1;2.0;1.9|< 2023-02-23|
|[noizetier_doc](https://git.spip.net/spip-contrib-extensions/noizetier_doc)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[notifications](https://git.spip.net/spip-contrib-extensions/notifications)|[3.0.0;4.*]|470|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2025-01-29|
|[oai](https://git.spip.net/spip-contrib-extensions/oai)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[obfuscation_liens](https://git.spip.net/spip-contrib-extensions/obfuscation_liens)|[3.0.0;4.*]|3|4.3;4.2;3.2|2024-07-05|
|[onglets_texte](https://git.spip.net/spip-contrib-extensions/onglets_texte)|[3.0.0;4.*]|125|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;1.9|2024-07-05|
|[outils_article](https://git.spip.net/spip-contrib-extensions/outils_article)|[3.0.0;4.*]|42|4.3;4.1;3.2;3.1;3.0;2.1;2.0;1.9|2024-07-13|
|[owlcarousel](https://git.spip.net/spip-contrib-extensions/owlcarousel)|[3.0.5;4.*]|91|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[pages_mobiles](https://git.spip.net/spip-contrib-extensions/page_mobiles)|[3.0.4;3.2.*]|19|3.2;3.0|< 2023-02-23|
|[partageur](https://git.spip.net/spip-contrib-extensions/partageur)|[3.0.5;4.*]|24|4.3;4.2;3.2;3.1;3.0|2024-07-05|
|[partenaires](https://git.spip.net/spip-contrib-extensions/partenaires)|[3.0.5;4.*]|4|4.2;3.2|2021-03-23|
|[passe_complexe](https://git.spip.net/spip-contrib-extensions/passe_complexe)|[3.0.0;4.*]|0|N/A|2024-07-13|
|[periodes](https://git.spip.net/spip-contrib-extensions/periodes)|[3.0.0;4.*]|0|N/A|2024-07-20|
|[picto](https://git.spip.net/spip-contrib-extensions/picto)|[3.0.0;4.*]|63|4.4;4.3;4.2;4.1;3.2;3.0|2024-07-05|
|[planning_simple](https://git.spip.net/spip-contrib-extensions/planning_simple)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[plantuml](https://git.spip.net/spip-contrib-extensions/plantuml)|[3.0.0;4.*]|0|N/A|2024-07-05|
|[podcast](https://git.spip.net/spip-contrib-extensions/podcast)|[3.0.0;4.*]|8|4.4;4.3;3.2;3.0|2024-07-05|
|[polyconf](https://git.spip.net/spip-contrib-extensions/polyhierarchie_configurable)|[3.0.0;4.*]|0|N/A|2024-07-05|
|[spipopup](https://git.spip.net/spip-contrib-extensions/popup)|[3.0.0;4.*]|22|4.3;4.2;3.2;3.1;2.1|2024-07-13|
|[pp_blocs](https://git.spip.net/spip-contrib-extensions/porte_plume_blocs)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[pp_chgt_lg](https://git.spip.net/spip-contrib-extensions/porte_plume_changement_langue)|[3.0.0;4.*]|219|4.3;4.2;4.1;3.2;3.1;2.1|2024-07-05|
|[typoenluminee](https://git.spip.net/spip-contrib-extensions/typoenluminee)|[3.0.0;4.*]|1308|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2024-10-04|
|[portfolio_simple_v2](https://git.spip.net/spip-contrib-extensions/portfolio_simple_v2)|[3.0.0;3.2.*]|4|3.2;3.0|< 2023-02-23|
|[prestashop_api](https://git.spip.net/spip-contrib-extensions/prestashop_api)|[3.0.0;3.2.*]|0|N/A|2018-08-20|
|[prive_fluide](https://git.spip.net/spip-contrib-extensions/prive_fluide)|[3.0.0;3.2.*]|16|3.2;3.1;3.0|< 2023-02-23|
|[prive_fluide_plus](https://git.spip.net/spip-contrib-extensions/prive_fluide_plus)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[prive_fluide_remix](https://git.spip.net/spip-contrib-extensions/prive_fluide_remix)|[3.0.0;3.2.*]|7|3.2|< 2023-02-23|
|[prix_objets_periodes](https://git.spip.net/spip-contrib-extensions/prix_objets_periodes)|[3.0.0;4.*]|0|N/A|2024-07-20|
|[qrcode](https://git.spip.net/spip-contrib-extensions/qrcode)|[3.0.15;4.*]|78|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1|2024-07-05|
|[quickflip](https://git.spip.net/spip-contrib-extensions/quickflip)|[3.0.0;4.*]|2|4.3;3.2|2024-07-05|
|[rao](https://git.spip.net/spip-contrib-extensions/rao)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[recherche_sti](https://git.spip.net/spip-contrib-extensions/recherche_mots_cles)|[3.0.0;3.2.*]|2|3.2|< 2023-02-23|
|[relecture](https://git.spip.net/spip-contrib-extensions/relecture)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[requeteursql](https://git.spip.net/spip-contrib-extensions/requeteursql)|[3.0.0;3.2.*]|2|3.2;3.1|2023-09-27|
|[reservation_bank](https://git.spip.net/spip-contrib-extensions/reservations_bank)|[3.0.20;4.*]|0|N/A|2024-07-20|
|[reservations_credits](https://git.spip.net/spip-contrib-extensions/reservations_credits)|[3.0.20;4.*]|0|N/A|2024-07-20|
|[reservations_multiples](https://git.spip.net/spip-contrib-extensions/reservations_multiples)|[3.0.16;4.*]|3|4.1;4.0;3.2|2024-07-13|
|[reservation_evenement](https://git.spip.net/spip-contrib-extensions/reservation_evenement)|[3.0.16;4.*]|18|4.3;4.2;4.1;4.0;3.2|2024-07-13|
|[roles_gis](https://git.spip.net/spip-contrib-extensions/roles_gis)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[roles_selections](https://git.spip.net/spip-contrib-extensions/roles_selections)|[3.0.0;4.*]|4|4.3;4.2;4.0;3.2|2024-07-05|
|[rspipcm](https://git.spip.net/spip-contrib-extensions/rspip_code_mail)|[3.0.0;3.2.*]|7|3.2;3.1;3.0;2.1|< 2023-02-23|
|[rubriqueur](https://git.spip.net/spip-contrib-extensions/rubriqueur)|[3.0.0;4.*]|9|4.4;4.3;3.2;3.1|2024-11-10|
|[rubriquepreferee](https://git.spip.net/spip-contrib-extensions/rubrique_preferee)|[3.0.0;3.2.*]|2|4.2;3.2|< 2023-02-23|
|[saisie_liste](https://git.spip.net/spip-contrib-extensions/saisie_liste)|[3.0.10;3.2.*]|9|3.2;3.1|< 2023-02-23|
|[sale](https://git.spip.net/spip-contrib-extensions/sale)|[3.0.0;4.*]|4|4.1;3.2;3.1;2.1|2024-07-31|
|[savecfg](https://git.spip.net/spip-contrib-extensions/savecfg)|[3.0.20;4.*]|74|4.3;4.2;3.2;3.1;2.1|2024-07-05|
|[seances](https://git.spip.net/spip-contrib-extensions/seances)|[3.0.0;3.2.*]|8|3.2;3.0;2.1|< 2023-02-23|
|[sedna](https://git.spip.net/spip-contrib-extensions/sedna)|[3.0.0;4.*]|20|4.4;4.3;3.2;3.0;2.1;2.0;1.9|2024-07-05|
|[signalement](https://git.spip.net/spip-contrib-extensions/signalement)|[3.0.0;3.2.*]|1|3.1|< 2023-02-23|
|[sjcycle](https://git.spip.net/spip-contrib-extensions/sjcycle)|[3.0.0;4.*]|710|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;2.0|2024-07-05|
|[slick](https://git.spip.net/spip-contrib-extensions/slick)|[3.0.0;4.*]|277|4.4;4.3;4.2;4.1;3.2;3.1|2024-07-05|
|[sms](https://git.spip.net/spip-contrib-extensions/sms)|[3.0.0;4.*]|7|4.3;3.2;3.1|2024-07-05|
|[smush](https://git.spip.net/spip-contrib-extensions/smush_images)|[3.0.3;4.*]|66|4.3;3.2;3.1;3.0|2024-07-05|
|[soapsympa](https://git.spip.net/spip-contrib-extensions/soap-sympa)|[3.0.0;4.*]|1|4.3|2024-07-05|
|[sommaire](https://git.spip.net/spip-contrib-extensions/sommaire)|[3.0.0;4.*]|128|4.4;4.3;4.2;4.1;4.0;3.2;3.1|2024-10-27|
|[souhaits](https://git.spip.net/spip-contrib-extensions/souhaits)|[3.0.5;4.*]|0|N/A|2024-11-09|
|[spipcatchat](https://git.spip.net/spip-contrib-extensions/spipcatchat)|[3.0.0;3.2.*]|1|3.0|< 2023-02-23|
|[spipmotion](https://git.spip.net/spip-contrib-extensions/spipmotion)|[3.0.0;4.*]|11|3.2;3.0;2.0|2021-03-21|
|[spip_400](https://git.spip.net/spip-contrib-extensions/spip_400)|[3.0.0;4.*]|13|4.3;4.0;3.2;3.1;3.0|2024-07-22|
|[spip_proprio](https://git.spip.net/spip-contrib-extensions/spip_proprietaire)|[3.0.0;4.*]|164|4.3;4.2;4.0;3.2;3.1;3.0;2.1|2024-07-13|
|[squelettesmots](https://git.spip.net/spip-contrib-extensions/squelettes_par_mots_cle)|[3.0.0;4.*]|50|4.4;4.3;4.2;4.1;3.2;3.1;3.0;2.1;1.9|2024-07-05|
|[statut_articles](https://git.spip.net/spip-contrib-extensions/statut_articles)|[3.0.0;4.*]|50|4.3;4.1;4.0;3.2;3.1|2024-07-05|
|[styleguide](https://git.spip.net/spip-contrib-extensions/styleguide)|[3.0.0;4.*]|0|N/A|2024-05-27|
|[suploadhtml5](https://git.spip.net/spip-contrib-extensions/suploadhtml5)|[3.0.0;3.2.*]|2|3.2|< 2023-02-23|
|[textes_etendus](https://git.spip.net/spip-contrib-extensions/textes_etendus)|[3.0.0;4.*]|4|4.3|2024-07-05|
|[tipafriend](https://git.spip.net/spip-contrib-extensions/tipafriend)|[3.0.0;4.*]|14|4.3;4.2;4.0;3.2;3.1;3.0;2.1|2024-07-13|
|[titre_logo](https://git.spip.net/spip-contrib-extensions/titre_de_logo)|[3.0.0;4.*]|20|4.3;4.2;4.1;3.2;3.1;3.0;2.1|2024-09-16|
|[tradrub_proposee](https://git.spip.net/spip-contrib-extensions/tradrub_proposee)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[taa](https://git.spip.net/spip-contrib-extensions/traduction_articles_autrement)|[3.0.0;3.2.*]|23|3.2;3.0;2.1|< 2023-02-23|
|[trad_rub](https://git.spip.net/spip-contrib-extensions/traduction_rubriques_autrement)|[3.0.10;3.2.*]|20|3.2;3.0|< 2023-02-23|
|[traduiretexte](https://git.spip.net/spip-contrib-extensions/traduire_texte)|[3.0.0;4.*]|3|4.3;3.2|2024-07-05|
|[uploadhtml5](https://git.spip.net/spip-contrib-extensions/uploadhtml5)|[3.0.0;4.*]|91|4.3;4.2;4.1;4.0;3.3;3.2;3.1;3.0|2024-11-13|
|[verifier_plugins](https://git.spip.net/spip-contrib-extensions/verifier_plugins)|[3.0.0;4.*]|362|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2025-02-14|
|[vider_rubrique](https://git.spip.net/spip-contrib-extensions/vider_rubrique)|[3.0.0;4.*]|21|4.3;4.2;4.1;3.2;3.0|2024-07-05|
|[vignettes_oxygen](https://git.spip.net/spip-contrib-extensions/vignettes_oxygen_128)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[vignettes_oxygen52](https://git.spip.net/spip-contrib-extensions/vignettes_oxygen_52)|[3.0.0;3.2.*]|2|3.2;2.1|< 2023-02-23|
|[wp_import](https://git.spip.net/spip-contrib-extensions/wp_import)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[xiti](https://git.spip.net/spip-contrib-extensions/xiti)|[3.0.0;3.2.*]|4|4.2;3.2|< 2023-02-23|
|[zengarden](https://git.spip.net/spip-contrib-extensions/zen-garden)|[3.0.0;4.*]|564|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0;2.1;2.0;1.9|2025-01-29|
|[zeroclipboard](https://git.spip.net/spip-contrib-extensions/zeroclipboard)|[3.0.5;3.2.*]|1|3.2|< 2023-02-23|
|[zinit](https://git.spip.net/spip-contrib-extensions/zinit)|[3.0.9;4.*]|13|4.3;4.2;4.1;3.2;3.1|2025-01-20|
|[zless](https://git.spip.net/spip-contrib-extensions/zless)|[3.0.0;4.*]|0|N/A|2024-07-05|
|[balloons](https://git.spip.net/spip-contrib-squelettes/balloons)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[curalicious](https://git.spip.net/spip-contrib-squelettes/curalicious)|[3.0.0;3.2.*]|2|3.2;3.0|< 2023-02-23|
|[documentation](https://git.spip.net/spip-contrib-squelettes/documentation)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[alpha](https://git.spip.net/spip-contrib-squelettes/html5up_alpha_legacy)|[3.0.0;4.*]|19|4.4;4.3;4.2;4.1;3.2;3.0|2024-07-13|
|[html5up_strongly_typed](https://git.spip.net/spip-contrib-squelettes/html5up_strongly_typed)|[3.0.0;3.2.*]|0|N/A|< 2023-02-23|
|[japibas](https://git.spip.net/spip-contrib-squelettes/japibas)|[3.0.0;3.2.*]|7|3.2;3.1;3.0|< 2023-02-23|
|[median](https://git.spip.net/spip-contrib-squelettes/median)|[3.0.0;3.2.*]|2|3.2|< 2023-02-23|
|[moodboard](https://git.spip.net/spip-contrib-squelettes/moodboard)|[3.0.0;3.2.*]|1|3.2|< 2023-02-23|
|[parallelism](https://git.spip.net/spip-contrib-squelettes/parallelism4spip)|[3.0.1;3.2.*]|1|3.2|< 2023-02-23|
|[sarkaspipr](https://git.spip.net/spip-contrib-squelettes/sarkaspipr)|[3.0.0;4.*]|49|4.4;4.3;4.2;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[sdc](https://git.spip.net/spip-contrib-squelettes/spipr-dane-config)|[3.0.17;4.*]|32|4.4;4.2|2024-07-13|
|[spipr_doc](https://git.spip.net/spip-contrib-squelettes/spipr-doc)|[3.0.0;4.*]|10|4.3;4.1;4.0;3.2;3.1;3.0|2024-07-05|
|[spipr_educ](https://git.spip.net/spip-contrib-squelettes/spipr-educ)|[3.0.0;4.*]|91|4.3;4.2;3.2|2024-07-13|
|[tutocommerce](https://git.spip.net/spip-contrib-squelettes/tutocommerce)|[3.0.17;3.2.*]|4|4.3;3.2|< 2023-02-23|
|[zoundation](https://git.spip.net/spip-contrib-squelettes/zoundation)|[3.0.13;4.*]|4|4.2;3.2;3.1|2024-07-05|
|[zpip](https://git.spip.net/spip-contrib-squelettes/zpip-dist)|[3.0.0;4.*]|28|4.3;4.2;3.2;3.1;3.0|2024-07-05|
|[syntaxe_spip](https://git.spip.net/spip-galaxie/syntaxe.spip.net)|[3.0.0;4.*]|0|N/A|2024-05-29|
|[sync_from_forge](https://git.spip.net/spip-contrib-extensions/sync_from_forge)|[3.0.0;[|1|3.2|2024-09-03|
|[debut_fin](https://git.spip.net/spip-contrib-extensions/debut_fin4)|[3.0.0;4.*]|2|3.2;3.1|2025-02-10|
