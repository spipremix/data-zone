# max31

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[emballe_medias](https://git.spip.net/spip-contrib-extensions/emballe_medias)|[3.0.3;3.1.*]|10|3.2;3.1;3.0|< 2023-02-23|
|[fusionmots](https://git.spip.net/spip-contrib-extensions/fusion_mots)|[3.0.0;3.1.*]|1|3.0|2023-04-09|
