# max40

|prefix|spip_compatibility|sites|spip|last_commit|
|:-|:-|-:|:-|:-|
|[aeres](https://git.spip.net/spip-contrib-extensions/aeres)|[3.2.0;4.0.*]|0|N/A|< 2023-02-23|
|[basecss](https://git.spip.net/spip-contrib-extensions/basecss)|[4.0.0-dev;4.0.*]|0|N/A|< 2023-02-23|
|[bibliocheck](https://git.spip.net/spip-contrib-extensions/bibliocheck)|[3.2.0;4.0.*]|0|N/A|< 2023-02-23|
|[bootstrap3_sass](https://git.spip.net/spip-contrib-extensions/bootstrap3_sass)|[3.0.2;4.0.*]|1|3.2|< 2023-02-23|
|[chatbox](https://git.spip.net/spip-contrib-extensions/chatbox)|[3.0.0;4.0.*]|1|3.1|< 2023-02-23|
|[chiffrer](https://git.spip.net/spip-contrib-extensions/chiffrer)|[4.0.0;4.0.*]|0|N/A|< 2023-02-23|
|[colorbox](https://git.spip.net/spip-contrib-extensions/colorbox)|[4.0.0;4.0.*]|0|N/A|< 2023-02-23|
|[historique_spip32_html4](https://git.spip.net/spip-contrib-extensions/historique_spip32_html4)|]3.2.999;4.0.*]|0|N/A|< 2023-02-23|
|[immeubles](https://git.spip.net/spip-contrib-extensions/immeubles)|[3.0.0;4.0.*]|0|N/A|< 2023-02-23|
|[ipset](https://git.spip.net/spip-contrib-extensions/ipset)|[3.2.0;4.0.*]|0|N/A|< 2023-02-23|
|[jqvmap](https://git.spip.net/spip-contrib-extensions/jqvmap)|[3.0.10;4.0.*]|1|3.2|< 2023-02-23|
|[location_objets](https://git.spip.net/spip-contrib-extensions/location_objets)|[3.0.0;4.0.*]|0|N/A|< 2023-02-23|
|[opquast](https://git.spip.net/spip-contrib-extensions/opquast)|[3.2.0;4.0.*]|1|4.0|< 2023-02-23|
|[op_elec](https://git.spip.net/spip-contrib-extensions/op_elec)|[4.0.0;4.0.*]|0|N/A|< 2023-02-23|
|[owncloud](https://git.spip.net/spip-contrib-extensions/owncloud)|[3.1.0;4.0.*]|0|N/A|2024-09-12|
|[rang_mots](https://git.spip.net/spip-contrib-extensions/rang_mots)|]3.3.0-dev;4.0.*]|0|N/A|2023-04-09|
|[svpcvs](https://git.spip.net/spip-contrib-extensions/svpcvs)|[3.2.0-dev;4.0.*]|0|N/A|< 2023-02-23|
|[tb](https://git.spip.net/spip-contrib-extensions/testbuilder)|[4.0.0-alpha;4.0.*]|0|N/A|< 2023-02-23|
|[tradsync](https://git.spip.net/spip-contrib-extensions/tradsync)|[4.0.0;4.0.*]|0|N/A|2025-01-29|
|[vimeo](https://git.spip.net/spip-contrib-extensions/vimeo)|[3.2.0;4.0.*]|1|3.2|< 2023-02-23|
|[historique_dist_html4](https://git.spip.net/spip-contrib-squelettes/historique_dist_html4)|]3.2.999;4.0.*]|0|N/A|< 2023-02-23|
|[zvide](https://git.spip.net/spip-contrib-squelettes/zpip-vide)|[4.0.0;4.0.*]|20|3.2;3.1;3.0;2.1|< 2023-02-23|
|[theme_bs4blog](https://git.spip.net/spip-contrib-themes/spipr_v2_beige)|[3.2.0;4.0.*]|0|N/A|< 2023-02-23|
|[theme_bs4journal](https://git.spip.net/spip-contrib-themes/spipr_v2_journal)|[3.2.0;4.0.*]|1|4.3|< 2023-02-23|
|[theme_bs4kit_de_demarrage](https://git.spip.net/spip-contrib-themes/spipr_v2_kit_de_demarrage)|[3.2.0;4.0.*]|0|N/A|2024-05-09|
